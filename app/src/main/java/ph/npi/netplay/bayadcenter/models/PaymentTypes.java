package ph.npi.netplay.bayadcenter.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by Rhen Nepacena on 5/27/2015.
 */

@Table(name = "paymentTypes")
public class PaymentTypes extends Model {

    @Column(name = "pk_id")
    public int pk_id;

    @Column(name = "name", index = true)
    public String name;

    @Column(name = "code")
    public String code;




    public int getPaymentTypeCount() {
        return new Select().from(PaymentTypes.class).count();
    }

    public List<PaymentTypes> getPaymentTypeList(){
        return new Select().from(PaymentTypes.class).execute();

    }


}
