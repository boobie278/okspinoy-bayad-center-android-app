package ph.npi.netplay.bayadcenter.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.activeandroid.util.SQLiteUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Table(name = "transactions")
public class Transaction extends Model {

    public static final int SMS_PENDING_REPLY = 80;
    public static final int SMS_REPLY_RECEIVED = 81;

    @Column(name = "mobileNumber")
    public String mobileNumber;
    @Column(name = "topupId", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public int topupId;
    @Column(name = "referenceNo", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String referenceNo;
    @Column(name = "timestampMod", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String timestampMod;
    @Column(name = "response")
    public String response;
    @Column(name = "createdDate")
    public Date createdDate;
    @Column(name = "product")
    public String product;
    @Column(name="brands")
    public String brands;
    @Column(name = "isSuccessful")
    public boolean isSuccessful;
    @Column(name = "status")
    public int status;



    public Transaction() {
        super();
    }

    private Date stringToDate(String mysqlTimestamp) {
        Date date = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd kk:mm:ss");

            long longDate = dateFormat.parse(mysqlTimestamp).getTime();

            date = new Date(longDate);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }

    public List<Transaction> getTransactions(int limit, int offset) {
        return new Select().from(Transaction.class).orderBy("createdDate DESC").limit(limit).offset(offset).execute();
    }

    public List<Transaction> getTransactions() {
        return new Select().from(Transaction.class).orderBy("createdDate DESC").limit(10).offset(0).execute();
    }

    public List<Transaction> getDistinctNumbers(){
        return new Select().distinct().from(Transaction.class).groupBy("mobileNumber").execute();
    }



    public List<Transaction> getMobileNumbers(String brands){
        SQLiteUtils sqlutils = new SQLiteUtils();
        String[] args ={"%"+brands+"%"};

        return sqlutils.rawQuery(Transaction.class,"SELECT * FROM transactions WHERE product LIKE ? GROUP BY mobileNumber",args);
    }
}
