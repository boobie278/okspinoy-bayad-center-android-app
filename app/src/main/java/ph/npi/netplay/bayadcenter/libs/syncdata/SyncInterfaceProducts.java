package ph.npi.netplay.bayadcenter.libs.syncdata;

/**
 * Created by Rhen Nepacena on 3/9/2015.
 */
public interface SyncInterfaceProducts {
    public void onComplete();
    public void onError();
}
