package ph.npi.netplay.bayadcenter.models;

import com.activeandroid.serializer.TypeSerializer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Rhen Nepacena on 11/10/2015.
 */
final public class UtilMapSerializer extends TypeSerializer {
    @Override
    public Class<?> getDeserializedType() {
        return Map.class;
    }

    @Override
    public Class<?> getSerializedType() {
        return String.class;
    }

    @Override
    public String serialize(Object data) {
        if (data == null) {
            return null;
        }

        // Transform a Map<String, Object> to JSON and then to String
        return new JSONObject((Map<String, String>) data).toString();
    }

    @Override
    public Map<String, String> deserialize(Object data) {
        if (data == null) {
            return null;
        }

        // Properties of Model
        Map<String, String> map = new HashMap<String, String>();

        try {
            JSONObject json = new JSONObject((String) data);

            for(Iterator it = json.keys(); it.hasNext();) {
                String key = (String) it.next();

                map.put(key, json.get(key).toString());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return map;
    }
}
