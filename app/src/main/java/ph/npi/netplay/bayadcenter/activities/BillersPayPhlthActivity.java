package ph.npi.netplay.bayadcenter.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.fourmob.datetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import ph.npi.netplay.bayadcenter.Epinoyload;
import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.models.Billers;
import ph.npi.netplay.bayadcenter.models.BillsPay;
import ph.npi.netplay.bayadcenter.models.Events;
import ph.npi.netplay.bayadcenter.models.UtilMapSerializer;

/**
 * Created by Rhen Nepacena on 11/3/2015.
 */
public class BillersPayPhlthActivity extends BillersPayActivity implements DatePickerDialog.OnDateSetListener{

    private EditText med_accountnumber;
    private EditText med_firstname;
    private EditText med_lastname;
    private EditText med_middleinitial;
    private EditText med_amount;
    private Button mbtn_submit;
    private Spinner mspnr_philMemberType;
    private Spinner mspnr_philPaymentType;
    private TextView mtt_fromDate;
    private TextView mtt_toDate;

    private Context mContext;

    private String straccountNumber;
    private String stramount;
    private String strFirstName;
    private String strLastName;
    private String strMiddleName;
    private String strMemberType;
    private String strMemberTypeOutput;
    private String strPhilPaymentType;
    private String strPhilPaymentTypeOutput;
    private String strfromDate;
    private String strtoDate;

    private static final String DATE_PICKER_TAG = "datepicker";
    DatePickerDialog datePickerDialogFrom;
    DatePickerDialog datePickerDialogTo;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_bill_phlth);
        Events.inputEventLogs("Access PhilHealth Form");

        med_accountnumber   = (EditText) findViewById(R.id.ed_accountnumber);
        med_firstname       = (EditText) findViewById(R.id.ed_firstname);
        med_lastname        = (EditText) findViewById(R.id.ed_lastname);
        med_middleinitial   = (EditText) findViewById(R.id.ed_middleinitial);
        med_amount          = (EditText) findViewById(R.id.ed_amount);
        mbtn_submit         = (Button) findViewById(R.id.btn_submit);

        mspnr_philMemberType= (Spinner) findViewById(R.id.spnr_philMemberType);
        mspnr_philPaymentType= (Spinner) findViewById(R.id.spnr_philPaymentType);

        mtt_fromDate        = (TextView) findViewById(R.id.tt_fromDate);
        mtt_toDate          = (TextView) findViewById(R.id.tt_toDate);


        mbtn_submit.setOnClickListener(submitOnClicked);
        mContext = this;

        final Calendar calendar = Calendar.getInstance();
        datePickerDialogFrom = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);
        datePickerDialogTo = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            intentextras = extras.getString("biller_name");
        }

        biller = new Billers();
        biller = biller.getByName(intentextras);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_action_hardware_phone);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(biller.name);


        mtt_fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                datePickerDialogFrom.setYearRange(2014, 2037);
                datePickerDialogFrom.show(getSupportFragmentManager(), DATE_PICKER_TAG);


            }
        });

        mtt_toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                datePickerDialogTo.setYearRange(2014, 2037);
                datePickerDialogTo.show(getSupportFragmentManager(), DATE_PICKER_TAG);


            }
        });

       initializeCheckComponents();
       populateSpinner();

    }



    private View.OnClickListener submitOnClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            straccountNumber  = med_accountnumber.getText().toString().trim();
            stramount         = med_amount.getText().toString().trim();
            strFirstName      = med_firstname.getText().toString().trim();
            strLastName       = med_lastname.getText().toString().trim();
            strMiddleName     = med_middleinitial.getText().toString().trim();
            strMemberType     = mspnr_philMemberType.getSelectedItem().toString();
            strPhilPaymentType= mspnr_philPaymentType.getSelectedItem().toString();

            stramount    = stramount.replace(" ", "");
            stramount    = stramount.replace("-","");

            checkValues();


            if(strMemberType.contains("Informal (Non Pro)")) {
                strMemberTypeOutput = "INP";
            }else if(strMemberType.contains("Self Earning (Pro)")){
                strMemberTypeOutput = "SEP";
            }else if(strMemberType.contains("OFW")){
                strMemberTypeOutput = "OFW";
            }

            if(strPhilPaymentType.contains("6 months")){
                strPhilPaymentTypeOutput = "Semi Annually";
            }else if(strPhilPaymentType.contains("1 year")){
                strPhilPaymentTypeOutput = "Annually";
            }else if(strPhilPaymentType.contains("2 years")){
                strPhilPaymentTypeOutput = "Two Years";
            }


            if((straccountNumber.isEmpty()) || (stramount.isEmpty()) || (strFirstName.isEmpty()) || (strLastName.isEmpty())) {
                if (straccountNumber.isEmpty()) {
                    med_accountnumber.setBackgroundResource(R.drawable.edittext_red_rounded);
                }
                if (stramount.isEmpty()) {
                    med_amount.setBackgroundResource(R.drawable.edittext_red_rounded);
                }

                if(strFirstName.isEmpty()){
                    med_firstname.setBackgroundResource(R.drawable.edittext_red_rounded);
                }

                if(strLastName.isEmpty()){
                    med_lastname.setBackgroundResource(R.drawable.edittext_red_rounded);
                }

                return;
            }

            if(straccountNumber.length() < 11){
                Epinoyload.showAlert(mContext, "Account Number ", "Need to check Account Number");
            }else{
                if (Epinoyload.isOffline(mContext) == false) {
                    AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
                    alertDialog.setTitle("Please Confirm");
                    alertDialog.setMessage("You are about to pay " + biller.name + " with " + biller.field + straccountNumber + ". Are you sure you want to continue?");
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            queTransactions();
                            return;
                        }
                    });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            return;
                        }
                    });
                    alertDialog.show();
                } else {
                    // offline
                    Epinoyload.showAlert(mContext, "Please Confirm", "You are about to pay " + biller.name + " with " + straccountNumber + ".\n\nYou are in offline mode ! Transactions will be queued...\n\nAre you sure you want to continue?",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // Postive
                                    queTransactions();
                                }
                            }, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // negative
                                    dialogInterface.dismiss();
                                }
                            }
                    );
                }
            }
        }
    };


    private void queTransactions(){

        HashMap<String,String> api_encode_all = new HashMap<String, String>();
        api_encode_all.put("account_no",straccountNumber);
        api_encode_all.put("amount_due",stramount);

        api_encode_all.put("service_code","PHLTH");
        api_encode_all.put("mode_of_payment",strPaymentType);
        api_encode_all.put("first_name",strFirstName);
        api_encode_all.put("last_name",strLastName);
        api_encode_all.put("middle_initial",strMiddleName);
        api_encode_all.put("member_type",strMemberTypeOutput);
        api_encode_all.put("bill_date",strfromDate);
        api_encode_all.put("due_date",strtoDate);
        api_encode_all.put("payment_type",strPhilPaymentTypeOutput);

        /*if(strPaymentType.contains("Check") || strPaymentType.contains("Combined")){
            api_encode_all.put("check_no", strcheckNumber);
            api_encode_all.put("check_date",strcheckDate);
            api_encode_all.put("bank_name",strbankName);
            api_encode_all.put("branch",strbranch);

            api_encode_all.put("name",strcheckName);
            api_encode_all.put("bank_code",strbankCode);
            api_encode_all.put("check_type",strtypeName);
        }*/

        if(strPaymentType.contains("Cash")){
            api_encode_all.put("amount",stramount);
            api_encode_all.put("amount_paid",stramount);
        }/*else if(strPaymentType.contains("Check")){
            api_encode_all.put("amount",strcheckAmount);
            api_encode_all.put("amount_paid",stramount);
        }else if(strPaymentType.contains("Combined")){
            api_encode_all.put("cash_amount",stramount);
            api_encode_all.put("check_amount",strcheckAmount);
            float cash_amount = Float.parseFloat(stramount);
            float check_amount = Float.parseFloat(strcheckAmount);
            float total = cash_amount + check_amount;
            api_encode_all.put("amount_paid",String.valueOf(total));
        }*/

        UtilMapSerializer utilMapSerializer = new UtilMapSerializer();
        String api_encode = utilMapSerializer.serialize(api_encode_all);

        Calendar calendar = Calendar.getInstance();
       /* String sentDate = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"))
                .format(calendar.getTime());*/
        String sentTime = (new SimpleDateFormat("HH:mm:ss"))
                .format(calendar.getTime());

        BillsPay bps = new BillsPay();
        bps.billspayid = String.valueOf("" + (System.currentTimeMillis() / 1000));
        bps.createdDate = new Date();
        bps.time_sent = sentTime;

        bps.isSuccessful = false;
        bps.biller_code = biller.bilcode;
        bps.billername = biller.name;
        bps.failed_transaction = 0;

        bps.service_code    = "PHLTH";
        bps.account_no      = straccountNumber;
        bps.amount_due      = stramount;
        bps.amount_paid     = stramount;
        bps.mode_of_payment = strPaymentType;
        bps.amount          = stramount;

        bps.first_name      = strFirstName;
        bps.last_name       = strLastName;
        bps.middle_initial  = strMiddleName;
        bps.member_type     = strMemberTypeOutput;
        bps.to_date         = strtoDate;
        bps.from_date       = strfromDate;
        bps.phil_payment_type= strPhilPaymentTypeOutput;

        bps.check_no        = strcheckNumber;
        bps.check_date      = strcheckDate;
        bps.bank_name       = strbankName;
        bps.branch          = strbranch;
        bps.check_amount    = strcheckAmount;
        bps.check_name      = strcheckName;
        bps.bank_code       = strbankCode;
        bps.type_name       = strtypeName;

        bps.api_encode  =     api_encode;
        bps.field_status = "ForUploading";
        bps.is_sent = 0;

        bps.save();
        //showDialogAlert("Transactions Saved","Queued");
        processWorkFlow(bps);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.billspay, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        //TODO check day if day < 10 add 0 prefix.

        if(datePickerDialog == datePickerDialogFrom){
            String dayString = day + "";
            String monString = month + "";
            if(day < 10){
                dayString = "0" + day;
            }

            if(month < 9){
                month += 1;
                monString = "0" + month;
            }else{
                monString = String.valueOf(month + 1);
            }

            //mDepositDate = (month + 1) + "-" + dayString + "-" + year;
            strfromDate = (monString) + "/" + dayString + "/" + year;
            mtt_fromDate.setText(strfromDate);

            return;
        }

        if(datePickerDialog == datePickerDialogTo){

            String dayString = day + "";
            String monString = month + "";
            if(day < 10){
                dayString = "0" + day;
            }

            if(month < 9){
                month += 1;
                monString = "0" + month;
            }else{
                monString = String.valueOf(month + 1);
            }

            //mDepositDate = (month + 1) + "-" + dayString + "-" + year;
            strtoDate = (monString) + "/" + dayString + "/" + year;

            mtt_toDate.setText(strtoDate);

            return;
        }


        if(datePickerDialog == datePickerDialogCheck){
            String dayString = day + "";
            String monString = month + "";
            if(day < 10){
                dayString = "0" + day;
            }

            if(month < 9){
                month += 1;
                monString = "0" + month;
            }else{
                monString = String.valueOf(month + 1);
            }


            strcheckDate = (monString) + "-" + dayString + "-" + year;

            mtt_checkDate.setText(strcheckDate);
        }
    }


}
