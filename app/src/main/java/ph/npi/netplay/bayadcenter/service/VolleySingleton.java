package ph.npi.netplay.bayadcenter.service;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import ph.npi.netplay.bayadcenter.BayadApplication;


/**
 * Created by Rhen Nepacena on 4/3/2015.
 */
public class VolleySingleton {

    private static VolleySingleton sInstance = null;

    private RequestQueue mRequestQueue;

    private VolleySingleton(){
        mRequestQueue = Volley.newRequestQueue(BayadApplication.getAppContext());
    }

    public static VolleySingleton getsInstance(){
        if(sInstance == null){
            sInstance = new VolleySingleton();
        }
        return sInstance;
    }

    public RequestQueue getRequestQueue(){
        return mRequestQueue;
    }
}
