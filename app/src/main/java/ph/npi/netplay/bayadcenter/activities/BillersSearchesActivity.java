package ph.npi.netplay.bayadcenter.activities;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.adapters.BillersSearchAdapter;
import ph.npi.netplay.bayadcenter.libs.syncdata.SyncBillers;
import ph.npi.netplay.bayadcenter.libs.syncdata.SyncInterface;
import ph.npi.netplay.bayadcenter.libs.syncdata.SyncPaymentTypes;
import ph.npi.netplay.bayadcenter.models.Billers;
import ph.npi.netplay.bayadcenter.models.Events;
import ph.npi.netplay.bayadcenter.models.PaymentTypes;


/**
 * Created by Rhen Nepacena on 10/10/2014.
 */
public class BillersSearchesActivity extends SherlockActivity {

    private ListView mlstvwBillers;
    private Context mContext;
    private EditText medtxtSearch;
    List<Billers> searchTemp = new ArrayList<Billers>();
    List<Billers> searchBillersList = new ArrayList<Billers>();
    List<Billers> newSearchBillersList = new ArrayList<Billers>();
    List<Billers> billers;
    List<String> searchContents = new ArrayList<String>();
    ArrayAdapter<Billers> billeradapter;
    private LinearLayout mNoBrandsLinearLayout;
    private LinearLayout mlnrlyoutbillers;
    private Menu menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billers);
        Events.inputEventLogs("Access Pay Bills");

        mContext = this;
        mlstvwBillers = (ListView) findViewById(R.id.lstvwBillers);
        mlnrlyoutbillers  = (LinearLayout) findViewById(R.id.lnrlyoutbillers);
        mNoBrandsLinearLayout = (LinearLayout) findViewById(R.id.noBrandsLinearLayout);
        mlstvwBillers.setVisibility(View.VISIBLE);

        getSupportActionBar().setIcon(getResources().getDrawable(R.drawable.ic_action_hardware_phone));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.searchlayout);

        View view = LayoutInflater.from(this).inflate(R.layout.searchbillerslayout, null);

        getSupportActionBar().setCustomView(view);

        medtxtSearch = (EditText) view.findViewById(R.id.searchNow);
        medtxtSearch.setFocusable(true);
        medtxtSearch.requestFocus();


        sync();
        getBillerList();

        medtxtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                String newText = charSequence.toString();
                newSearchBillersList.clear();
                if (newText.length() > 0) {
                    int j = 0;
                    boolean searchCheck = false;
                    String[] searchTemp = newText.split(" ");
                    for (String search : searchContents) {
                        for (String temp : searchTemp) {
                            temp = temp.replace(" ", "");
                            if (search.trim().toLowerCase().contains((temp.toLowerCase().replace(" ", "")))) {
                                searchCheck = true;
                            } else {
                                searchCheck = false;
                                break;
                            }
                        }

                        if (searchCheck) {
                            newSearchBillersList.add(searchBillersList.get(j));
                        }

                        j++;
                    }



                    //add to adapter the new list
                    billeradapter = new BillersSearchAdapter(mContext, newSearchBillersList);
                    mlstvwBillers.setAdapter(billeradapter);
                    mlstvwBillers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            /*Billers billerinfo = newSearchBillersList.get(i);
                            Intent intent = new Intent(BillersSearchesActivity.this, BillersPayActivity.class);
                            intent.putExtra("biller_name",billerinfo.name);
                            BillersSearchesActivity.this.startActivity(intent);*/

                            Billers bi = newSearchBillersList.get(i);
                            processBillerSelection(bi);



                        }
                    });
                    billeradapter.notifyDataSetChanged();

                } else {
                    searchContents.clear();
                    getBillerList();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });




    }

    private void getBillerList() {

        searchBillersList.clear();

        billers = new Billers().get();

        if (billers.size() <= 0) {
            mNoBrandsLinearLayout.setVisibility(View.VISIBLE);
            mlnrlyoutbillers.setVisibility(View.GONE);
            return;
        }


        mNoBrandsLinearLayout.setVisibility(View.GONE);
        mlnrlyoutbillers.setVisibility(View.VISIBLE);

        for(Billers bills: billers){
            searchBillersList.add(bills);
            searchContents.add((bills.name.toString().toLowerCase().replace(" ", "")));

        }


        billeradapter = new BillersSearchAdapter(mContext, searchBillersList);
        mlstvwBillers.setAdapter(billeradapter);
        mlstvwBillers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                /*Billers billerinfo = searchBillersList.get(i);
                Intent intent = new Intent(BillersSearchesActivity.this, BillersPayActivity.class);
                intent.putExtra("biller_name",billerinfo.name);
                BillersSearchesActivity.this.startActivity(intent);*/



                Billers bi = searchBillersList.get(i);
                processBillerSelection(bi);

            }
        });
        billeradapter.notifyDataSetChanged();


    }


    public void processBillerSelection(Billers bi){
        //hardcoded to be replaced soon...
        if((bi.name).equals("Meralco")){
            updateBillersCounting(bi,BillersPayMecoaActivity.class);
        }else if((bi.name).equals("Manila Water")){
            updateBillersCounting(bi,BillersPayMWActivity.class);
        }else if((bi.name).equals("PLDT")){
            updateBillersCounting(bi,BillersPayPLDTActivity.class);
        }else if((bi.name).equals("Sky Cable")){
            updateBillersCounting(bi,BillersPaySkyActivity.class);
        }else if((bi.name).equals("Bayan")){
            updateBillersCounting(bi,BillersPayBayanActivity.class);
        }else if((bi.name).equals("MCWD")){
            updateBillersCounting(bi,BillersPayMCWDActivity.class);
        }else if((bi.name).equals("MWCOM")){
            updateBillersCounting(bi,BillersPayMWCOMActivity.class);
        }else if((bi.name).equals("GLOBE")){
            updateBillersCounting(bi,BillersPayGlobeActivity.class);
        }else if((bi.name).equals("SMART")){
            updateBillersCounting(bi,BillersPaySmartActivity.class);
        }else if((bi.name).equals("SUNCL")){
            updateBillersCounting(bi,BillersPaySunActivity.class);
        }else if((bi.name).equals("DVolt")){
            updateBillersCounting(bi,BillersPayDVoltActivity.class);
        }else if((bi.name).equals("VIECO")){
            updateBillersCounting(bi,BillersPayViecoActivity.class);
        }else if((bi.name).equals("PHLTH")){
            updateBillersCounting(bi,BillersPayPhlthActivity.class);
        }else if((bi.name).equals("HDMF1")){
            updateBillersCounting(bi,BillersPayHMDFActivity.class);
        }else if((bi.name).equals("SSS")){
            updateBillersCounting(bi,BillersPaySSSActivity.class);
        }




        Events.inputEventLogs(bi.name+" Paybills Item Selected");


    }

    public void updateBillersCounting(Billers bi,Class mClass){
        Intent intent = null;
        Billers bi1 = bi.getBillersByCount(bi.name);
        if(bi1 != null){
            bi1.count +=1;
            Long checkUpdate = bi1.save();
            if(checkUpdate > 0){
                try{
                    intent = new Intent(mContext,mClass);
                    intent.putExtra("biller_name",bi.name);
                    BillersSearchesActivity.this.startActivity(intent);

                }catch (NullPointerException e){
                    e.printStackTrace();
                    return;
                }catch (ActivityNotFoundException e){
                    e.printStackTrace();
                    return;
                }
            }else{
                Toast.makeText(mContext,"Update Failed",Toast.LENGTH_SHORT).show();
            }
        }else{
            return;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.search, menu);

        return super.onCreateOptionsMenu(menu);
    }



    private void sync(){
        Billers biller = new Billers();
        if(biller.getBillersCount() <= 0){
            final ProgressDialog syncProgressDialog = new ProgressDialog(mContext);
            syncProgressDialog.setCancelable(false);
            syncProgressDialog.setMessage("Syncing data ...");
            syncProgressDialog.show();
            SyncBillers syncProducts = new SyncBillers(mContext, new SyncInterface() {
                @Override
                public void onComplete() {
                    syncProgressDialog.dismiss();

                }
            });
            syncProducts.execute(true);

        }

        PaymentTypes paymentTypes = new PaymentTypes();
        if(paymentTypes.getPaymentTypeCount() <= 0){

            SyncPaymentTypes syncPaymentTypes = new SyncPaymentTypes(mContext, new SyncInterface() {
                @Override
                public void onComplete() {


                }
            });
            syncPaymentTypes.execute(true);
        }


    }




}
