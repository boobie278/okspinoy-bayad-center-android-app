package ph.npi.netplay.bayadcenter.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

import java.util.List;

import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.adapters.EventsAdapter;
import ph.npi.netplay.bayadcenter.models.Events;


/**
 * Created by Rhen Nepacena on 6/8/2015.
 */
public class EventLogsActivity extends SherlockFragmentActivity {

    private Context mContext;
    private List<Events> mEventList;
    private EventsAdapter mEventAdapter;
    private ProgressDialog mProgressDialog;

    private ListView mtransactionsListView;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);

        Events.inputEventLogs("Access Event Logs");


        mContext = this;

        getSupportActionBar().setIcon(getResources().getDrawable(R.drawable.ic_action_list));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Access Logs");


        Events events = new Events();
        mtransactionsListView = (ListView) findViewById(R.id.transactionsListView);
        mEventList =  events.getEventList();
        mEventAdapter = new EventsAdapter(mContext,R.layout.listview_event_logs,mEventList);
        mtransactionsListView.setAdapter(mEventAdapter);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Events.inputEventLogs("Exit Event Logs");
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
