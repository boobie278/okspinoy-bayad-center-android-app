package ph.npi.netplay.bayadcenter.models;

import com.activeandroid.Cache;
import com.activeandroid.Model;
import com.activeandroid.TableInfo;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Table(name = "billspay")
public class BillsPay extends Model {

    public static final int SMS_PENDING_REPLY = 80;
    public static final int SMS_REPLY_RECEIVED = 81;

    @Column(name = "billspayid" ,notNull = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String billspayid;

    @Column(name = "createdDate")
    public Date createdDate;

    @Column(name = "time_sent")
    public String time_sent;

    @Column(name = "external_reference")
    public String external_reference;



    //Biller Info
    @Column(name = "billername")
    public String billername;

    @Column(name = "biller_code")
    public String biller_code;


    //Basic Details
    @Column(name = "account_no")
    public String account_no;

    @Column(name = "amount_due")
    public String amount_due;

    @Column(name = "amount_paid")
    public String amount_paid;

    @Column(name = "amount")
    public String amount;

    @Column(name = "service_code")
    public String service_code;

    @Column(name = "other_charges")
    public String other_charges;

    @Column(name = "mode_of_payment")
    public String mode_of_payment;


    //Bayan Additional comp_api
    @Column(name = "customer_name")
    public String customer_name;

    @Column(name = "phone_no")
    public String phone_no;

    //DVOLT Additional comp_api
    @Column(name = "last_name")
    public String last_name;

    @Column(name = "first_name")
    public String first_name;

    //GLOBE Additional comp_api
    @Column(name = "account_name")
    public String account_name;


    //MWCD1 Additional comp_api
    @Column(name = "to_payonly")
    public String to_payonly;

    //MWSI Additional comp_api
    @Column(name = "rap")
    public String rap;

    //SKY Additional comp_api
    @Column(name = "service_type")
    public String service_type;

    @Column(name = "from_date")
    public String from_date;

    @Column(name = "to_date")
    public String to_date;

    //SMART Additional comp_api
    @Column(name = "product")
    public String product;

    @Column(name = "service_reference_no")
    public String service_reference_no;


    //PHILHEALTH Additional comp_api
    @Column(name = "middle_initial")
    public String middle_initial;

    @Column(name = "member_type")
    public String member_type;

    @Column(name = "phil_payment_type")
    public String phil_payment_type;


    //SSS Additional comp_api
    @Column(name = "sss_payment_type")
    public String sss_payment_type;

    @Column(name = "sss_amount")
    public String sss_amount;

    @Column(name = "sss_flexifund")
    public String sss_flexifund;

    @Column(name = "sss_loan_type")
    public String sss_loan_type;

    @Column(name = "payor_type")
    public String payor_type;

    @Column(name = "loan_account_no")
    public String loan_account_no;

    @Column(name = "company_name")
    public String company_name;

    @Column(name = "sss_ec_amount")
    public String sss_ec_amount;






    //Payment details - check || cash
    @Column(name = "payment_type")
    public String payment_type;

    @Column(name = "check_no")
    public String check_no;

    @Column(name = "check_date")
    public String check_date;

    @Column(name = "bank_name")
    public String bank_name;

    @Column(name = "branch")
    public String branch;

    @Column(name = "check_amount")
    public String check_amount;

    @Column(name = "check_name")
    public String check_name;

    @Column(name = "bank_code")
    public String bank_code;

    @Column(name = "type_name")
    public String type_name;





    //Billspay transaction status - api related concern
    @Column(name = "failed_transaction")
    public int failed_transaction;

    @Column(name = "isSuccessful")
    public boolean isSuccessful;

    @Column(name = "api_start")
    public String api_start;

    @Column(name = "api_end")
    public String api_end;

    @Column(name = "time_received")
    public String time_received;

    @Column(name = "is_sent")
    public int is_sent;

    @Column(name = "status")
    public int status;

    @Column(name = "field_status")
    public String field_status;

    @Column(name = "error_message")
    public String error_message;

    @Column(name = "telcorefno")
    public String telcorefno;

    @Column(name = "response")
    public String response;

    @Column(name = "remarks")
    public String remarks;

    @Column(name = "api_encode")
    public String api_encode;






    private Date stringToDate(String mysqlTimestamp) {
        Date date = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd kk:mm:ss");

            long longDate = dateFormat.parse(mysqlTimestamp).getTime();

            date = new Date(longDate);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }

    public BillsPay getByBillspayid(String billspayid) {
        return new Select().from(BillsPay.class).where("billspayid = ?", billspayid).executeSingle();
    }

    public List<BillsPay> getTransactions(int limit, int offset) {
        return new Select().from(BillsPay.class).orderBy("createdDate DESC").limit(limit).offset(offset).execute();
    }

    public List<BillsPay> getTransactions() {
        return new Select().from(BillsPay.class).orderBy("createdDate DESC").execute();
    }

    public List<BillsPay> getQueuedTransactions(){
        return new Select().from(BillsPay.class).where("field_status = 'ForUploading'").execute();
    }

    public Collection<Field> getColumnName(){
        TableInfo info =Cache.getTableInfo(BillsPay.class);
        Collection<Field> fields = info.getFields();
        Set<String> columnNames = new HashSet<String>();

        return fields;
    }
}
