package ph.npi.netplay.bayadcenter.libs.syncdata;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.models.Billers;


/**
 * Created by Rhen Nepacena  on 10/10/14.
 */
public class SyncBillers {

    private final SyncInterface mSyncInterface;
    private Context mContext;
    private ProgressDialog mProgressDialog;
    //private List<EpinoyloadApi> mApiCallList;
    private boolean mSyncError = false;
    //private EpinoyloadApi epinoyloadApi;

    public SyncBillers(Context context, SyncInterface syncInterface) {
        mContext = context;
        mSyncInterface = syncInterface;
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Syncing ....");
    }

    public void execute(boolean isOffline) {
        if (!isOffline) {
            //execute();
            return;
        }

        mProgressDialog.show();

        String billersDataString = readTxt(R.raw.billers);
        JsonObject billersData = new JsonParser().parse(billersDataString.trim()).getAsJsonObject();

        JsonArray a_billers = billersData.get("billers").getAsJsonArray();

        deleteBillers();
        ActiveAndroid.beginTransaction();
        try {
            for (int i = 0; i < a_billers.size(); i++) {
                JsonObject o_billers = a_billers.get(i).getAsJsonObject();
                Billers billers = new Billers();
                billers.name = o_billers.get("name").getAsString();
                billers.field = o_billers.get("field").getAsString();
                billers.type = o_billers.get("type").getAsString();
                billers.bilcode = o_billers.get("id").getAsString();
                billers.count = o_billers.get("count").getAsInt();
                billers.label = o_billers.get("label").getAsString();

                billers.save();
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }



        mProgressDialog.dismiss();

        mSyncInterface.onComplete();
    }





    private void deleteBillers() {
        new Delete().from(Billers.class).execute();
    }





    private String readTxt(int rawResource) {

        InputStream inputStream = mContext.getResources().openRawResource(rawResource);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int i;
        try {
            i = inputStream.read();
            while (i != -1) {
                byteArrayOutputStream.write(i);
                i = inputStream.read();
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return byteArrayOutputStream.toString();
    }

}
