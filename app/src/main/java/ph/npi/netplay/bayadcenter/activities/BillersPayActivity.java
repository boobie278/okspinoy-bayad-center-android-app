package ph.npi.netplay.bayadcenter.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.activeandroid.query.Select;
import com.fourmob.datetimepicker.date.DatePickerDialog;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.JoseException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import ph.npi.netplay.bayadcenter.Epinoyload;
import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.adapters.PaymentTypeAdapter;
import ph.npi.netplay.bayadcenter.models.Billers;
import ph.npi.netplay.bayadcenter.models.BillsPay;
import ph.npi.netplay.bayadcenter.models.Events;
import ph.npi.netplay.bayadcenter.models.PaymentTypes;
import ph.npi.netplay.bayadcenter.models.UtilMapSerializer;
import ph.npi.netplay.bayadcenter.service.WebServiceResponds;

/**
 * Created by Rhen Nepacena on 11/8/2015.
 */

public class BillersPayActivity extends SherlockFragmentActivity implements  DatePickerDialog.OnDateSetListener{

    private com.zkc.pc700.helper.SerialPort mSerialPort ;
    String thread = "readThread";
    String choosed_serial ="/dev/ttySAC3";
    int choosed_buad = 38400;

    protected static final String DATE_PICKER_TAG = "datepicker";
    protected EditText med_accountnumber;
    protected EditText med_amount;
    protected EditText med_checkNumber;
    protected EditText med_bankName;
    protected EditText med_branch;
    protected EditText med_checkAmount;
    protected EditText med_checkName;
    protected EditText med_bankCode;
    protected TextView mtt_checkDate;
    protected Spinner mspnr_checkType;
    protected Spinner mspnr_paymentType;
    protected LinearLayout mll_paymentDetails;

    protected Context mContext;

    protected Billers biller;
    protected List<PaymentTypes> l_paymentTypes;
    protected PaymentTypeAdapter a_paymentTypes;
    protected String intentextras;

    protected static String strPaymentType;
    protected static String strcheckNumber;
    protected static String strcheckDate;
    protected static String strbankName;
    protected static String strbranch;
    protected static String strcheckAmount;
    protected static String strcheckName;
    protected static String strbankCode;
    protected static String strtypeName;

    private String bearer_token = null;
    private String responds = null;
    private String external_id;

    protected DatePickerDialog datePickerDialogCheck;

    public static final String BAYADCENTER_BILLSPAY = "http://125.5.114.138:50005/bayadcenter/v1/transactions/billspay";

    protected void initializeCheckComponents(){

        final Calendar calendar = Calendar.getInstance();
        datePickerDialogCheck = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);

        med_checkNumber     = (EditText) findViewById(R.id.ed_checkNumber);
        mtt_checkDate       = (TextView) findViewById(R.id.tt_checkDate);
        med_bankName        = (EditText) findViewById(R.id.ed_bankName);
        med_branch          = (EditText) findViewById(R.id.ed_branch);
        med_checkAmount     = (EditText) findViewById(R.id.ed_checkAmount);
        med_checkName       = (EditText) findViewById(R.id.ed_checkName);
        med_bankCode        = (EditText) findViewById(R.id.ed_bankCode);
        mspnr_checkType     = (Spinner) findViewById(R.id.spnr_checkType);

        mll_paymentDetails  = (LinearLayout) findViewById(R.id.ll_paymentDetails);

        mtt_checkDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialogCheck.setYearRange(2014, 2037);
                datePickerDialogCheck.show(getSupportFragmentManager(), DATE_PICKER_TAG);

            }
        });

        mspnr_checkType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        strtypeName = "Local";
                        break;
                    case 1:
                        strtypeName = "Regional";
                        break;
                    case 3:
                        strtypeName = "Managers Check";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    protected void checkValues(){


        strcheckNumber      = med_checkNumber.getText().toString();
        strbankName         = med_bankName.getText().toString();
        strbranch           = med_branch.getText().toString();
        strcheckAmount      = med_checkAmount.getText().toString();
        strcheckName        = med_checkName.getText().toString();
        strbankCode         = med_bankCode.getText().toString();

    }

    protected void populateSpinner(){

        mspnr_paymentType  = (Spinner) findViewById(R.id.spnr_paymentType);

        l_paymentTypes = new PaymentTypes().getPaymentTypeList();

        if(!(l_paymentTypes.isEmpty())){

            a_paymentTypes =new PaymentTypeAdapter(this,R.layout.listview_paymenttype,l_paymentTypes);
            mspnr_paymentType.setAdapter(a_paymentTypes);


            mspnr_paymentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    PaymentTypes paymentTypes = l_paymentTypes.get(i);
                    if(paymentTypes.code.equals("cash")){
                        mll_paymentDetails.setVisibility(View.GONE);
                    }

                    strPaymentType = paymentTypes.name;
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }

    protected void showDialogAlert(String message,String title) {

        AlertDialog alertDialog = new AlertDialog.Builder(BillersPayActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("isBiller",true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return;
            }
        });
        alertDialog.show();
    }

    protected void showOptionDialog(String title, String message,final BillsPay bp){
        AlertDialog alertDialog = new AlertDialog.Builder(BillersPayActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
                return;
            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "PRINT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                printReceipt(bp);
                dialogInterface.dismiss();
                finish();
                return;
            }
        });
        alertDialog.show();
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        //TODO check day if day < 10 add 0 prefix.

        if(datePickerDialog == datePickerDialogCheck){
            String dayString = day + "";
            String monString = month + "";
            if(day < 10){
                dayString = "0" + day;
            }

            if(month < 9){
                month += 1;
                monString = "0" + month;
            }else{
                monString = String.valueOf(month + 1);
            }


            strcheckDate = (monString) + "/" + dayString + "/" + year;

            mtt_checkDate.setText(strcheckDate);
        }
    }

   private String getExternalID(){
       String result = null;
       try{
           Date dates = new Date();
           Random r = new Random();
           long i1 = r.nextInt(99999999) + 6500;

           result = String.valueOf(dates.getTime() + i1);
       }catch (Exception e){
           result = "000000000";
       }

      return result;
   }


    protected void processWorkFlow(BillsPay billsPay){
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(true);
        pd.show();

        external_id = getExternalID();

        //ForUploading
        Long res = updateBillspayStatus(billsPay, "Uploaded"," "," "," ",external_id);
        String resString = res.toString();

        // res always returns the billspay id

        if(resString.equals(billsPay.getId().toString())) {
            //proceed
        } else {
            pd.dismiss();
            return;
        }

        final BillsPay bp1 = getBillspay(billsPay.getId(), "Uploaded");
        if(bp1 != null) {
            //proceed
        } else {
            pd.dismiss();
            return;
        }

        new AsyncTask<String,String,String>(){

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... strings) {
                String result = null;
                try{


                    bearer_token = getAuthorizationBearerToken(external_id);

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(BAYADCENTER_BILLSPAY);
                    httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
                    httpPost.addHeader("Authorization", "Bearer " + bearer_token);

                    //hardcoded values need to be recode if  production purposes
                    List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);

                    UtilMapSerializer utilMapSerializer = new UtilMapSerializer();
                    Map<String,String> api_encode = utilMapSerializer.deserialize(bp1.api_encode);


                    for(Map.Entry<String,String> map_entry : api_encode.entrySet()){
                        String key = map_entry.getKey();
                        String value = map_entry.getValue();
                        Log.d("billerspayactivity-key",key);
                        Log.d("billerspayactivity-value",value);

                        nameValuePair.add(new BasicNameValuePair(key,value));
                    }

                        /*
                        *   check_no
                            check_date MM/dd/yyyy
                            bank_name
                            branch
                            amount – must include other charges value
                            name
                            bank_code
                            type_name – values are ‘Local’, ‘Regional’, ‘Managers Check’
                        *
                        * */

                    try {
                        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                    } catch (UnsupportedEncodingException e) {
                        // log exception
                        e.printStackTrace();
                    }


                    try {
                        HttpResponse response = httpClient.execute(httpPost);

                        Header[] headers = response.getAllHeaders();
                        for (Header header : headers) {
                            Log.d("syncqueuedtask-headers", "Key : " + header.getName() + " ,Value : " + header.getValue());
                        }

                        responds = WebServiceResponds.convertResponseToString(response);
                        Log.d("syncqueuedtask-resp",responds);

                        try{
                       /*
                         {
                            "external_reference":"139270898923931",
                            "result_code":"000",
                            "result_message":"Transaction successful.",
                            "transaction_no":"15308063240000197",
                            "request_timestamp":"11/04/2015 10:49:58 AM",
                            "response_timestamp":"11/04/2015 10:49:58 AM",
                            "wallet_balance":9985,"amount":"1.00"
                         }
                       */
                            /*
                             {
                              "amount":"0",
                              "result_message":"AccountNo is invalid.",
                              "result_code":"400","wallet_balance":9973,
                              "external_reference":"1446742437889",
                              "request_timestamp":"2015-11-05T13:50:13Z",
                              "transaction_no":null,
                              "response_timestamp":"2015-11-05T13:50:15Z"
                              }
                            */
                            JSONObject jsonObject = new JSONObject(responds);

                            if(jsonObject != null){
                                String strResultCode = jsonObject.getString("result_code");

                                if(strResultCode.contains("000") || strResultCode == "000"){

                                    BillsPay bp2 = getBillspay(bp1.getId(), "Uploaded");
                                    if(bp2 != null) {
                                        Log.d("syncqueuedtaskBillspay-success",strResultCode);
                                        String strBalance   = jsonObject.getString("wallet_balance");
                                        String strMessage   = jsonObject.getString("result_message");
                                        String strTrx_no    = jsonObject.getString("transaction_no");
                                        String strReqTimestamp = jsonObject.getString("request_timestamp");
                                        String strResTimestamp = jsonObject.getString("response_timestamp");
                                        String strExternalId   = jsonObject.getString("external_reference");

                                        bp2.telcorefno      = strTrx_no;
                                        bp2.response        = strMessage;
                                        bp2.remarks         = "Balance: " + strBalance;

                                        Float  fltBalance   = Float.parseFloat(strBalance);


                                        Epinoyload.setBalanceLastUpdate(getApplicationContext());
                                        Epinoyload.setBalance(getApplicationContext(), fltBalance, Epinoyload.KEY_LOAD_BAL);

                                        updateBillspayStatus(bp2, "Uploaded-Success"," ",strReqTimestamp,strResTimestamp,strExternalId);
                                        pd.dismiss();
                                        result = "SUCCESS - REF# "+strExternalId+" "+strMessage;
                                        //showOptionDialog("TRANSACTION COMPLETE", "Successful "+strExternalId+ " "+bp2.remarks,bp2);
                                    }
                                }else{

                                    String errorMessage = jsonObject.getString("result_message");
                                    String strReqTimestamp = jsonObject.getString("request_timestamp");
                                    String strResTimestamp   = jsonObject.getString("response_timestamp");

                                    BillsPay bp2 = getBillspay(bp1.getId(), "Uploaded");
                                    if(bp2 != null) {
                                        updateBillspayStatus(bp2, "Uploaded-Failed",errorMessage,strReqTimestamp,strResTimestamp,external_id);
                                    } //end if

                                    pd.dismiss();
                                    result = "FAILED -"+errorMessage;
                                    //showDialogAlert("TRANSACTION FAILED ", "ERROR " + errorMessage);
                                    Log.i("syncqueuedtask-processBillersFailed", errorMessage);

                                }//end else strResultCode

                            }else{
                                String errorMessage = jsonObject.getString("result_message");
                                String strReqTimestamp = jsonObject.getString("request_timestamp");
                                String strResTimestamp   = jsonObject.getString("response_timestamp");

                                BillsPay bp2 = getBillspay(bp1.getId(), "Uploaded");
                                if(bp2 != null) {
                                    updateBillspayStatus(bp2, "Uploaded-Failed",errorMessage,strReqTimestamp,strResTimestamp,external_id);
                                } //end if
                                pd.dismiss();
                                result = "FAILED -"+errorMessage;
                                Log.i("syncqueuedtask-processBillersFailed", errorMessage);
                                //showDialogAlert("TRANSACTION FAILED ","ERROR "+errorMessage);

                            }//end else !jsonObject


                        } catch (Exception e){

                            String errorMessage = e.getMessage();

                            BillsPay bp2 = getBillspay(bp1.getId(), "Uploaded");
                            if(bp2 != null) {
                                updateBillspayStatus(bp2, "Uploaded-Failed",errorMessage," "," ",external_id);
                            } //end if
                            pd.dismiss();
                            result = "FAILED -"+errorMessage;
                            //showDialogAlert("TRANSACTION FAILED ","ERROR "+errorMessage);
                        }

                    } catch (ClientProtocolException e) {
                        // Log exception
                        e.printStackTrace();
                        String errorMessage = e.getMessage();

                        BillsPay bp2 = getBillspay(bp1.getId(), "Uploaded");
                        if(bp2 != null) {
                            updateBillspayStatus(bp2, "Uploaded-Failed",errorMessage," "," ",external_id);
                        } //end if

                        pd.dismiss();
                        result = "FAILED -"+errorMessage;
                        //showDialogAlert("TRANSACTION FAILED ","ERROR "+errorMessage);
                    } catch (IOException e) {
                        // Log exception
                        e.printStackTrace();
                        String errorMessage = e.getMessage();

                        BillsPay bp2 = getBillspay(bp1.getId(), "Uploaded");
                        if(bp2 != null) {
                            updateBillspayStatus(bp2, "Uploaded-Failed",errorMessage," "," ",external_id);
                        } //end if
                        pd.dismiss();
                        result = "FAILED -"+errorMessage;
                        //showDialogAlert("TRANSACTION FAILED ","ERROR "+errorMessage);
                    }
                }catch (Exception e){
                    String errorMessage = e.getMessage();

                    BillsPay bp2 = getBillspay(bp1.getId(), "Uploaded");
                    if(bp2 != null) {
                        updateBillspayStatus(bp2, "Uploaded-Failed",errorMessage," "," ",external_id);
                    }
                    pd.dismiss();
                    result = "FAILED - "+errorMessage;
                    //
                }
                return result;
            }

            @Override
            protected void onPostExecute(String responds) {
                super.onPostExecute(responds);

                if(responds.contains("FAILED")){
                    showDialogAlert("TRANSACTION FAILED ",responds);
                }else if(responds.contains("SUCCESS")){
                    showOptionDialog("TRANSACTION COMPLETE", responds,bp1);
                }

            }

            @Override
            protected void onCancelled() {
                super.onCancelled();
            }
        }.execute();
    }


    private void printReceipt(BillsPay bp){

        Events.inputEventLogs("Print Receipt Start");

        Calendar calendar = Calendar.getInstance();
        String sentDate = (new SimpleDateFormat("yyyy-MM-dd"))
                .format(calendar.getTime());
        String sentTime = (new SimpleDateFormat("HH:mm:ss"))
                .format(calendar.getTime());

        String str = "";

        StringBuilder sb = new StringBuilder();

        sb.append("\n\n");
        sb.append("       Bayad Center       ");
        sb.append("\n\n");
        sb.append("CUSTOMER REF: ");
        sb.append(bp.billspayid);
        sb.append("\n\n");
        sb.append("ACT NO: ");
        sb.append(bp.account_no);
        sb.append("\n\n");
        sb.append("BILLER NAME: ");
        sb.append(bp.billername);
        sb.append("\n\n");
        sb.append("TOTAL AMOUNT PAID:  ");
        sb.append(bp.amount);
        sb.append("\n\n");
        sb.append("TYPE OF PAYMENT: ");
        sb.append(bp.mode_of_payment);
        sb.append("\n\n");
        sb.append("\n\n");
        sb.append("DATE: ");
        sb.append(sentDate);
        sb.append(" TIME: ");
        sb.append(sentTime);
        sb.append("\n\n");
        sb.append("Donnalyn B.");
        sb.append("\n\n");
        sb.append("\n\n");
        sb.append("\n\n");



        str = String.valueOf(sb);


        try {

            mSerialPort = new com.zkc.pc700.helper.SerialPort(choosed_serial,
                    choosed_buad, 0);

        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (mSerialPort != null) {

            new readThread().start();

        }


        byte[] buffer = null;

        try {
            buffer = str.getBytes("GBK");
        } catch (Exception e) {
            buffer = str.getBytes();
        }

        try {
            mSerialPort.getOutputStream().write(buffer);

        } catch (Exception e) {
            // TODO: handle exception
        }

        mSerialPort.close();

        Events.inputEventLogs("Print Receipt End");
    }

    class readThread extends Thread {

        @Override
        public void run() {
            super.run();
            while (!isInterrupted()) {
                int size;
                try {
                    byte[] buffer = new byte[300];
                    if (mSerialPort.getInputStream() == null)
                        return;
                    size = mSerialPort.getInputStream().read(buffer);
                    if (size > 0) {
                        //onDataReceived(buffer, size);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }
    }


    protected Long updateBillspayStatus(BillsPay bp, String status,String responses,String start, String end,String external_id) {

        Calendar calendar = Calendar.getInstance();
        String timeAcquired = (new SimpleDateFormat("HH:mm:ss"))
                .format(calendar.getTime());

        if(status.equals("Uploaded")) {
            bp.is_sent      = 1;
        } else if(status.equals("Uploaded-Success")) {
            bp.time_received = timeAcquired;
            bp.isSuccessful = true;
            bp.api_start  = start;
            bp.api_end  = end;
            bp.external_reference = external_id;
        } else if(status.equals("Uploaded-Failed")) {
            bp.time_received = timeAcquired;
            bp.isSuccessful = false;
            bp.error_message = responses;
            bp.failed_transaction = 1;
            bp.api_start  = start;
            bp.api_end  = end;
            bp.external_reference = external_id;
        }

        bp.field_status = status;
        Long res = bp.save();

        String resString = res.toString();

        Log.i("syncqueuedtaskBillspayStatus-DBSAVE", resString);
        Log.i("syncqueuedtaskBillspayStatus-id",String.valueOf(bp.getId()));
        Log.i("syncqueuedtaskBillspayStatus-biller",String.valueOf(bp.account_no));
        Log.i("syncqueuedtaskBillspayStatus-status",String.valueOf(bp.field_status));
        Log.i("syncqueuedtaskBillspayStatus-received",String.valueOf(bp.time_received));
        Log.i("syncqueuedtaskBillspayStatus-sent",String.valueOf(bp.time_sent));

        return res;
    }


    protected BillsPay getBillspay(Long id, String status) {
        BillsPay bp = new Select().from(BillsPay.class).where("id = ? AND field_status = '" + status + "' ", id).executeSingle();
        return bp;
    }

    private String getAuthorizationBearerToken(String external_id) throws NoSuchAlgorithmException, JoseException, UnsupportedEncodingException{

        JwtClaims claims = new JwtClaims();

        claims.setIssuedAtToNow();
        claims.setClaim("username","bc_android");
        claims.setClaim("external_reference",external_id);
        claims.setClaim("ip","127.0.0.1");

        String secret_key = "29a9d233e48ffa699c3c1f991943a5518f9123db";
        Key key = new HmacKey(secret_key.getBytes("UTF-8"));

        JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());

        jws.setKey(key);
        jws.setHeader("typ","JWT");
        jws.setHeader("alg","HS256");
        jws.setDoKeyValidation(false);

        jws.setPayload(claims.toJson());
        String result = jws.getCompactSerialization();

        return result;
    }
}
