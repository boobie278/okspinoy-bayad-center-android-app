package ph.npi.netplay.bayadcenter.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.libs.prettydate.PrettyDate;
import ph.npi.netplay.bayadcenter.models.BillsPay;
import ph.npi.netplay.bayadcenter.models.Transaction;

/**
 * Created by Rhen Nepacena on 10/15/14.
 */
public class BillsPayTransactionsAdapter extends ArrayAdapter<BillsPay> {

    private Context mContext;
    private BillsPay mbillspaytransact;

    public BillsPayTransactionsAdapter(Context context, List<BillsPay> objects) {
        super(context, 0 , objects);
        this.mContext = context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);

        convertView = inflater.inflate(R.layout.listview_transaction, null);

        mbillspaytransact = getItem(position);

        TextView transactionDateTextView = (TextView) convertView.findViewById(R.id.transactionDateTextView);
        TextView ttbillerAccountNo = (TextView) convertView.findViewById(R.id.ttbillerAccountNo);
        TextView ttbillerAmount    = (TextView) convertView.findViewById(R.id.ttbillerAmount);
        TextView ttbillerName      = (TextView) convertView.findViewById(R.id.ttbillerName);
        TextView ttBillerRefNo     = (TextView) convertView.findViewById(R.id.ttBillerRefNo);
        TextView ttTimeSent        = (TextView) convertView.findViewById(R.id.ttTimeSent);
        TextView ttTimeReceived    = (TextView) convertView.findViewById(R.id.ttTimeReceived);
        TextView ttErrorMsg        = (TextView) convertView.findViewById(R.id.ttErrorMsg);

        TextView ttApiStart        = (TextView) convertView.findViewById(R.id.ttApiStart);
        TextView ttApiEnd          = (TextView) convertView.findViewById(R.id.ttApiEnd);



        ImageView transactionAcceptImageView = (ImageView) convertView.findViewById(R.id.transactionAcceptImageView);
        ImageView transactioWarningImageView = (ImageView) convertView.findViewById(R.id.transactioWarningImageView);
        ImageView transactionSentImageView = (ImageView) convertView.findViewById(R.id.transactionSentImageView);

        String info = mbillspaytransact.time_received;

        if(info == null){
            info = "00:00:00";
        }

        String errmsg = mbillspaytransact.error_message;

        if(errmsg == null){
            errmsg = " ";
        }

        String apiStart = mbillspaytransact.api_start;

        if(apiStart == null){
            apiStart = " ";
        }else{
            apiStart = "API start "+ mbillspaytransact.api_start;
        }

        String apiEnd = mbillspaytransact.api_end;

        if(apiEnd == null){
            apiEnd = " ";
        }else{
            apiEnd = "API end "+mbillspaytransact.api_end;
        }

        transactionDateTextView.setText(new PrettyDate(mbillspaytransact.createdDate).toString());
        ttbillerAccountNo.setText(mbillspaytransact.account_no);
        ttbillerAmount.setText(mbillspaytransact.amount);
        ttbillerName.setText(mbillspaytransact.billername);
        ttBillerRefNo.setText(mbillspaytransact.external_reference);
        ttTimeSent.setText("Requested "+mbillspaytransact.time_sent);
        ttTimeReceived.setText("Finished " + info);
        ttErrorMsg.setText(errmsg);
        ttApiStart.setText(apiStart);
        ttApiEnd.setText(apiEnd);

//        transactionBillerTextView.setText( " "+info);
//        transactionBillerTextView.setText(mbillspaytransact.billername);
//        transactionMobileNumberTextView.setText(mbillspaytransact.accountno);
//        transactionReferenceNoTextView.setText(mbillspaytransact.telcorefno);



        if (mbillspaytransact.isSuccessful == true) {
            transactionAcceptImageView.setVisibility(View.VISIBLE);
        } else if (mbillspaytransact.status == Transaction.SMS_PENDING_REPLY) {
            transactionSentImageView.setVisibility(View.VISIBLE);
        } else if ((mbillspaytransact.is_sent == 0) && (mbillspaytransact.failed_transaction == 0)) {
            transactioWarningImageView.setVisibility(View.GONE);
            transactionSentImageView.setVisibility(View.GONE);
        } else if (mbillspaytransact.failed_transaction == 1){
            transactioWarningImageView.setVisibility(View.VISIBLE);
        }

        return convertView;
    }
}
