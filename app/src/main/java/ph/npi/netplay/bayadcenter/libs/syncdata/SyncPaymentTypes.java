package ph.npi.netplay.bayadcenter.libs.syncdata;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.models.PaymentTypes;


/**
 * Created by Rhen Nepacena  on 10/10/14.
 */
public class SyncPaymentTypes {

    private final SyncInterface mSyncInterface;
    private Context mContext;
    private ProgressDialog mProgressDialog;
    //private List<EpinoyloadApi> mApiCallList;
    private boolean mSyncError = false;
    //private EpinoyloadApi epinoyloadApi;

    public SyncPaymentTypes(Context context, SyncInterface syncInterface) {
        mContext = context;
        mSyncInterface = syncInterface;
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Syncing ....");
    }

    public void execute(boolean isOffline) {
        if (!isOffline) {
            //execute();
            return;
        }

        mProgressDialog.show();

        String payTypesDataString = readTxt(R.raw.payments);
        JsonObject payTypesData = new JsonParser().parse(payTypesDataString.trim()).getAsJsonObject();

        JsonArray a_paymentTypes = payTypesData.get("payments").getAsJsonArray();

        deletePayments();
        ActiveAndroid.beginTransaction();
        try {
            for (int i = 0; i < a_paymentTypes.size(); i++) {
                JsonObject o_paymentTypes = a_paymentTypes.get(i).getAsJsonObject();
                PaymentTypes paymentTypes = new PaymentTypes();
                paymentTypes.pk_id = o_paymentTypes.get("id").getAsInt();
                paymentTypes.name = o_paymentTypes.get("name").getAsString();
                paymentTypes.code = o_paymentTypes.get("code").getAsString();

                paymentTypes.save();
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }



        mProgressDialog.dismiss();

        mSyncInterface.onComplete();
    }

    /*public void execute() {
        mProgressDialog.show();

        epinoyloadApi = new EpinoyloadApi(mContext);
        epinoyloadApi.load(EpinoyloadApi.URL_BILLERS, null, new SyncBillersCallback());
    }
*/


    private void deletePayments() {
        new Delete().from(PaymentTypes.class).execute();
    }



    /*private class SyncBillersCallback implements EpinoyloadApi.EpinoyloadApiCallback {

        @Override
        public void onStart() {

        }

        @Override
        public void onFinish() {

        }

        @Override
        public void onSuccess(JsonObject data) {

            JsonArray a_paymentType = data.get("payments").getAsJsonArray();

            deletePayments();

            ActiveAndroid.beginTransaction();

            mApiCallList = new ArrayList<EpinoyloadApi>();
            epinoyloadApi.setmIsFinished(true);

            try {
                for(int i = 0; i <a_paymentType.size(); i++){
                    JsonObject o_paymentType = a_paymentType.get(i).getAsJsonObject();
                    PaymentTypes paymentTypes = new PaymentTypes();
                    paymentTypes.pk_id = o_paymentType.get("id").getAsInt();
                    paymentTypes.name = o_paymentType.get("name").getAsString();
                    paymentTypes.code = o_paymentType.get("code").getAsString();

                    mApiCallList.add(epinoyloadApi);

                    paymentTypes.save();
                }



                ActiveAndroid.setTransactionSuccessful();
            }catch (Exception e){

            }finally {
                ActiveAndroid.endTransaction();
            }





            checkFinished();


        }

        @Override
        public void onError(String errorMessage) {
            mProgressDialog.dismiss();

        }
    }*/



    /*private void stopAllApiCalls() {
        if (mSyncError == false) {
            mSyncError = true;
            if (mApiCallList != null) {
                for (EpinoyloadApi epinoyloadApi : mApiCallList) {
                    epinoyloadApi.stop();
                }
                deletePayments();

            }
            Epinoyload.showAlert(mContext, "Error", "There was an error with syncing. Please try again.");
            mProgressDialog.dismiss();
            mSyncInterface.onComplete();
        }
    }*/

    /*private void checkFinished() {
        int count = 0;
        for (EpinoyloadApi epinoyloadApi : mApiCallList) {
            if (epinoyloadApi.ismIsFinished()) {
                count++;

            }

            Log.i("incheckfin",String.valueOf(count));
        }
        if (count >= mApiCallList.size()) {
            mSyncInterface.onComplete();
            Epinoyload.showAlert(mContext, "Sync Successful!", "Your device has been successfully synced.");
            mProgressDialog.dismiss();
        }
    }*/

    private String readTxt(int rawResource) {

        InputStream inputStream = mContext.getResources().openRawResource(rawResource);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int i;
        try {
            i = inputStream.read();
            while (i != -1) {
                byteArrayOutputStream.write(i);
                i = inputStream.read();
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return byteArrayOutputStream.toString();
    }

}
