package ph.npi.netplay.bayadcenter.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.models.Events;


/**
 * Created by Rhen Nepacena on 6/8/2015.
 */
public class EventsAdapter extends BaseAdapter {

    private List<Events> data;
    private LayoutInflater mInflater;
    private Context mContext;
    private int resource;
    private String strEventLogs;
    private String strEventDate;

    public EventsAdapter(Context context, int resource, List<Events> data){

        mInflater            = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext        = context;
        this.data            = data;
        this.resource        = resource;

    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view == null){
            view   = mInflater.inflate(resource,null);
            holder = new ViewHolder();
            holder.textview   = (TextView) view.findViewById(R.id.tteventLogs);
            holder.textview01 = (TextView) view.findViewById(R.id.tteventDate);

            holder.textview.setPadding(2,2,2,2);
            holder.textview01.setPadding(2,2,2,2);

            view.setTag(holder);

        }else{
            holder   = (ViewHolder) view.getTag();
        }

        if(position < 0 || position >= data.size()){
            strEventLogs = "Unable to Display Logs";
            strEventDate = "01-01-2015 00:00:00";
        }else{
            strEventLogs = data.get(position).event_logs;
            strEventDate = data.get(position).event_date;
        }



        holder.textview.setText(strEventLogs);
        holder.textview01.setText(strEventDate);


        return view;
    }



    public static class ViewHolder{
        TextView textview;
        TextView textview01;
    }
}
