package ph.npi.netplay.bayadcenter.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.models.PaymentTypes;


/**
 * Created by Rhen Nepacena on 5/28/2015.
 */
public class PaymentTypeAdapter extends BaseAdapter {
    private List<PaymentTypes> data;
    private LayoutInflater mInflater;
    private Context mContext;
    private int resource;
    private String strOutput;

    public PaymentTypeAdapter(Context context, int resource, List<PaymentTypes> data){
        mInflater            = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext        = context;
        this.data            = data;
        this.resource        = resource;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view == null){

            view = mInflater.inflate(resource,null);
            holder = new ViewHolder();
            holder.textview = (TextView) view.findViewById(R.id.ttpaymentTypes);
            holder.textview.setPadding(5,5,5,5);

            view.setTag(holder);
        }else{
            holder   = (ViewHolder) view.getTag();
        }

        strOutput = data.get(position).name;

        holder.textview.setText(strOutput);

        return view;

    }


    public static class ViewHolder{
        TextView textview;
    }
}
