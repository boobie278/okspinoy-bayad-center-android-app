package ph.npi.netplay.bayadcenter.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.activeandroid.query.Select;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.JoseException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import ph.npi.netplay.bayadcenter.Epinoyload;
import ph.npi.netplay.bayadcenter.models.BillsPay;
import ph.npi.netplay.bayadcenter.models.Transaction;
import ph.npi.netplay.bayadcenter.models.User;
import ph.npi.netplay.bayadcenter.models.UtilMapSerializer;

/**
 * Created by Rhen Nepacena on 10/28/2015.
 */
public class SyncQueuedTaskNewApi  extends Service{
    private Context mContext;

    static Thread thread;
    private Handler mHandler;

    private String bearer_token = null;
    private String responds = null;
    private String external_id;

    public static final String BAYADCENTER_BILLSPAY = "http://125.5.114.138:50005/bayadcenter/v1/transactions/billspay";
    public static final String BAYADCENTER_BALANCE  = "http://125.5.114.138:50005/bayadcenter/v1/transactions/wallet/balance";



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.setProperty("http.keepAlive","false");
        this.mContext  = getApplicationContext();
        mHandler = new Handler(Looper.getMainLooper());

        thread = new Thread() {
            @Override
            public void run() {

                for(;;){
                    try{

                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                processSync();
                            }
                        });
                        if(isInterrupted())
                            break;
                        sleep(8000);
                    } catch (InterruptedException e){
                        break;
                    } catch (Exception e){

                    }
                }

            }
        };
        thread.start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void processSync(){

        BillsPay bp = new BillsPay();
//       SELECT transactions WHERE status = 'ForUploading'
        for(BillsPay billsPay: bp.getQueuedTransactions()){

            Log.d("syncqueuedtask-acctno",String.valueOf(billsPay.account_no));
            Log.d("syncqueuedtask-biller",String.valueOf(billsPay.billername));
            Log.d("syncqueuedtask-status",String.valueOf(billsPay.field_status));

            //AND AUTHENTICATED
            boolean isAuthenticated = true;
            User mUser = new User(mContext);
            isAuthenticated = mUser.getAuthentication();


            if(WifiStateTask.isOnline == true && isAuthenticated){

                processWorkFlow(billsPay);

            } else {
                billsPay.status = Transaction.SMS_PENDING_REPLY;
                billsPay.save();
            } //end if-else wifi

        } //end for

    }




    protected void processWorkFlow(BillsPay billsPay){

        BillsPay bp = getBillspay(billsPay.getId(), "ForUploading");
        if(bp != null){

            Long res = updateBillspayStatus(bp, "Uploaded"," "," "," ",external_id);
            String resString = res.toString();

            // res always returns the billspay id
            if(resString.equals(billsPay.getId().toString())) {
                //proceed
            } else {
                return;
            }

        } else {
            return;
        }


        final BillsPay bp1 = getBillspay(billsPay.getId(), "Uploaded");
        if(bp1 != null) {
            //proceed
        } else {
            return;
        }

        new AsyncTask<String,String,String>(){

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... strings) {

                try{
                    Date dates = new Date();
                    Random r = new Random();
                    long i1 = r.nextInt(99999999) + 6500;

                    external_id = String.valueOf(dates.getTime() + i1);

                    bearer_token = getAuthorizationBearerToken(external_id);

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(BAYADCENTER_BILLSPAY);
                    httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
                    httpPost.addHeader("Authorization", "Bearer " + bearer_token);

                    //hardcoded values need to be recode if  production purposes
                    List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);

                    UtilMapSerializer utilMapSerializer = new UtilMapSerializer();
                    Map<String,String> api_encode = utilMapSerializer.deserialize(bp1.api_encode);


                    for(Map.Entry<String,String> map_entry : api_encode.entrySet()){
                        String key = map_entry.getKey();
                        String value = map_entry.getValue();

                        nameValuePair.add(new BasicNameValuePair(key,value));
                    }

                        /*
                        *   check_no
                            check_date MM/dd/yyyy
                            bank_name
                            branch
                            amount – must include other charges value
                            name
                            bank_code
                            type_name – values are ‘Local’, ‘Regional’, ‘Managers Check’
                        *
                        * */

                    try {
                        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                    } catch (UnsupportedEncodingException e) {
                        // log exception
                        e.printStackTrace();
                    }


                    try {
                        HttpResponse response = httpClient.execute(httpPost);

                        Header[] headers = response.getAllHeaders();
                        for (Header header : headers) {
                            Log.d("syncqueuedtask-headers","Key : " + header.getName() + " ,Value : " + header.getValue());
                        }

                        responds = WebServiceResponds.convertResponseToString(response);

                        try{
                       /*
                         {
                            "external_reference":"139270898923931",
                            "result_code":"000",
                            "result_message":"Transaction successful.",
                            "transaction_no":"15308063240000197",
                            "request_timestamp":"11/04/2015 10:49:58 AM",
                            "response_timestamp":"11/04/2015 10:49:58 AM",
                            "wallet_balance":9985,"amount":"1.00"
                         }
                       */

                            /*
                             {
                              "amount":"0",
                              "result_message":"AccountNo is invalid.",
                              "result_code":"400","wallet_balance":9973,
                              "external_reference":"1446742437889",
                              "request_timestamp":"2015-11-05T13:50:13Z",
                              "transaction_no":null,
                              "response_timestamp":"2015-11-05T13:50:15Z"
                              }
                            */

                            JSONObject jsonObject = new JSONObject(responds);

                            if(jsonObject != null){
                                String strResultCode = jsonObject.getString("result_code");
                                if(strResultCode.contains("000")){
                                    BillsPay bp2 = getBillspay(bp1.getId(), "Uploaded");
                                    if(bp2 != null) {
                                        String strBalance   = jsonObject.getString("wallet_balance");
                                        String strMessage   = jsonObject.getString("result_message");
                                        String strTrx_no    = jsonObject.getString("transaction_no");
                                        String strReqTimestamp = jsonObject.getString("request_timestamp");
                                        String strResTimestamp = jsonObject.getString("response_timestamp");

                                        bp2.telcorefno      = strTrx_no;
                                        bp2.response        = strMessage;
                                        bp2.remarks         = "Balance: " + strBalance;

                                        Float  fltBalance   = Float.parseFloat(strBalance);


                                        Epinoyload.setBalanceLastUpdate(mContext);
                                        Epinoyload.setBalance(mContext, fltBalance, Epinoyload.KEY_LOAD_BAL);

                                        updateBillspayStatus(bp2, "Uploaded-Success"," ",strReqTimestamp,strResTimestamp,external_id);

                                    }
                                }else{

                                    String errorMessage = jsonObject.getString("result_message");
                                    String strReqTimestamp = jsonObject.getString("request_timestamp");
                                    String strResTimestamp   = jsonObject.getString("response_timestamp");

                                    BillsPay bp2 = getBillspay(bp1.getId(), "Uploaded");
                                    if(bp2 != null) {
                                        updateBillspayStatus(bp2, "Uploaded-Failed",errorMessage,strReqTimestamp,strResTimestamp,external_id);
                                    } //end if


                                    Log.i("syncqueuedtask-processBillersFailed", errorMessage);

                                }//end else strResultCode

                            }else{
                                String errorMessage = jsonObject.getString("result_message");
                                String strReqTimestamp = jsonObject.getString("request_timestamp");
                                String strResTimestamp   = jsonObject.getString("response_timestamp");

                                BillsPay bp2 = getBillspay(bp1.getId(), "Uploaded");
                                if(bp2 != null) {
                                    updateBillspayStatus(bp2, "Uploaded-Failed",errorMessage,strReqTimestamp,strResTimestamp,external_id);
                                } //end if


                                Log.i("syncqueuedtask-processBillersFailed", errorMessage);

                            }//end else !jsonObject


                        } catch (Exception e){

                            String errorMessage = e.getMessage();

                            BillsPay bp2 = getBillspay(bp1.getId(), "Uploaded");
                            if(bp2 != null) {
                                updateBillspayStatus(bp2, "Uploaded-Failed",errorMessage," "," ",external_id);
                            } //end if

                        }


                    } catch (ClientProtocolException e) {
                        // Log exception
                        e.printStackTrace();
                        String errorMessage = e.getMessage();

                        BillsPay bp2 = getBillspay(bp1.getId(), "Uploaded");
                        if(bp2 != null) {
                            updateBillspayStatus(bp2, "Uploaded-Failed",errorMessage," "," ",external_id);
                        } //end if

                    } catch (IOException e) {
                        // Log exception
                        e.printStackTrace();
                        String errorMessage = e.getMessage();

                        BillsPay bp2 = getBillspay(bp1.getId(), "Uploaded");
                        if(bp2 != null) {
                            updateBillspayStatus(bp2, "Uploaded-Failed",errorMessage," "," ",external_id);
                        } //end if
                    }
                }catch (Exception e){
                    String errorMessage = e.getMessage();

                    BillsPay bp2 = getBillspay(bp1.getId(), "Uploaded");
                    if(bp2 != null) {
                        updateBillspayStatus(bp2, "Uploaded-Failed",errorMessage," "," ",external_id);
                    }
                }


                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
            }
        }.execute();


    }



    protected Long updateBillspayStatus(BillsPay bp, String status,String responses,String start, String end,String external_id) {

        Calendar calendar = Calendar.getInstance();
        String timeAcquired = (new SimpleDateFormat("HH:mm:ss"))
                .format(calendar.getTime());

        if(status.equals("Uploaded")) {
            bp.is_sent      = 1;
        } else if(status.equals("Uploaded-Success")) {
            bp.time_received = timeAcquired;
            bp.isSuccessful = true;
            bp.api_start  = start;
            bp.api_end  = end;
            bp.external_reference = external_id;
        } else if(status.equals("Uploaded-Failed")) {
            bp.time_received = timeAcquired;
            bp.isSuccessful = false;
            bp.error_message = responses;
            bp.failed_transaction = 1;
            bp.api_start  = start;
            bp.api_end  = end;
            bp.external_reference = external_id;
        }

        bp.field_status = status;
        Long res = bp.save();

        String resString = res.toString();

        Log.i("syncqueuedtaskBillspayStatus-DBSAVE", resString);
        Log.i("syncqueuedtaskBillspayStatus-id",String.valueOf(bp.getId()));
        Log.i("syncqueuedtaskBillspayStatus-biller",String.valueOf(bp.account_no));
        Log.i("syncqueuedtaskBillspayStatus-status",String.valueOf(bp.field_status));
        Log.i("syncqueuedtaskBillspayStatus-received",String.valueOf(bp.time_received));
        Log.i("syncqueuedtaskBillspayStatus-sent",String.valueOf(bp.time_sent));

        return res;
    }


    protected BillsPay getBillspay(Long id, String status) {
        BillsPay bp = new Select().from(BillsPay.class).where("id = ? AND field_status = '" + status + "' ", id).executeSingle();
        return bp;
    }

    public  String sha1(String toHash) {
        String hash = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            byte[] bytes = toHash.getBytes("UTF-8");
            digest.update(bytes, 0, bytes.length);
            bytes = digest.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b : bytes) {
                sb.append(String.format("%02X", b));
            }
            hash = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return hash.toLowerCase(Locale.ENGLISH);
    }

    protected String urlencode(String value) {
        return URLEncoder.encode(value);
    }

    private String getAuthorizationBearerToken(String external_id) throws NoSuchAlgorithmException, JoseException, UnsupportedEncodingException{

        JwtClaims claims = new JwtClaims();

        claims.setIssuedAtToNow();
        claims.setClaim("username","bc_android");
        claims.setClaim("external_reference",external_id);
        claims.setClaim("ip","127.0.0.1");

        String secret_key = "29a9d233e48ffa699c3c1f991943a5518f9123db";
        Key key = new HmacKey(secret_key.getBytes("UTF-8"));

        JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());

        jws.setKey(key);
        jws.setHeader("typ","JWT");
        jws.setHeader("alg","HS256");
        jws.setDoKeyValidation(false);

        jws.setPayload(claims.toJson());
        String result = jws.getCompactSerialization();

        return result;
    }
}
