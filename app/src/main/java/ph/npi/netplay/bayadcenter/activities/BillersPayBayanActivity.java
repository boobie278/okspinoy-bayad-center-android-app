package ph.npi.netplay.bayadcenter.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import ph.npi.netplay.bayadcenter.Epinoyload;
import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.models.Billers;
import ph.npi.netplay.bayadcenter.models.BillsPay;
import ph.npi.netplay.bayadcenter.models.Events;
import ph.npi.netplay.bayadcenter.models.UtilMapSerializer;

/**
 * Created by Rhen Nepacena on 11/3/2015.
 */
public class BillersPayBayanActivity extends BillersPayActivity{

    private EditText med_accountnumber;
    private EditText med_telephoneNumber;
    private EditText med_customerName;
    private EditText med_amount;

    private Button mbtn_submit;

    private Context mContext;

    private String straccountNumber;
    private String stramount;
    private String strphoneNumber;
    private String strcustomerName;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_bill_bayan);
        Events.inputEventLogs("Access Bayantel Form");

        med_accountnumber   = (EditText) findViewById(R.id.ed_accountnumber);
        med_telephoneNumber = (EditText) findViewById(R.id.ed_telephoneNumber);
        med_customerName    = (EditText) findViewById(R.id.ed_customerName);
        med_amount          = (EditText) findViewById(R.id.ed_amount);
        mbtn_submit         = (Button) findViewById(R.id.btn_submit);


        mbtn_submit.setOnClickListener(submitOnClicked);

        mContext = this;

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            intentextras = extras.getString("biller_name");
        }

        biller = new Billers();
        biller = biller.getByName(intentextras);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_action_hardware_phone);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(biller.name);
       initializeCheckComponents();
       populateSpinner();
    }


    private void queTransactions(){
        HashMap<String,String> api_encode_all = new HashMap<String, String>();
        api_encode_all.put("account_no",straccountNumber);
        api_encode_all.put("amount_due",stramount);

        api_encode_all.put("service_code","BAYAN");
        api_encode_all.put("mode_of_payment",strPaymentType);
        api_encode_all.put("phone_no",strphoneNumber);
        api_encode_all.put("customer_name",strcustomerName);

        /*if(strPaymentType.contains("Check")){
            api_encode_all.put("check_no", strcheckNumber);
            api_encode_all.put("check_date",strcheckDate);
            api_encode_all.put("bank_name",strbankName);
            api_encode_all.put("branch",strbranch);

            api_encode_all.put("name",strcheckName);
            api_encode_all.put("bank_code",strbankCode);
            api_encode_all.put("check_type",strtypeName);
        }*/

        if(strPaymentType.contains("Cash")){
            api_encode_all.put("amount",stramount);
            api_encode_all.put("amount_paid",stramount);
        }/*else if(strPaymentType.contains("Check")){
            api_encode_all.put("amount",strcheckAmount);
            api_encode_all.put("amount_paid",stramount);
        }else if(strPaymentType.contains("Combined")){
            api_encode_all.put("cash_amount",stramount);
            api_encode_all.put("check_amount",strcheckAmount);
            float cash_amount = Float.parseFloat(stramount);
            float check_amount = Float.parseFloat(strcheckAmount);
            float total = cash_amount + check_amount;
            api_encode_all.put("amount_paid",String.valueOf(total));
        }*/

        UtilMapSerializer utilMapSerializer = new UtilMapSerializer();
        String api_encode = utilMapSerializer.serialize(api_encode_all);

        Calendar calendar = Calendar.getInstance();
       /* String sentDate = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"))
                .format(calendar.getTime());*/
        String sentTime = (new SimpleDateFormat("HH:mm:ss"))
                .format(calendar.getTime());

        BillsPay bps = new BillsPay();
        bps.billspayid = String.valueOf("" + (System.currentTimeMillis() / 1000));
        bps.createdDate = new Date();
        bps.time_sent = sentTime;

        bps.isSuccessful = false;
        bps.biller_code = biller.bilcode;
        bps.billername = biller.name;
        bps.failed_transaction = 0;

        bps.service_code    = "BAYAN";
        bps.account_no      = straccountNumber;
        bps.amount_due      = stramount;
        bps.amount_paid     = stramount;
        bps.mode_of_payment = strPaymentType;
        bps.amount          = stramount;

        bps.phone_no        = strphoneNumber;
        bps.customer_name   = strcustomerName;

        bps.check_no        = strcheckNumber;
        bps.check_date      = strcheckDate;
        bps.bank_name       = strbankName;
        bps.branch          = strbranch;
        bps.check_amount    = strcheckAmount;
        bps.check_name      = strcheckName;
        bps.bank_code       = strbankCode;
        bps.type_name       = strtypeName;

        bps.api_encode = api_encode;
        bps.field_status = "ForUploading";
        bps.is_sent = 0;

        bps.save();
        //showDialogAlert("Transactions Saved","Queued");
        processWorkFlow(bps);

    }




    private View.OnClickListener submitOnClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            straccountNumber  = med_accountnumber.getText().toString().trim();
            stramount         = med_amount.getText().toString().trim();
            strphoneNumber    = med_telephoneNumber.getText().toString().trim();
            strcustomerName   = med_customerName.getText().toString().trim();

            stramount    = stramount.replace(" ", "");
            stramount    = stramount.replace("-","");

            checkValues();

            if((straccountNumber.isEmpty()) || (stramount.isEmpty()) || (strphoneNumber.isEmpty())) {
                if (straccountNumber.isEmpty()) {
                    med_accountnumber.setBackgroundResource(R.drawable.edittext_red_rounded);
                }
                if (stramount.isEmpty()) {
                    med_amount.setBackgroundResource(R.drawable.edittext_red_rounded);
                }

                if(strcustomerName.isEmpty()){
                    med_customerName.setBackgroundResource(R.drawable.edittext_red_rounded);
                }
                return;
            }
            if(straccountNumber.length() < 9){
                Epinoyload.showAlert(mContext, "Account Number ", "Need to check Account Number");
            }else{
                if (Epinoyload.isOffline(mContext) == false) {
                    AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
                    alertDialog.setTitle("Please Confirm");
                    alertDialog.setMessage("You are about to pay " + biller.name + " with " + biller.field + straccountNumber + ". Are you sure you want to continue?");
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            queTransactions();
                            return;
                        }
                    });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            return;
                        }
                    });
                    alertDialog.show();
                } else {
                    // offline
                    Epinoyload.showAlert(mContext, "Please Confirm", "You are about to pay " + biller.name + " with " + straccountNumber + ".\n\nYou are in offline mode ! Transactions will be queued...\n\nAre you sure you want to continue?",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // Postive
                                    queTransactions();
                                }
                            }, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // negative
                                    dialogInterface.dismiss();
                                }
                            }
                    );
                }
            }
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.billspay, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
