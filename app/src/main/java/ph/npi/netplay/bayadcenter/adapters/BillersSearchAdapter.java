package ph.npi.netplay.bayadcenter.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.models.Billers;


/**
 * Created by Rhen Nepacena on 10/13/2014.
 */
public class BillersSearchAdapter extends ArrayAdapter {

    private Context mContext;

    public BillersSearchAdapter(Context context, List objects) {
        super(context, 0, objects);
        this.mContext = context;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listview_billers, null);
        }else{

        }

        Billers billersinfo = (Billers) getItem(position);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.imgVwLogo);
        TextView name = (TextView) convertView.findViewById(R.id.txtvwBillersName);
        name.setText(billersinfo.label);




        //hardcoded to be replaced soon...
        if(billersinfo.name.equals("Manila Water")){
            getImageDisplay(imageView,R.drawable.maynilad_water);
        }else if(billersinfo.name.equals("Meralco")){
            getImageDisplay(imageView,R.drawable.meralco);
        }else if(billersinfo.name.equals("PLDT")){
            getImageDisplay(imageView,R.drawable.pldt);
        }else if(billersinfo.name.equals("Sky Cable")){
            getImageDisplay(imageView,R.drawable.skycable);
        }else if(billersinfo.name.equals("Bayan")){
            getImageDisplay(imageView,R.drawable.bayantel);
        }else if(billersinfo.name.equals("MCWD")){
            getImageDisplay(imageView,R.drawable.mcwd);
        }else if(billersinfo.name.equals("MWCOM")){
            getImageDisplay(imageView,R.drawable.manila_water);
        }else if(billersinfo.name.equals("GLOBE")){
            getImageDisplay(imageView,R.drawable.globe);
        }else if(billersinfo.name.equals("SMART")){
            getImageDisplay(imageView,R.drawable.smart);
        }else if(billersinfo.name.equals("SUNCL")){
            getImageDisplay(imageView,R.drawable.sun_cellular);
        }else if(billersinfo.name.equals("DVolt")){
            getImageDisplay(imageView,R.drawable.dvolt);
        }else if(billersinfo.name.equals("VIECO")){
            getImageDisplay(imageView,R.drawable.vieco);
        }else if(billersinfo.name.equals("PHLTH")){
            getImageDisplay(imageView,R.drawable.philhealth);
        }else if(billersinfo.name.equals("HDMF1")){
            getImageDisplay(imageView,R.drawable.pagibig);
        }else if(billersinfo.name.equals("SSS")){
            getImageDisplay(imageView,R.drawable.sss);
        }



        return convertView;
    }


    private void getImageDisplay(ImageView imageView,int drawable){

        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            imageView.setBackgroundDrawable( mContext.getResources().getDrawable(drawable) );
        } else {
            imageView.setBackground( mContext.getResources().getDrawable(drawable));
        }
    }
}
