package ph.npi.netplay.bayadcenter.activities;

import android.app.ActionBar;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.adapters.BillersSearchAdapter;
import ph.npi.netplay.bayadcenter.adapters.BillsPayTransactionsAdapter;
import ph.npi.netplay.bayadcenter.libs.loadmore.LoadMoreListView;
import ph.npi.netplay.bayadcenter.libs.loadmore.PullAndLoadListView;
import ph.npi.netplay.bayadcenter.libs.loadmore.PullToRefreshListView;
import ph.npi.netplay.bayadcenter.libs.paging.PagingListView;
import ph.npi.netplay.bayadcenter.models.Billers;
import ph.npi.netplay.bayadcenter.models.BillsPay;
import ph.npi.netplay.bayadcenter.models.Events;

/**
 * Created by Rhen Nepacena on 11/5/2015.
 */
public class BillersTransactionActivity extends SherlockFragmentActivity {
    int check_offset = 0;

    private com.zkc.pc700.helper.SerialPort mSerialPort ;
    String thread = "readThread";
    String choosed_serial ="/dev/ttySAC3";
    int choosed_buad = 38400;

    private LoadMoreListView mlv_billspayTransactions;
    private EditText medtxtSearch;

    private BillsPayTransactionsAdapter billeradapter;
    private List<BillsPay> searchBillersList = new ArrayList<BillsPay>();
    private List<BillsPay> newSearchBillersList = new ArrayList<BillsPay>();
    private List<String> searchContents = new ArrayList<String>();
    private List<BillsPay> billspay;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_bill_transaction);

        getSupportActionBar().setIcon(getResources().getDrawable(R.drawable.ic_action_hardware_phone));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.searchlayout);

        View view = LayoutInflater.from(this).inflate(R.layout.searchbillerslayout, null);
        getSupportActionBar().setCustomView(view);

        mContext = this;
        BillsPay bp = new BillsPay();
        searchBillersList = bp.getTransactions();
        mlv_billspayTransactions = (LoadMoreListView) findViewById(R.id.lv_billspayTransactions);


        medtxtSearch = (EditText) view.findViewById(R.id.searchNow);
        medtxtSearch.setFocusable(true);
        medtxtSearch.requestFocus();
        medtxtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                String newText = charSequence.toString();
                newSearchBillersList.clear();
                if (newText.length() > 0) {
                    int j = 0;
                    boolean searchCheck = false;
                    String[] searchTemp = newText.split(" ");
                    for (String search : searchContents) {
                        for (String temp : searchTemp) {
                            temp = temp.replace(" ", "");
                            if (search.trim().toLowerCase().contains((temp.toLowerCase().replace(" ", "")))) {
                                searchCheck = true;
                            } else {
                                searchCheck = false;
                                break;
                            }
                        }

                        if (searchCheck) {
                            newSearchBillersList.add(searchBillersList.get(j));
                        }

                        j++;
                    }

                    //add to adapter the new list
                    billeradapter = new BillsPayTransactionsAdapter(mContext, newSearchBillersList);
                    mlv_billspayTransactions.setAdapter(billeradapter);
                    mlv_billspayTransactions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    mlv_billspayTransactions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            BillsPay billerinfo = newSearchBillersList.get(i);
                            if(billerinfo != null){
                                if(billerinfo.isSuccessful){
                                    //printReceipt(billerinfo);
                                    Log.d("billerstransactionactivity-billerinfo",String.valueOf(i));
                                    Log.d("billerstransactionactivity-billerinfo",String.valueOf(billerinfo.external_reference));
                                }else{
                                    Toast.makeText(mContext, "No Valid Transaction", Toast.LENGTH_SHORT).show();
                                }

                            }else{

                            }
                        }
                    });
                    billeradapter.notifyDataSetChanged();

                } else {
                    searchContents.clear();
                    getBillerList();
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {


            }
        });
        getBillerList();
    }



    private void getBillerList() {

        searchBillersList.clear();
        billspay = new BillsPay().getTransactions(3,check_offset);
        if (billspay.size() <= 0) {
            return;
        }


        for(BillsPay bills: billspay){
            searchBillersList.add(bills);

            if(bills.external_reference != null){
                searchContents.add((bills.external_reference.toLowerCase().replace(" ", "")));
            }
        }

        billeradapter = new BillsPayTransactionsAdapter(mContext, searchBillersList);
        mlv_billspayTransactions.setAdapter(billeradapter);
        mlv_billspayTransactions.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                    }

                    @Override
                    protected Void doInBackground(Void... voids) {
                        if (isCancelled()) {
                            return null;
                        }
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                        }
                        searchBillersList.clear();
                        searchContents.clear();
                        for(BillsPay bills: billspay){
                            if(bills != null){
                                searchBillersList.add(bills);
                                if(bills.external_reference != null){
                                    searchContents.add((bills.external_reference.toLowerCase().replace(" ", "")));
                                }
                            }
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        billeradapter.notifyDataSetChanged();
                        mlv_billspayTransactions.onLoadMoreComplete();
                        super.onPostExecute(aVoid);
                        Log.d("billerstransactionactivity-offset",String.valueOf(check_offset));

                        check_offset = check_offset + 3;

                        billspay = new BillsPay().getTransactions(3,check_offset);
                        if (billspay.size() <= 0) {
                            return;
                        }
                    }

                    @Override
                    protected void onCancelled() {
                        mlv_billspayTransactions.onLoadMoreComplete();
                    }
                }.execute();
            }
        });
        mlv_billspayTransactions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                BillsPay billerinfo = searchBillersList.get(i);

                if(billerinfo != null){
                    if(billerinfo.isSuccessful){
                        //printReceipt(billerinfo);
                    }else{
                        Toast.makeText(mContext, "No Valid Transaction", Toast.LENGTH_SHORT).show();
                    }

                }else{

                }
            }
        });
        billeradapter.notifyDataSetChanged();

    }


    private void printReceipt(BillsPay bp){

        Events.inputEventLogs("Print Receipt Start");

        Calendar calendar = Calendar.getInstance();
        String sentDate = (new SimpleDateFormat("yyyy-MM-dd"))
                .format(calendar.getTime());
        String sentTime = (new SimpleDateFormat("HH:mm:ss"))
                .format(calendar.getTime());

        String str = "";

        StringBuilder sb = new StringBuilder();

        sb.append("\n\n");
        sb.append("       Bayad Center       ");
        sb.append("\n\n");
        sb.append("CUSTOMER REF: ");
        sb.append(bp.billspayid);
        sb.append("\n\n");
        sb.append("ACT NO: ");
        sb.append(bp.account_no);
        sb.append("\n\n");
        sb.append("BILLER NAME: ");
        sb.append(bp.billername);
        sb.append("\n\n");
        sb.append("TOTAL AMOUNT PAID:  ");
        sb.append(bp.amount);
        sb.append("\n\n");
        sb.append("TYPE OF PAYMENT: ");
        sb.append(bp.mode_of_payment);
        sb.append("\n\n");
        sb.append("\n\n");
        sb.append("DATE: ");
        sb.append(sentDate);
        sb.append(" TIME: ");
        sb.append(sentTime);
        sb.append("\n\n");
        sb.append("Donnalyn B.");
        sb.append("\n\n");
        sb.append("\n\n");
        sb.append("\n\n");



        str = String.valueOf(sb);


        try {

            mSerialPort = new com.zkc.pc700.helper.SerialPort(choosed_serial,
                    choosed_buad, 0);

        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (mSerialPort != null) {

            new readThread().start();

        }


        byte[] buffer = null;

        try {
            buffer = str.getBytes("GBK");
        } catch (Exception e) {
            buffer = str.getBytes();
        }

        try {
            mSerialPort.getOutputStream().write(buffer);

        } catch (Exception e) {
            // TODO: handle exception
        }

        mSerialPort.close();

        Events.inputEventLogs("Print Receipt End");
    }

    class readThread extends Thread {

        @Override
        public void run() {
            super.run();
            while (!isInterrupted()) {
                int size;
                try {
                    byte[] buffer = new byte[300];
                    if (mSerialPort.getInputStream() == null)
                        return;
                    size = mSerialPort.getInputStream().read(buffer);
                    if (size > 0) {
                        //onDataReceived(buffer, size);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.search, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }





}
