package ph.npi.netplay.bayadcenter.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.models.Transaction;


/**
 * Created by Rhen Nepacena on 10/26/2015.
 */
public class RecentTransactionView {

    public static View getLayout(Context ctx, Transaction transaction) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.container_transaction_history, null, false);

        TextView date = (TextView) view.findViewById(R.id.transaction_date);
        TextView sku = (TextView) view.findViewById(R.id.transaction_sku);
        TextView amount = (TextView) view.findViewById(R.id.transaction_amount);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        date.setText("" + df.format(transaction.createdDate));
        sku.setText("" + transaction.product);
        amount.setText("" + transaction.referenceNo);

        return view;
    }
}
