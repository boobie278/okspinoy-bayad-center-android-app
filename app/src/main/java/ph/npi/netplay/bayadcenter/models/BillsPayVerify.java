package ph.npi.netplay.bayadcenter.models;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.Date;

/**
 * Created by Rhen Nepacena on 11/12/2015.
 */
@Table(name = "billspayverify")
public class BillsPayVerify {

    @Column(name = "external_reference")
    public String external_reference;

}
