package ph.npi.netplay.bayadcenter.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.fourmob.datetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import ph.npi.netplay.bayadcenter.Epinoyload;
import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.adapters.PaymentTypeAdapter;
import ph.npi.netplay.bayadcenter.models.Billers;
import ph.npi.netplay.bayadcenter.models.BillsPay;
import ph.npi.netplay.bayadcenter.models.Events;
import ph.npi.netplay.bayadcenter.models.PaymentTypes;
import ph.npi.netplay.bayadcenter.models.UtilMapSerializer;


/**
 * Created by Rhen Nepacena on 5/4/2015.
 */
public class BillersPayManuActivity extends BillersPayActivity implements DatePickerDialog.OnDateSetListener{
    private Context mContext;

    private Button btnSubmit;
    private EditText med_accountnumber;
    private EditText med_amount;
    private TextView mtt_toDate;

    private String straccountnumber;
    private String stramount;

    private String strtodate;

    private static final String DATE_PICKER_TAG = "datepicker";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_bill_manu);
        Events.inputEventLogs("Access Manulife PayBills Form");

        btnSubmit           = (Button) findViewById(R.id.btnSubmit);
        med_accountnumber   = (EditText) findViewById(R.id.ed_accountnumber);
        med_amount          = (EditText) findViewById(R.id.ed_amount);
        mtt_toDate          = (TextView) findViewById(R.id.tt_toDate);

        final Calendar calendar = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);

        btnSubmit.setOnClickListener(submitOnClicked);

        mContext = this;

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            intentextras = extras.getString("biller_name");
        }

        biller = new Billers();
        biller = biller.getByName(intentextras);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_action_hardware_phone);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(biller.name);

        mtt_toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                datePickerDialog.setYearRange(2014, 2037);
                datePickerDialog.show(getSupportFragmentManager(), DATE_PICKER_TAG);


            }
        });

        initializeCheckComponents();
        populateSpinner();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.billspay, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private View.OnClickListener submitOnClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            straccountnumber = med_accountnumber.getText().toString().trim();
            stramount        = med_amount.getText().toString().trim();
            strtodate        = mtt_toDate.getText().toString().trim();

            stramount    = stramount.replace(" ", "");
            stramount    = stramount.replace("-","");

            if((straccountnumber.isEmpty()) || (stramount.isEmpty()) || (strtodate.isEmpty())) {
                if (straccountnumber.isEmpty()) {
                    med_accountnumber.setBackgroundResource(R.drawable.edittext_red_rounded);

                }
                if (stramount.isEmpty()) {
                    med_amount.setBackgroundResource(R.drawable.edittext_red_rounded);

                }

                if(strtodate.isEmpty()){
                    mtt_toDate.setBackgroundResource(R.drawable.edittext_red_rounded);
                }

                return;
            }

            if(straccountnumber.length() < 8){
                Epinoyload.showAlert(mContext, "Account Number ", "Need to check Account Number");
            }else{
                if (Epinoyload.isOffline(mContext) == false) {
                    AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
                    alertDialog.setTitle("Please Confirm");
                    alertDialog.setMessage("You are about to pay " + biller.name + " with " + biller.field + straccountnumber + ". Are you sure you want to continue?");
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            queTransactions();
                            return;
                        }
                    });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            return;
                        }
                    });
                    alertDialog.show();
                } else {
                    // offline
                    Epinoyload.showAlert(mContext, "Please Confirm", "You are about to pay " + biller.name + " with " + straccountnumber + ".\n\nYou are in offline mode ! Transactions will be queued...\n\nAre you sure you want to continue?",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // Postive
                                    queTransactions();
                                }
                            }, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // negative
                                    dialogInterface.dismiss();
                                }
                            }
                    );
                }
            }
        }
    };


    private void queTransactions(){
        HashMap<String,String> api_encode_all = new HashMap<String, String>();
        api_encode_all.put("account_no",straccountnumber);
        api_encode_all.put("amount_due",stramount);
        api_encode_all.put("amount_paid",stramount);
        api_encode_all.put("amount",stramount);
        api_encode_all.put("service_code","MANUL");
        api_encode_all.put("to_date",strtodate);

        if(strPaymentType.contains("ccard") || strPaymentType.contains("ccard")){

            api_encode_all.put("check_no",strcheckNumber);
            api_encode_all.put("check_date",strcheckDate);
            api_encode_all.put("bank_name",strbankName);
            api_encode_all.put("branch",strbranch);
            api_encode_all.put("check_amount",strcheckAmount);
            api_encode_all.put("check_name",strcheckName);
            api_encode_all.put("bank_code",strbankCode);
            api_encode_all.put("type_name",strtypeName);
        }
        UtilMapSerializer utilMapSerializer = new UtilMapSerializer();
        String api_encode = utilMapSerializer.serialize(api_encode_all);
        Calendar calendar = Calendar.getInstance();
       /* String sentDate = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"))
                .format(calendar.getTime());*/
        String sentTime = (new SimpleDateFormat("HH:mm:ss"))
                .format(calendar.getTime());

        BillsPay bps = new BillsPay();
        bps.billspayid = String.valueOf("" + (System.currentTimeMillis() / 1000));
        bps.createdDate = new Date();
        bps.time_sent = sentTime;

        bps.isSuccessful = false;
        bps.biller_code = biller.bilcode;
        bps.billername = biller.name;
        bps.failed_transaction = 0;

        bps.service_code    = "MANUL";
        bps.account_no      = straccountnumber;
        bps.amount_due      = stramount;
        bps.amount_paid     = stramount;
        bps.mode_of_payment = strPaymentType;
        bps.amount          = stramount;

        bps.to_date         = strtodate;

        bps.check_no        = strcheckNumber;
        bps.check_date      = strcheckDate;
        bps.bank_name       = strbankName;
        bps.branch          = strbranch;
        bps.check_amount    = strcheckAmount;
        bps.check_name      = strcheckName;
        bps.bank_code       = strbankCode;
        bps.type_name       = strtypeName;

        bps.api_encode = api_encode;
        bps.field_status = "ForUploading";
        bps.is_sent = 0;

        bps.save();
        showDialogAlert("Transactions Saved","Queued");


    }




    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        //TODO check day if day < 10 add 0 prefix.
        String dayString = day + "";
        String monString = month + "";
        if(day < 10){
            dayString = "0" + day;
        }

        if(month < 9){
            month += 1;
            monString = "0" + month;
        }else{
            monString = String.valueOf(month + 1);
        }

        //mDepositDate = (month + 1) + "-" + dayString + "-" + year;
        strtodate = (monString) + "-" + dayString + "-" + year;

        mtt_toDate.setText(strtodate);


    }




}
