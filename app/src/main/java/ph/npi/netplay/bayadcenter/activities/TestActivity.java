package ph.npi.netplay.bayadcenter.activities;

import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.jose4j.base64url.internal.apache.commons.codec.binary.Base64;
import org.jose4j.jwk.EcJwkGenerator;
import org.jose4j.jwk.EllipticCurveJsonWebKey;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwk.OctJwkGenerator;
import org.jose4j.jwk.OctetSequenceJsonWebKey;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.AesKey;
import org.jose4j.keys.EllipticCurves;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.ByteUtil;
import org.jose4j.lang.JoseException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.models.BillsPay;
import ph.npi.netplay.bayadcenter.service.VolleySingleton;
import ph.npi.netplay.bayadcenter.service.WebServiceResponds;

/**
 * Created by Rhen Nepacena on 11/1/2015.
 */
public class TestActivity extends SherlockFragmentActivity implements View.OnClickListener{
    private Button btn_submit;

    private String bearer_token = null;
    private String responds;
    private static final String BAYADCENTER_LOGIN = "http://125.5.114.138:50005/bayadcenter/v1/authenticate";
    private static final String AUTHORIZATION = "Authorization";
    private static final String USERNAME = "bc_android";
    private static final String PASSWORD = "wildenson123";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        btn_submit = (Button) findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_submit:
                //test();
                try{
                    //processWorkFlow();
                    getbalance();
                    //String external_id = String.valueOf("139270898923924");
                    //bearer_token = getAuthorizationBearerToken(external_id);
                    //Log.d("testactivity-token",bearer_token);
                    //makePostRequest();
                    //gettransaction("139270898923931");
                }catch (Exception e){
                    Log.d("testactivity-er",e.toString());
                }
                break;

        }
    }

    protected void gettransaction(String transactionref){
        try{
            Date dates = new Date();
            Random r = new Random();
            long i1 = r.nextInt(99999999) + 6500;


            String external_id = String.valueOf(dates.getTime() + i1);
            Log.d("testactivity-id",external_id);

            bearer_token = getAuthorizationBearerToken(external_id);
            Log.d("testactivity-token",bearer_token);

        }catch (Exception e){

        }

        RequestQueue queue = VolleySingleton.getsInstance().getRequestQueue();

        String url = "http://125.5.114.138:50005/bayadcenter/v1/transactions/wallet/info/"+transactionref;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("testactivity-resp",response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("testactivity-err1",error.toString());
            }

        }
        ){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

            @Override
            public Map <String, String > getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer "+bearer_token);
                return headers;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    private void testlogin() {
        new AsyncTask<String,String,String>(){

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params) {
                try{
                    HttpGet request = new HttpGet(BAYADCENTER_LOGIN);
                    String auth = USERNAME + ":" + PASSWORD;
                    byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("ISO-8859-1")));
                    String authHeader = "Basic " + new String(encodedAuth);
                    request.setHeader(AUTHORIZATION, authHeader);

                    /*HttpClient client = HttpClientBuilder.create().build();
                    HttpResponse response = client.execute(request);

                    int statusCode = response.getStatusLine().getStatusCode();
                    assertThat(statusCode, equalTo(HttpStatus.SC_OK));*/


                    HttpResponse httpresponse = new DefaultHttpClient().execute(request);
                    StatusLine statusline = httpresponse.getStatusLine();


                    if(statusline.getStatusCode() == 200){
                        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
                        httpresponse.getEntity().writeTo(bytearrayoutputstream);
                        responds	=	bytearrayoutputstream.toString();
                        bytearrayoutputstream.close();

                        Log.i("testactivity-httpgetresp", responds);

                    }else{
                        httpresponse.getEntity().getContent().close();
                        throw new IOException(statusline.getReasonPhrase());
                    }


                }catch (Exception e){

                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
            }
        }.execute();
    }


    private void makePostRequest() {



        new AsyncTask<String,String,String>(){

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... strings) {

                try{
                    Date dates = new Date();
                    Random r = new Random();
                    //long i1 = r.nextInt(99999999) + 6500;
                    //long i1 = r.nextInt(9999) + 6500;

                    String external_id = String.valueOf("139270898923937");
                    Log.d("testactivity-id",external_id);

                    bearer_token = getAuthorizationBearerToken(external_id);
                    Log.d("testactivity-token",bearer_token);

                }catch (Exception e){

                }

                HttpClient httpClient = new DefaultHttpClient();
                // replace with your url
                HttpPost httpPost = new HttpPost("http://125.5.114.138:50005/bayadcenter/v1/transactions/billspay");
                httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
                httpPost.addHeader("Authorization", "Bearer " + bearer_token);

                //Post Data
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);

                nameValuePair.add(new BasicNameValuePair("account_no", "23136100007"));
                nameValuePair.add(new BasicNameValuePair("amount_due", "1.00"));
                nameValuePair.add(new BasicNameValuePair("amount_paid", "1.00"));
                nameValuePair.add(new BasicNameValuePair("service_code", "DVOLT"));
                nameValuePair.add(new BasicNameValuePair("mode_of_payment", "Cash"));
                nameValuePair.add(new BasicNameValuePair("amount", "1.00"));
                nameValuePair.add(new BasicNameValuePair("last_name", "Nepacena"));
                nameValuePair.add(new BasicNameValuePair("first_name", "Wildenson"));


                //Encoding POST data
                try {
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                } catch (UnsupportedEncodingException e) {
                    // log exception
                    e.printStackTrace();
                }

                //making POST request.
                try {
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();
                    // write response to log

                    Header[] headers = response.getAllHeaders();
                    for (Header header : headers) {
                        Log.d("testactivity-log","Key : " + header.getName() + " ,Value : " + header.getValue());
                    }

                    responds = WebServiceResponds.convertResponseToString(response);

                    Log.d("testactivity-logbody",responds);

                } catch (ClientProtocolException e) {
                    // Log exception
                    e.printStackTrace();
                } catch (IOException e) {
                    // Log exception
                    e.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
            }
        }.execute();

    }







    protected void getbalance(){
        try{
            Date dates = new Date();
            Random r = new Random();
            long i1 = r.nextInt(99999999) + 6500;


            String external_id = String.valueOf(dates.getTime() + i1);
            Log.d("testactivity-id",external_id);

            bearer_token = getAuthorizationBearerToken(external_id);
            Log.d("testactivity-token",bearer_token);

        }catch (Exception e){

        }

        RequestQueue queue = VolleySingleton.getsInstance().getRequestQueue();

        String url = "http://125.5.114.138:50005/bayadcenter/v1/transactions/wallet/balance";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("testactivity-resp",response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("testactivity-err1",error.toString());
            }

        }
        ){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

            @Override
            public Map <String, String > getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer "+bearer_token);
                return headers;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }




    private String getAuthorizationBearerToken() throws NoSuchAlgorithmException, JoseException, UnsupportedEncodingException {

        JwtClaims claims = new JwtClaims();

        claims.setIssuedAtToNow();
        claims.setClaim("username","Bob");
        claims.setClaim("external_reference","00443");
        claims.setClaim("ip","127.0.0.1");


        List<String> groups = Arrays.asList("group-one", "other-group", "group-three");
        claims.setStringListClaim("groups", groups);
        //EllipticCurveJsonWebKey senderJwk = EcJwkGenerator.generateJwk(EllipticCurves.P256);
        //senderJwk.setKeyId("29a9d233e48ffa699c3c1f991943a5518f9123db");
        /*JsonWebKey senderJwk = OctJwkGenerator.generateJwk(2048);
        senderJwk.setKeyId("29a9d233e48ffa699c3c1f991943a5518f9123db");*/

        String secret = "29a9d233e48ffa699c3c1f991943a5518f9123db";
        Key key = new HmacKey(secret.getBytes("UTF-8"));
        JsonWebSignature jws1 = new JsonWebSignature();
        jws1.setPayload(claims.toJson());
        jws1.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);
        jws1.setKey(key);
        jws1.setHeader("typ","JWT");
        jws1.setHeader("alg","HS256");
        jws1.setDoKeyValidation(false); // relaxes the key length requirement

        JsonWebSignature jws = new JsonWebSignature();

        jws1.setPayload(claims.toJson());

        //jws.getHeaders();
        //jws.setKey(senderJwk.getKey());

        //Log.d("testactivity-goes",String.valueOf(senderJwk.getKey()));
        //jws.setKeyIdHeaderValue(senderJwk.getKeyId());
        //jws.setHeader("typ","JWT");
        //jws.setHeader("alg","HS256");
        //jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);

        String result = jws1.getCompactSerialization();
        //validate(result,senderJwk);
        return result;
    }

    private void validate(String jwt,JsonWebKey receiverJwk){
        JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                //.setRequireExpirationTime() // the JWT must have an expiration time
                //.setRequireSubject() // the JWT must have a subject claim
                //.setExpectedIssuer("sender") // whom the JWT needs to have been issued by
                //.setExpectedAudience("receiver") // to whom the JWT is intended for
                .setDecryptionKey(receiverJwk.getKey()) // decrypt with the receiver's private key
                .setVerificationKey(receiverJwk.getKey()) // verify the signature with the sender's public key
                .build(); // create the JwtConsumer instance

        try
        {
            //  Validate the JWT and process it to the Claims
            JwtClaims jwtClaims = jwtConsumer.processToClaims(jwt);
            System.out.println("JWT validation succeeded! " + jwtClaims);
            Log.d("testactivity-succes",String.valueOf(jwtClaims));
        }
        catch (InvalidJwtException e)
        {
            // InvalidJwtException will be thrown, if the JWT failed processing or validation in anyway.
            // Hopefully with meaningful explanations(s) about what went wrong.
            System.out.println("Invalid JWT! " + e);
            Log.d("testactivity-err",e.toString());
        }
    }


    private String getAuthorizationBearerToken(String external_id) throws NoSuchAlgorithmException, JoseException, UnsupportedEncodingException{

        JwtClaims claims = new JwtClaims();

        claims.setIssuedAtToNow();
        claims.setClaim("username","bc_android");
        claims.setClaim("external_reference",external_id);
        claims.setClaim("ip","127.0.0.1");

        /*List<String> groups = Arrays.asList("group-one", "other-group", "group-three");
        claims.setStringListClaim("groups", groups);*/

        String secret_key = "29a9d233e48ffa699c3c1f991943a5518f9123db";
        Key key = new HmacKey(secret_key.getBytes("UTF-8"));

        JsonWebSignature jws1 = new JsonWebSignature();
        jws1.setPayload(claims.toJson());

        jws1.setKey(key);
        jws1.setHeader("typ","JWT");
        jws1.setHeader("alg","HS256");
        jws1.setDoKeyValidation(false);

        jws1.setPayload(claims.toJson());
        String result = jws1.getCompactSerialization();

        return result;
    }

}
