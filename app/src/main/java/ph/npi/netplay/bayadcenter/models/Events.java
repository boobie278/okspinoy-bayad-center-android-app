package ph.npi.netplay.bayadcenter.models;

import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Rhen Nepacena on 6/8/2015.
 */

@Table(name = "events")
public class Events extends Model {

    @Column(name = "event_logs", index = true)
    public String event_logs;

    @Column(name = "event_date")
    public String event_date;

    @Column(name = "event_time")
    public String event_time;


    public List<Events> getEventList() {
        return new Select().from(Events.class).orderBy("event_date DESC").execute();
    }


    public static void inputEventLogs(String logs){
        Calendar calendar = Calendar.getInstance();
        String sentTime = (new SimpleDateFormat("MM-dd-yyyy HH:mm:ss"))
                .format(calendar.getTime());

        Events mevents = new Events();


        if(mevents != null ){
            mevents.event_date = sentTime;
            mevents.event_logs = logs;
            long checkSave = mevents.save();

            if(checkSave > 0){
                Log.d("event-logs", "event-save");
            }else{
                Log.d("event-logs","event-no-save");
            }

        }else{
            return;
        }



    }







}
