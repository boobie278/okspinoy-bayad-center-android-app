package ph.npi.netplay.bayadcenter.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

//Created for Billers Model
@Table(name = "billers")
public class Billers extends Model {
    // If name is omitted, then the field name is used.
    @Column(name = "name", index = true)
    public String name;

    @Column(name ="bilcode")
    public String bilcode;

    @Column(name = "field")
    public String field;

    @Column(name = "type")
    public String type;

    @Column(name = "label")
    public String label;

    @Column(name = "count")
    public int count;

    public Billers() {
        super();
    }




    public Billers(String name, String field, String type, String bilcode) {
        super();
        this.name = name;
        this.field = field;
        this.type = type;
        this.bilcode = bilcode;
    }

    public Billers getByName(String name) {
        return new Select().from(Billers.class).where("name = ?", name).executeSingle();
    }


    public List<Billers> getByType(String type){
        List<Billers> billers = new Select().from(Billers.class).where("type = ?", type).execute();
        return billers;
    }


    public List<Billers> get() {
        return new Select().from(Billers.class).orderBy("count DESC").execute();
    }

    public List<Billers> getType() {
        List<Billers> billers = new Select("type").distinct().from(Billers.class).execute();

        return billers;
    }

    public int getBillersCount() {
        return new Select().from(Billers.class).count();
    }


    public Billers getBillersByCount(String billersName){
        Billers billers = new Select().from(Billers.class).where("name = ?",billersName).executeSingle();
        return billers;
    }


}

