package ph.npi.netplay.bayadcenter.activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import ph.npi.netplay.bayadcenter.Epinoyload;
import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.fragment.MainFragment;
import ph.npi.netplay.bayadcenter.models.Events;
import ph.npi.netplay.bayadcenter.models.User;
import ph.npi.netplay.bayadcenter.service.SyncQueuedTaskNewApi;
import ph.npi.netplay.bayadcenter.service.WifiStateTask;


public class MainActivity extends SherlockFragmentActivity {

    // Declare Variables
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private MenuListAdapter mMenuAdapter;
    private String[] title;
    private int[] icon;
    private Fragment fragment1 ;
    private Fragment fragment2;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private LinearLayout mDrawerLinearLayout;
    private Context mContext;


    @Override
    protected void onSaveInstanceState(Bundle outState) {

        /*FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        fragment2 = new MainFragment();
        ft.replace(R.id.content_frame, fragment2);*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;

        Events.inputEventLogs("Access Dashboard");


        if (Epinoyload.getMobileNumber(mContext).equals("") ||
                Epinoyload.getMobileNumber(mContext).equals(null) ||
                Epinoyload.getMobileNumber(mContext).isEmpty()) {
            Events.inputEventLogs("No Mobile Number");
            Intent intent = new Intent(MainActivity.this, MobileNumberActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        checkUserPassword();







        // Get the view from drawer_main.xml
        setContentView(R.layout.drawer_main);


        // Remove focus from any edittext
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        // Get the Title
        mTitle = mDrawerTitle = getTitle();

        // Generate title
        /*title = new String[]{
                "Home",
                "Bills Pay",
                "Mag-LOAD Tayo",
                "Transaction Log",
                "Pasa-Credits (RLOAD)",
                "Add Trader / Retailer",
                "Deposit",
                "Contact Us",
                "Settings",
                "Log Out"
        };*/

        title = new String[]{
                "Home",
                "Bills Pay",
//                "Mag-LOAD Tayo",
                "Verify Transaction",
//                "Pasa-Credits (RLOAD)",
//                "Add Trader / Retailer",
//                "Deposit",
                "Contact Us",
                "Access Logs",
                "Transaction Logs",
                "Settings",
                "Log Out"
        };

        // Generate icon
        icon = new int[]{
                R.drawable.ic_action_home,
                R.drawable.ic_action_billspay,
//                R.drawable.ic_action_hardware_phone,
                R.drawable.ic_action_prepaid,
//                R.drawable.ic_action_transact_logs,
//                R.drawable.ic_action_social_group,
//                R.drawable.ic_action_add_personone,
//                R.drawable.ic_action_deposit,
                R.drawable.ic_action_contact_center,
                R.drawable.ic_action_transact_logs,
                R.drawable.ic_action_transact_logs,
                R.drawable.ic_action_gear_settings,
                R.drawable.ic_action_logout_me
        };

        // Locate DrawerLayout in drawer_main.xml
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Locate ListView in drawer_main.xml
        mDrawerLinearLayout = (LinearLayout) findViewById(R.id.drawerLinearLayout);
        mDrawerList = (ListView) findViewById(R.id.drawerListView);

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);

        mDrawerList.setCacheColorHint(Color.BLACK);

        // Pass string arrays to MenuListAdapter
        mMenuAdapter = new MenuListAdapter(MainActivity.this, title, icon);

        // Set the MenuListAdapter to the ListView
        mDrawerList.setAdapter(mMenuAdapter);

        // Capture listview menu item click
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // Enable ActionBar app icon to behave as action to toggle nav drawer

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*setTitle("Home");
        setIcon(R.drawable.ic_action_home);*/

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer,
                R.string.drawer_open,
                R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            public void onDrawerOpened(View drawerView) {
                // Set the title on the action when drawer open
//                getSupportActionBar().setTitle(mDrawerTitle);
                super.onDrawerOpened(drawerView);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectItem(0);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(mDrawerLinearLayout)) {
                    mDrawerLayout.closeDrawer(mDrawerLinearLayout);
                } else {
                    mDrawerLayout.openDrawer(mDrawerLinearLayout);
                }
                break;
            case R.id.action_logout:
                Epinoyload.showAlert(mContext, "Sign Out", "This will sign you out of the application. Are you sure you want to continue?", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Positive
                        /*Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        MainActivity.this.startActivity(intent);*/
                        stopServices();
                        MainActivity.this.finish();




                        return;
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    // ListView click listener in the navigation drawer
    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            selectItem(position);
        }
    }


    private void checkUserPassword(){
        User mUser = new User(mContext);
        if(((mUser.getUsername()).equals("") && (mUser.getPassword()).equals("") )){
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
            finish();
        }

    }



    private void selectItem(int position) {

        Intent intent = null;

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // Locate Position
        switch (position) {
            case 0:

                Boolean intentextras;
                intentextras = false;

                Bundle bundle = getIntent().getExtras();
                if(bundle != null){
                    intentextras = bundle.getBoolean("isBiller");
                    if(intentextras == true){

                        fragment2 = new MainFragment();
                        ft.replace(R.id.content_frame, fragment2);
                    }else{

                        /*fragment1 = new HomeFragment();
                        ft.replace(R.id.content_frame, fragment1);*/
                    }
                }else{



                    if(Epinoyload.getDashboard(mContext,"dsdashboard").equals("")){
                        Epinoyload.setDashboard(mContext, "Bills Pay");
                        fragment2 = new MainFragment();
                        ft.replace(R.id.content_frame, fragment2);
                    }else{
                        String dashedboards = Epinoyload.getDashboard(mContext,"dsdashboard").toString();

                        if(dashedboards.equals("Bills Pay")){
                            fragment2 = new MainFragment();
                            ft.replace(R.id.content_frame, fragment2);
                        }else if(dashedboards.equals("Cellphone Load")){
                            /*fragment1 = new HomeFragment();
                            ft.replace(R.id.content_frame, fragment1);*/
                        }

                    }
                }


                break;
            case 1:
                fragment2 = new MainFragment();
                ft.replace(R.id.content_frame, fragment2);
                break;

            case 2:
                ft.commitAllowingStateLoss();
                Intent checkTransactions = new Intent(mContext, CheckTransactionActivity.class);
                mContext.startActivity(checkTransactions);
                return;

            case 3:
                ft.commitAllowingStateLoss();
                Intent contactus = new Intent(mContext, ContactUsActivity.class);
                mContext.startActivity(contactus);
                return;


            case 4:
                ft.commitAllowingStateLoss();
                Intent eventlogs = new Intent(mContext, EventLogsActivity.class);
                mContext.startActivity(eventlogs);
                return;

            case 5:
                ft.commitAllowingStateLoss();
                Intent transactlogs = new Intent(mContext, BillersTransactionActivity.class);
                mContext.startActivity(transactlogs);
                return;

            case 6:
                ft.commitAllowingStateLoss();
                Intent settings = new Intent(mContext, SettingsActivity.class);
                mContext.startActivity(settings);
                finish();
                return;

            case 7:
                ft.commitAllowingStateLoss();
                Epinoyload.showAlert(mContext, "Exit Application", "This will exit the application. Are you sure you want to continue?", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        MainActivity.this.finish();
                        return;
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                return;
        }

        ft.commitAllowingStateLoss();
        mDrawerList.setItemChecked(position, true);

        // Get the title followed by the position
        setTitle(title[position]);
        setIcon(icon[position]);
        // Close drawer
        mDrawerLayout.closeDrawer(mDrawerLinearLayout);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    private void setIcon(int resId) {
        getSupportActionBar().setIcon(resId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.main, menu);

        return super.onCreateOptionsMenu(menu);
    }




    @Override
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(mDrawerLinearLayout)) {
            mDrawerLayout.closeDrawer(mDrawerLinearLayout);
        }else{
            Epinoyload.showAlert(mContext, "Exit Application", "This will exit the application. Are you sure you want to continue?", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    // Positive
                    /*Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    MainActivity.this.startActivity(intent);*/
                    MainActivity.this.finish();
                    return;
                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

        }
    }


    public void startServices() {

        Log.i("controller-main", "start services - start");

        startService(WifiStateTask.class);
        startService(SyncQueuedTaskNewApi.class);

        Log.i("controller-main", "start services - end");
    }


    public void stopServices() {
        Log.i("controller-main", "stop services");
        if (isServiceRunning(WifiStateTask.class, mContext)) {
            WifiStateTask.cancel();
            stopService(WifiStateTask.class);
        }
    }




    private void startService(Class<?> serviceClass){
        if(isServiceNotRunning(serviceClass, mContext))
            mContext.startService(new Intent(mContext, serviceClass));
    }

    private void stopService(Class<?> serviceClass){
        if(isServiceRunning(serviceClass, mContext)){
            mContext.stopService(new Intent(mContext, serviceClass));
        }
    }


    private boolean isServiceRunning(Class<?> serviceClass,Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private boolean isServiceNotRunning(Class<?> serviceClass,Context context) {
        return !isServiceRunning(serviceClass, context);
    }








}
