package ph.npi.netplay.bayadcenter.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import ph.npi.netplay.bayadcenter.Epinoyload;
import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.models.Billers;
import ph.npi.netplay.bayadcenter.models.BillsPay;
import ph.npi.netplay.bayadcenter.models.Events;
import ph.npi.netplay.bayadcenter.models.UtilMapSerializer;

/**
 * Created by Rhen Nepacena on 11/4/2015.
 */
public class BillersPayMCWDActivity extends BillersPayActivity{

    private Context mContext;

    private Button btnSubmit;

    private EditText med_accountnumber;
    private EditText med_amount;
    private Spinner mspnr_billOnly;

    private String straccountNumber;
    private String stramount;
    private String strbillOnly;
    private String strPayOnly;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_bill_mcwd);

        Events.inputEventLogs("Access MCWD PayBills Form");

        btnSubmit = (Button) findViewById(R.id.btnSubmit);


        med_accountnumber     =   (EditText) findViewById(R.id.ed_accountnumber);
        med_amount            =   (EditText) findViewById(R.id.ed_amount);
        mspnr_billOnly        = (Spinner) findViewById(R.id.spnr_billOnly);

        btnSubmit.setOnClickListener(submitOnClicked);

        mContext = this;

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            intentextras = extras.getString("biller_name");
        }

        biller = new Billers();
        biller = biller.getByName(intentextras);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_action_hardware_phone);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(biller.name);


        initializeCheckComponents();
        populateSpinner();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.billspay, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private View.OnClickListener submitOnClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            straccountNumber    = med_accountnumber.getText().toString().trim();
            stramount           = med_amount.getText().toString().trim();
            strbillOnly         = mspnr_billOnly.getSelectedItem().toString();

            stramount           = stramount.replace(" ", "");
            stramount           = stramount.replace("-","");

            checkValues();


            if(strbillOnly.contains("Up to Current")){
                strPayOnly = "0";
            }else{
                strPayOnly = "1";
            }


            if((straccountNumber.isEmpty()) || (stramount.isEmpty()) ) {
                if (straccountNumber.isEmpty()) {
                    med_accountnumber.setBackgroundResource(R.drawable.edittext_red_rounded);
                }
                if (stramount.isEmpty()) {
                    med_amount.setBackgroundResource(R.drawable.edittext_red_rounded);
                }

                return;
            }

            if(straccountNumber.length() < 8){
                Epinoyload.showAlert(mContext, "Account Number ", "Need to check Account Number");
            }else{
                if (Epinoyload.isOffline(mContext) == false) {
                    AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
                    alertDialog.setTitle("Please Confirm");
                    alertDialog.setMessage("You are about to pay " + biller.name + " with " + biller.field + straccountNumber + ". Are you sure you want to continue?");
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            queTransactions();
                            return;
                        }
                    });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            return;
                        }
                    });
                    alertDialog.show();
                } else {
                    // offline
                    Epinoyload.showAlert(mContext, "Please Confirm", "You are about to pay " + biller.name + " with " + straccountNumber + ".\n\nYou are in offline mode ! Transactions will be queued...\n\nAre you sure you want to continue?",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // Postive
                                    queTransactions();
                                }
                            }, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // negative
                                    dialogInterface.dismiss();
                                }
                            }
                    );
                }
            }

        }
    };

    private void queTransactions(){

        HashMap<String,String> api_encode_all = new HashMap<String, String>();
        api_encode_all.put("account_no",straccountNumber);
        api_encode_all.put("amount_due",stramount);

        api_encode_all.put("service_code","MCWD1");
        api_encode_all.put("mode_of_payment",strPaymentType);
        api_encode_all.put("previous_bill_only",strPayOnly);

        /*if(strPaymentType.contains("Check") || strPaymentType.contains("Combined")){
            api_encode_all.put("check_no", strcheckNumber);
            api_encode_all.put("check_date",strcheckDate);
            api_encode_all.put("bank_name",strbankName);
            api_encode_all.put("branch",strbranch);

            api_encode_all.put("name",strcheckName);
            api_encode_all.put("bank_code",strbankCode);
            api_encode_all.put("check_type",strtypeName);
        }*/

        if(strPaymentType.contains("Cash")){
            api_encode_all.put("amount",stramount);
            api_encode_all.put("amount_paid",stramount);
        }/*else if(strPaymentType.contains("Check")){
            api_encode_all.put("amount",strcheckAmount);
            api_encode_all.put("amount_paid",stramount);
        }else if(strPaymentType.contains("Combined")){
            api_encode_all.put("cash_amount",stramount);
            api_encode_all.put("check_amount",strcheckAmount);
            float cash_amount = Float.parseFloat(stramount);
            float check_amount = Float.parseFloat(strcheckAmount);
            float total = cash_amount + check_amount;
            api_encode_all.put("amount_paid",String.valueOf(total));
        }*/

        UtilMapSerializer utilMapSerializer = new UtilMapSerializer();
        String api_encode = utilMapSerializer.serialize(api_encode_all);
        Calendar calendar = Calendar.getInstance();
        String sentTime = (new SimpleDateFormat("HH:mm:ss"))
                .format(calendar.getTime());

        BillsPay bps = new BillsPay();
        bps.billspayid = String.valueOf("" + (System.currentTimeMillis() / 1000));
        bps.createdDate = new Date();
        bps.time_sent = sentTime;

        bps.isSuccessful = false;
        bps.biller_code = biller.bilcode;
        bps.billername = biller.name;
        bps.failed_transaction = 0;

        bps.service_code    = "MCWD1";
        bps.account_no      = straccountNumber;
        bps.amount_due      = stramount;
        bps.amount_paid     = stramount;
        bps.mode_of_payment = strPaymentType;
        bps.amount          = stramount;

        bps.to_payonly      = strPayOnly;

        bps.check_no        = strcheckNumber;
        bps.check_date      = strcheckDate;
        bps.bank_name       = strbankName;
        bps.branch          = strbranch;
        bps.check_amount    = strcheckAmount;
        bps.check_name      = strcheckName;
        bps.bank_code       = strbankCode;
        bps.type_name       = strtypeName;

        bps.api_encode = api_encode;
        bps.field_status = "ForUploading";
        bps.is_sent = 0;
        bps.save();
        //showDialogAlert("Transactions Saved","Queued");
        processWorkFlow(bps);


    }





}
