package ph.npi.netplay.bayadcenter.activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.JoseException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.service.VolleySingleton;

/**
 * Created by Rhen Nepacena on 11/4/2015.
 */
public class CheckTransactionActivity extends SherlockFragmentActivity implements View.OnClickListener{
    private String bearer_token;
    private Button mbtn_submit;
    private EditText med_externalRefNumber;
    private ProgressBar mpb_waitToLoad;
    private LinearLayout mll_details;
    private String strExternalRefNumber;

    private TextView mtt_channel;
    private TextView mtt_serviceType;
    private TextView mtt_referenceId;
    private TextView mtt_externalReferenceId;
    private TextView mtt_amount;
    private TextView mtt_walletBalance;
    private TextView mtt_timeStamp;
    private TextView mtt_username;

    private Context mContext;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_transactions);
        med_externalRefNumber = (EditText) findViewById(R.id.ed_externalRefNumber);
        mbtn_submit           = (Button) findViewById(R.id.btn_submit);
        mpb_waitToLoad        = (ProgressBar) findViewById(R.id.pb_waitToLoad);
        mll_details           = (LinearLayout) findViewById(R.id.ll_details);
        mtt_channel           = (TextView) findViewById(R.id.tt_channel);
        mtt_serviceType       = (TextView) findViewById(R.id.tt_serviceType);
        mtt_referenceId       = (TextView) findViewById(R.id.tt_referenceId);
        mtt_externalReferenceId = (TextView) findViewById(R.id.tt_externalReferenceId);
        mtt_amount            = (TextView) findViewById(R.id.tt_amount);
        mtt_walletBalance     = (TextView) findViewById(R.id.tt_walletBalance);
        mtt_timeStamp         = (TextView) findViewById(R.id.tt_timeStamp);
        mtt_username          = (TextView) findViewById(R.id.tt_username);

        mContext = this;
        mbtn_submit.setOnClickListener(this);

        getSupportActionBar().setIcon(getResources().getDrawable(R.drawable.ic_action_prepaid));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setTitle("Verify Transactions");

    }


    protected void getTransaction(String transactionref){
        try{
            Date dates = new Date();
            Random r = new Random();
            long i1 = r.nextInt(99999999) + 6500;


            String external_id = String.valueOf(dates.getTime() + i1);
            Log.d("testactivity-id", external_id);

            bearer_token = getAuthorizationBearerToken(external_id);
            Log.d("testactivity-token",bearer_token);

            mpb_waitToLoad.setVisibility(View.VISIBLE);
            mll_details.setVisibility(View.GONE);

        }catch (Exception e){

        }

        RequestQueue queue = VolleySingleton.getsInstance().getRequestQueue();

        String url = "http://125.5.114.138:50005/bayadcenter/v1/transactions/wallet/info/"+transactionref;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        /*
                        {
                         "channel":"API",
                         "service_type":"BILLSPAY",
                         "reference_id":54,
                         "external_reference":"139270898923931",
                         "status":"SUCCESSFUL",
                         "amount":1,
                         "wallet_balance":9985,
                         "timestamp":"2015-11-04T02:47:15.000Z",
                         "username":"bc_android"
                         }
                         */
                        /*
                        {
                         "channel":"API",
                         "service_type":"BILLSPAY",
                         "reference_id":63,
                         "external_reference":"139270898923937",
                         "status":"FAILED",
                         "amount":1,
                         "wallet_balance":9980,
                         "timestamp":"2015-11-04T06:32:36.000Z",
                         "username":"Bayad Center User"
                         }
                         */

                        try{
                            Log.d("checktransactionactivity-resp",response);
                            JSONObject jsonObject = new JSONObject(response);

                            if(jsonObject != null){
                                String strChannel;
                                String strServiceType;
                                String strReferenceId;
                                String strExternalReferenceId;
                                String strStatus;
                                String strAmount;
                                String strWalletBalance;
                                String strTimeStamp;
                                String strUsername;


                                strStatus = jsonObject.getString("status");
                                strChannel     = jsonObject.getString("channel");
                                strServiceType = jsonObject.getString("service_type");
                                strReferenceId = jsonObject.getString("reference_id");
                                strExternalReferenceId = jsonObject.getString("external_reference");
                                strAmount      = jsonObject.getString("amount");
                                strWalletBalance = jsonObject.getString("wallet_balance");
                                strTimeStamp   = jsonObject.getString("timestamp");
                                strUsername    = jsonObject.getString("username");



                                if(strStatus.contains("SUCCESSFUL")){



                                    mpb_waitToLoad.setVisibility(View.GONE);
                                    mll_details.setVisibility(View.VISIBLE);

                                    mtt_channel.setText(strChannel);
                                    mtt_serviceType.setText(strServiceType);
                                    mtt_referenceId.setText(strReferenceId);
                                    mtt_externalReferenceId.setText(strExternalReferenceId);
                                    mtt_amount.setText(strAmount);
                                    mtt_walletBalance.setText(strWalletBalance);
                                    mtt_timeStamp.setText(strTimeStamp);
                                    mtt_username.setText(strUsername);

                                }else if(strStatus.contains("FAILED")){

                                    mpb_waitToLoad.setVisibility(View.GONE);
                                    mll_details.setVisibility(View.VISIBLE);

                                    mtt_channel.setText(strChannel);
                                    mtt_serviceType.setText(strServiceType);
                                    mtt_referenceId.setText(strReferenceId);
                                    mtt_externalReferenceId.setText(strExternalReferenceId);
                                    mtt_amount.setText(strAmount);
                                    mtt_walletBalance.setText(strWalletBalance);
                                    mtt_timeStamp.setText(strTimeStamp);
                                    mtt_username.setText(strUsername);
                                }
                            }else{
                                Toast.makeText(mContext," TRANSACTION NOT FOUND ", Toast.LENGTH_SHORT).show();
                            }

                        }catch (Exception e){
                            mpb_waitToLoad.setVisibility(View.GONE);
                            Toast.makeText(mContext," TRANSACTION NOT FOUND ", Toast.LENGTH_SHORT).show();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("checktransactionactivity-err1",error.toString());
                mpb_waitToLoad.setVisibility(View.GONE);
                Toast.makeText(mContext," TRANSACTION NOT FOUND ", Toast.LENGTH_SHORT).show();
            }

        }
        ){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

            @Override
            public Map<String, String > getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer "+ bearer_token);
                return headers;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    private String getAuthorizationBearerToken(String external_id) throws NoSuchAlgorithmException, JoseException, UnsupportedEncodingException {

        JwtClaims claims = new JwtClaims();

        claims.setIssuedAtToNow();
        claims.setClaim("username","bc_android");
        claims.setClaim("external_reference",external_id);
        claims.setClaim("ip","127.0.0.1");


        String secret_key = "29a9d233e48ffa699c3c1f991943a5518f9123db";
        Key key = new HmacKey(secret_key.getBytes("UTF-8"));

        JsonWebSignature jws1 = new JsonWebSignature();
        jws1.setPayload(claims.toJson());

        jws1.setKey(key);
        jws1.setHeader("typ","JWT");
        jws1.setHeader("alg","HS256");
        jws1.setDoKeyValidation(false);

        jws1.setPayload(claims.toJson());
        String result = jws1.getCompactSerialization();

        return result;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_submit:
                strExternalRefNumber = med_externalRefNumber.getText().toString().trim();
                Log.d("checktransactionactivity-extref",strExternalRefNumber);
                getTransaction(strExternalRefNumber);
                break;


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }


}
