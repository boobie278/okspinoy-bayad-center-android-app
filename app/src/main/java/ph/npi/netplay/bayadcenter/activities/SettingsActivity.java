package ph.npi.netplay.bayadcenter.activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jose4j.base64url.internal.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import ph.npi.netplay.bayadcenter.Epinoyload;
import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.models.Events;
import ph.npi.netplay.bayadcenter.models.User;
import ph.npi.netplay.bayadcenter.service.SyncQueuedTaskNewApi;
import ph.npi.netplay.bayadcenter.service.VolleySingleton;
import ph.npi.netplay.bayadcenter.service.WifiStateTask;


public class SettingsActivity extends SherlockActivity implements View.OnClickListener {

    private Context mContext;
    private TextView mMobileNumberTextView;
    private TextView mAppVersionTextView;

    private String msmspinnerinfo;
    private WebView mdocumentat;
    private RelativeLayout mlnrlyoutOverlay;
    private Button mbtnClose;

    private Button msubmitButton;
    private EditText metUsername;
    private EditText metPassword;
    private TextView ttStatusIndicator;


    private String strUsername;
    private String strPassword;

    private static final String AUTHORIZATION = "Authorization";
    private static final String BAYADCENTER_LOGIN = "http://125.5.114.138:50005/bayadcenter/v1/authenticate";
    private String responds;

    final Handler myHandler = new Handler();


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent billerCategory = new Intent(SettingsActivity.this, MainActivity.class);
        SettingsActivity.this.startActivity(billerCategory);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mContext = this;

        getSupportActionBar().setIcon(R.drawable.ic_action_gear_settings);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Settings");


        mMobileNumberTextView = (TextView) findViewById(R.id.settingsMobileNumberTextView);
        mAppVersionTextView = (TextView) findViewById(R.id.settingsAppVersionTextView);
        msubmitButton = (Button) findViewById(R.id.submitButton);

        metUsername = (EditText) findViewById(R.id.etUsername);
        metPassword = (EditText) findViewById(R.id.etPassword);

        ttStatusIndicator = (TextView) findViewById(R.id.ttStatusIndicator);

        LinearLayout settingsMobileNumberLinearLayout = (LinearLayout) findViewById(R.id.settingsMobileNumberLinearLayout);
        settingsMobileNumberLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mobileNumber = new Intent(SettingsActivity.this, MobileNumberActivity.class);
                mobileNumber.putExtra("fromSettings", true);
                SettingsActivity.this.startActivity(mobileNumber);
            }
        });


        msubmitButton.setOnClickListener(this);

        startServices();



        Timer myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Log.d("homefragment", "updating UI");

                updateUi();

            }
        }, 0, 1000);



        populateSettings();


    }


    public void updateUi() {
        myHandler.post(myRunnable);
    }

    final Runnable myRunnable = new Runnable() {
        public void run() {
            if(WifiStateTask.isOnline == true){
                ttStatusIndicator.setText("You are now Online");
                msubmitButton.setEnabled(true);
            }else{
                ttStatusIndicator.setText("You are now Offline");
                msubmitButton.setEnabled(false);
            }

        }
    };

    private void saveUserPassword() {
        strUsername = metUsername.getText().toString().trim().replaceAll(" ", "");
        strPassword = metPassword.getText().toString().trim().replaceAll(" ", "");

        new AsyncTask<String,String,String>(){

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params) {
                try{

                    HttpGet request = new HttpGet(BAYADCENTER_LOGIN);
                    String auth = strUsername + ":" + strPassword;
                    byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("ISO-8859-1")));
                    String authHeader = "Basic " + new String(encodedAuth);
                    request.setHeader(AUTHORIZATION, authHeader);

                    HttpResponse httpresponse = new DefaultHttpClient().execute(request);
                    StatusLine statusline = httpresponse.getStatusLine();
                    //Log.d("settingsactivity-res",responds);
                    if(statusline.getStatusCode() == 200){
                        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
                        httpresponse.getEntity().writeTo(bytearrayoutputstream);
                        responds	=	bytearrayoutputstream.toString();
                        bytearrayoutputstream.close();

                        if(responds.contains("SUCCESS")){
                            processUserAuth(strUsername,strPassword, true);
                            Intent checkTransactions = new Intent(mContext, MainActivity.class);
                            mContext.startActivity(checkTransactions);
                            finish();

                        }else{
                            processUserAuth(strUsername,strPassword, false);
                        }

                    }else{
                        httpresponse.getEntity().getContent().close();
                        throw new IOException(statusline.getReasonPhrase());
                    }


                }catch (Exception e){
                    Log.d("settingsactivity-err",e.toString());
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
            }
        }.execute();

    }





    private void populateSettings() {

        mMobileNumberTextView.setText(Epinoyload.getMobileNumber(mContext));
        try {
            String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            mAppVersionTextView.setText("v" + versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        User mUser = new User(mContext);
        if(((mUser.getUsername()).equals("") && (mUser.getPassword()).equals(""))){
           //Do nothing
        }else{
            metUsername.setText(mUser.getUsername());
            metPassword.setText(mUser.getPassword());

        }

    }



    private void processUserAuth(String username,String password, Boolean isAuthenticated){
        User mUser = new User(mContext);
        mUser.setUsername(username);
        mUser.setPassword(password);
        mUser.setAuthentication(isAuthenticated);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent billerCategory = new Intent(SettingsActivity.this, MainActivity.class);
                SettingsActivity.this.startActivity(billerCategory);

                finish();
                return true;

            case R.id.needhelp:
                /*Intent intent = new Intent(SettingsActivity.this, DocumentationActivity.class);
                intent.putExtra("helpinfo",Epinoyload.DOCU_SETTINGS);
                SettingsActivity.this.startActivity(intent);*/
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.settings, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        populateSettings();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    private  String sha1(String toHash) {
        String hash = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            byte[] bytes = toHash.getBytes("UTF-8");
            digest.update(bytes, 0, bytes.length);
            bytes = digest.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b : bytes) {
                sb.append(String.format("%02X", b));
            }
            hash = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return hash.toLowerCase(Locale.ENGLISH);
    }

    protected String urlencode(String value) {
        return URLEncoder.encode(value);
    }





    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.submitButton:
                saveUserPassword();
            break;
        }
    }




    public void startServices() {

        Log.i("controller-settings", "start services - start");

        startService(WifiStateTask.class);
        //startService(SyncQueuedTaskNewApi.class);

        Log.i("controller-settings", "start services - end");
    }


    public void stopServices() {
        Log.i("controller", "stop services");
        if (isServiceRunning(WifiStateTask.class, mContext)) {
            WifiStateTask.cancel();
            stopService(WifiStateTask.class);
        }
    }




    private void startService(Class<?> serviceClass){
        if(isServiceNotRunning(serviceClass, mContext))
            mContext.startService(new Intent(mContext, serviceClass));
    }

    private void stopService(Class<?> serviceClass){
        if(isServiceRunning(serviceClass, mContext)){
            mContext.stopService(new Intent(mContext, serviceClass));
        }
    }


    private boolean isServiceRunning(Class<?> serviceClass,Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private boolean isServiceNotRunning(Class<?> serviceClass,Context context) {
        return !isServiceRunning(serviceClass, context);
    }


}
