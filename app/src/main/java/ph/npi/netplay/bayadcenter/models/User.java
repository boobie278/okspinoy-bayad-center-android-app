package ph.npi.netplay.bayadcenter.models;

import android.content.Context;
import android.content.SharedPreferences;

import ph.npi.netplay.bayadcenter.Epinoyload;


/**
 * Created by Rhen Nepacena on 10/29/15.
 */
public class User {

    private final Context context;
    private String username;
    private String password;
    private boolean isAuthentication;

    public User(Context ctx) {
        this.context = ctx;
    }

    public String getUsername() {
        if (username == null) {
            SharedPreferences prefs = context.getSharedPreferences(Epinoyload.PREFS_LOCATION, Context.MODE_PRIVATE);
            this.username = prefs.getString("username", "");
        }
        return this.username;
    }

    public void setUsername(String username) {
        username = username.trim();
        SharedPreferences prefs = context.getSharedPreferences(Epinoyload.PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putString("username", username).commit();
    }

    public String getPassword() {
        if (password == null) {
            SharedPreferences prefs = context.getSharedPreferences(Epinoyload.PREFS_LOCATION, Context.MODE_PRIVATE);
            this.password = prefs.getString("password", "");
        }
        return this.password;
    }

    public Boolean getAuthentication() {
        if (isAuthentication == false) {
            SharedPreferences prefs = context.getSharedPreferences(Epinoyload.PREFS_LOCATION, Context.MODE_PRIVATE);
            this.isAuthentication = prefs.getBoolean("isAuthentication", false);
        }
        return this.isAuthentication;
    }

    public void setAuthentication(Boolean isAuthentication) {
        SharedPreferences prefs = context.getSharedPreferences(Epinoyload.PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putBoolean("isAuthentication", isAuthentication).commit();
    }

    public void setPassword(String password) {
        password = password.trim();
        SharedPreferences prefs = context.getSharedPreferences(Epinoyload.PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putString("password", password).commit();
    }

    public boolean authenticateLocally(String aUsername, String aPassword) {
        if (aUsername.equals(getUsername()) && aPassword.equals(getPassword())) {
            return true;
        }
        return false;
    }

    private String md5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }
}
