package ph.npi.netplay.bayadcenter.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import java.util.HashMap;

/**
 * Created by Rhen Nepacena on 10/28/2014.
 */
public class CAutoCompleteTextView extends AutoCompleteTextView {

    public CAutoCompleteTextView(Context context,AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected CharSequence convertSelectionToString(Object selectedItem) {
        HashMap<String, String> hm = (HashMap<String, String>) selectedItem;
        return hm.get("number");
    }
}
