package ph.npi.netplay.bayadcenter.fragment;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.JoseException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.RoundingMode;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import ph.npi.netplay.bayadcenter.Epinoyload;
import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.activities.BillersPayActivity;
import ph.npi.netplay.bayadcenter.activities.BillersPayBayanActivity;
import ph.npi.netplay.bayadcenter.activities.BillersPayDVoltActivity;
import ph.npi.netplay.bayadcenter.activities.BillersPayGlobeActivity;
import ph.npi.netplay.bayadcenter.activities.BillersPayHMDFActivity;
import ph.npi.netplay.bayadcenter.activities.BillersPayMCWDActivity;
import ph.npi.netplay.bayadcenter.activities.BillersPayMWActivity;
import ph.npi.netplay.bayadcenter.activities.BillersPayMWCOMActivity;
import ph.npi.netplay.bayadcenter.activities.BillersPayManuActivity;
import ph.npi.netplay.bayadcenter.activities.BillersPayMecoaActivity;
import ph.npi.netplay.bayadcenter.activities.BillersPayPLDTActivity;
import ph.npi.netplay.bayadcenter.activities.BillersPayPhlthActivity;
import ph.npi.netplay.bayadcenter.activities.BillersPaySSSActivity;
import ph.npi.netplay.bayadcenter.activities.BillersPaySkyActivity;
import ph.npi.netplay.bayadcenter.activities.BillersPaySmartActivity;
import ph.npi.netplay.bayadcenter.activities.BillersPaySunActivity;
import ph.npi.netplay.bayadcenter.activities.BillersPayViecoActivity;
import ph.npi.netplay.bayadcenter.activities.BillersSearchesActivity;
import ph.npi.netplay.bayadcenter.activities.TestActivity;
import ph.npi.netplay.bayadcenter.adapters.BillersSearchAdapter;
import ph.npi.netplay.bayadcenter.adapters.BillsPayTransactionsAdapter;
import ph.npi.netplay.bayadcenter.libs.syncdata.SyncBillers;
import ph.npi.netplay.bayadcenter.libs.syncdata.SyncInterface;
import ph.npi.netplay.bayadcenter.libs.syncdata.SyncPaymentTypes;
import ph.npi.netplay.bayadcenter.models.Billers;
import ph.npi.netplay.bayadcenter.models.BillsPay;
import ph.npi.netplay.bayadcenter.models.Events;
import ph.npi.netplay.bayadcenter.models.PaymentTypes;
import ph.npi.netplay.bayadcenter.models.User;
import ph.npi.netplay.bayadcenter.service.SyncQueuedTaskNewApi;
import ph.npi.netplay.bayadcenter.service.VolleySingleton;
import ph.npi.netplay.bayadcenter.service.WifiStateTask;
import ph.npi.netplay.bayadcenter.views.NestedListView;


public class MainFragment extends SherlockFragment {

    private Context mContext;

    private ScrollView mscrlMain;
    private TextView mtt_walletBalance;
    private TextView mttTpaId;
    private EditText med_searchBillers;
    private ListView mlstvwBillers;

    private String bearer_token;
    private RelativeLayout mrl_updateBalance;

    ArrayAdapter<Billers> billeradapter;
    List<Billers> searchTemp = new ArrayList<Billers>();
    List<Billers> searchBillersList = new ArrayList<Billers>();
    List<Billers> newSearchBillersList = new ArrayList<Billers>();
    List<String> searchContents = new ArrayList<String>();
    List<Billers> billers = new ArrayList<Billers>();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view                   = inflater.inflate(R.layout.fragment_home, container, false);
        mContext                    = getActivity();
        /*mscrlMain                   = (ScrollView) view.findViewById(R.id.scrlMain);

        mscrlMain.fullScroll(View.FOCUS_DOWN);*/

        mtt_walletBalance           = (TextView) view.findViewById(R.id.tt_walletBalance);
        mrl_updateBalance           = (RelativeLayout) view.findViewById(R.id.rl_updateBalance);
        med_searchBillers           = (EditText) view.findViewById(R.id.ed_searchBillers);
        mlstvwBillers               = (ListView) view.findViewById(R.id.lstvwBillers);

        mttTpaId  = (TextView) view.findViewById(R.id.ttTpaId);

        User mUser = new User(mContext);

        if(mUser.getUsername() != null){
            mttTpaId.setText(mUser.getUsername());
        }



        sync();
        getBillerList();
        med_searchBillers.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                String newText = charSequence.toString();
                newSearchBillersList.clear();
                if (newText.length() > 0) {
                    int j = 0;
                    boolean searchCheck = false;
                    String[] searchTemp = newText.split(" ");
                    for (String search : searchContents) {
                        for (String temp : searchTemp) {
                            temp = temp.replace(" ", "");
                            if (search.trim().toLowerCase().contains((temp.toLowerCase().replace(" ", "")))) {
                                searchCheck = true;
                            } else {
                                searchCheck = false;
                                break;
                            }
                        }

                        if (searchCheck) {
                            newSearchBillersList.add(searchBillersList.get(j));
                        }

                        j++;
                    }



                    //add to adapter the new list
                    billeradapter = new BillersSearchAdapter(mContext, newSearchBillersList);
                    mlstvwBillers.setAdapter(billeradapter);
                    mlstvwBillers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            /*Billers billerinfo = newSearchBillersList.get(i);
                            Intent intent = new Intent(BillersSearchesActivity.this, BillersPayActivity.class);
                            intent.putExtra("biller_name",billerinfo.name);
                            BillersSearchesActivity.this.startActivity(intent);*/

                            Billers bi = newSearchBillersList.get(i);
                            processBillerSelection(bi);



                        }
                    });
                    billeradapter.notifyDataSetChanged();

                } else {
                    searchContents.clear();
                    getBillerList();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });



        mrl_updateBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getBalance();
            }
        });


        TextView offlineTextView = (TextView) view.findViewById(R.id.offlineTextView);
        if (!Epinoyload.isOffline(mContext)) {
            offlineTextView.setVisibility(View.GONE);
        }


        Timer myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {

            }
        }, 0, 1000);

        getBalance();


        return view;
    }


    private void getBillerList() {

        searchBillersList.clear();

        billers = new Billers().get();

        if (billers.size() <= 0) {
            //mNoBrandsLinearLayout.setVisibility(View.VISIBLE);
            //mlnrlyoutbillers.setVisibility(View.GONE);
            return;
        }


        //mNoBrandsLinearLayout.setVisibility(View.GONE);
        //mlnrlyoutbillers.setVisibility(View.VISIBLE);

        for(Billers bills: billers){
            searchBillersList.add(bills);
            searchContents.add((bills.label.toString().toLowerCase().replace(" ", "")));

        }


        billeradapter = new BillersSearchAdapter(mContext, searchBillersList);
        mlstvwBillers.setAdapter(billeradapter);
        mlstvwBillers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                /*Billers billerinfo = searchBillersList.get(i);
                Intent intent = new Intent(BillersSearchesActivity.this, BillersPayActivity.class);
                intent.putExtra("biller_name",billerinfo.name);
                BillersSearchesActivity.this.startActivity(intent);*/



                Billers bi = searchBillersList.get(i);
                processBillerSelection(bi);

            }
        });
        billeradapter.notifyDataSetChanged();


    }


    public void processBillerSelection(Billers bi){
        //hardcoded to be replaced soon...
        startServices();
        if((bi.name).equals("Meralco")){
            updateBillersCounting(bi,BillersPayMecoaActivity.class);
        }else if((bi.name).equals("Manulife")){
            updateBillersCounting(bi,BillersPayManuActivity.class);
        }else if((bi.name).equals("Manila Water")){
            updateBillersCounting(bi,BillersPayMWActivity.class);
        }else if((bi.name).equals("PLDT")){
            updateBillersCounting(bi,BillersPayPLDTActivity.class);
        }else if((bi.name).equals("Sky Cable")){
            updateBillersCounting(bi,BillersPaySkyActivity.class);
        }else if((bi.name).equals("Bayan")){
            updateBillersCounting(bi,BillersPayBayanActivity.class);
        }else if((bi.name).equals("MCWD")){
            updateBillersCounting(bi,BillersPayMCWDActivity.class);
        }else if((bi.name).equals("MWCOM")){
            updateBillersCounting(bi,BillersPayMWCOMActivity.class);
        }else if((bi.name).equals("GLOBE")){
            updateBillersCounting(bi,BillersPayGlobeActivity.class);
        }else if((bi.name).equals("SMART")){
            updateBillersCounting(bi,BillersPaySmartActivity.class);
        }else if((bi.name).equals("SUNCL")){
            updateBillersCounting(bi,BillersPaySunActivity.class);
        }else if((bi.name).equals("DVolt")){
            updateBillersCounting(bi,BillersPayDVoltActivity.class);
        }else if((bi.name).equals("VIECO")){
            updateBillersCounting(bi,BillersPayViecoActivity.class);
        }else if((bi.name).equals("PHLTH")){
            updateBillersCounting(bi,BillersPayPhlthActivity.class);
        }else if((bi.name).equals("HDMF1")){
            updateBillersCounting(bi,BillersPayHMDFActivity.class);
        }else if((bi.name).equals("SSS")){
            updateBillersCounting(bi,BillersPaySSSActivity.class);
        }

        Events.inputEventLogs(bi.name+" Paybills Item Selected");


    }


    private void sync(){
        Billers biller = new Billers();
        if(biller.getBillersCount() <= 0){
            final ProgressDialog syncProgressDialog = new ProgressDialog(mContext);
            syncProgressDialog.setCancelable(false);
            syncProgressDialog.setMessage("Syncing data ...");
            syncProgressDialog.show();
            SyncBillers syncProducts = new SyncBillers(mContext, new SyncInterface() {
                @Override
                public void onComplete() {
                    syncProgressDialog.dismiss();

                }
            });
            syncProducts.execute(true);

        }

        PaymentTypes paymentTypes = new PaymentTypes();
        if(paymentTypes.getPaymentTypeCount() <= 0){

            SyncPaymentTypes syncPaymentTypes = new SyncPaymentTypes(mContext, new SyncInterface() {
                @Override
                public void onComplete() {


                }
            });
            syncPaymentTypes.execute(true);
        }


    }


    public void updateBillersCounting(Billers bi,Class mClass){
        Intent intent = null;
        Billers bi1 = bi.getBillersByCount(bi.name);
        if(bi1 != null){
            bi1.count +=1;
            Long checkUpdate = bi1.save();
            if(checkUpdate > 0){
                try{
                    intent = new Intent(mContext,mClass);
                    intent.putExtra("biller_name",bi.name);
                    getActivity().startActivity(intent);

                }catch (NullPointerException e){
                    e.printStackTrace();
                    return;
                }catch (ActivityNotFoundException e){
                    e.printStackTrace();
                    return;
                }
            }else{
                Toast.makeText(mContext,"Update Failed",Toast.LENGTH_SHORT).show();
            }
        }else{
            return;
        }
    }



    protected void getBalance(){
        try{
            Date dates = new Date();
            Random r = new Random();
            long i1 = r.nextInt(99999999) + 6500;


            String external_id = String.valueOf(dates.getTime() + i1);
            Log.d("mainfragment-id",external_id);

            bearer_token = getAuthorizationBearerToken(external_id);
            Log.d("mainfragment-token",bearer_token);

        }catch (Exception e){

        }

        RequestQueue queue = VolleySingleton.getsInstance().getRequestQueue();

        String url = "http://125.5.114.138:50005/bayadcenter/v1/transactions/wallet/balance";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        /*
                         {"wallet_balance":8616}
                       */
                        if(response != null){
                            try{
                                JSONObject jsonObject = new JSONObject(response);
                                if(jsonObject != null){
                                    String strBalance   = jsonObject.getString("wallet_balance");
                                    Double dblBalance   = Double.parseDouble(strBalance);
                                    Float  fltBalance   = Float.parseFloat(strBalance);

                                    DecimalFormat money = new DecimalFormat("##,###.##");
                                    money.setRoundingMode(RoundingMode.UP);

                                    mtt_walletBalance.setText("P " + money.format(dblBalance));

                                    Epinoyload.setBalanceLastUpdate(mContext);
                                    Epinoyload.setBalance(mContext, fltBalance, Epinoyload.KEY_LOAD_BAL);
                                    populateBalanceOffline();

                                }
                            }catch (JSONException e){

                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("testactivity-err1",error.toString());
            }

        }
        ){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

            @Override
            public Map<String, String > getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer "+bearer_token);
                return headers;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    private void populateBalanceOffline() {

        String lastUpdate = Epinoyload.getBalanceLastUpdatePretty(mContext);
        mtt_walletBalance.setText(lastUpdate);
        if (lastUpdate.equals("")) {
            mtt_walletBalance.setText("0.00");
            return;
        }
        DecimalFormat money = new DecimalFormat("##,###.##");
        money.setRoundingMode(RoundingMode.UP);
        mtt_walletBalance.setText("PHP " + money.format(Double.parseDouble(Epinoyload.getBalance(mContext, Epinoyload.KEY_LOAD_BAL) + "")));
    }

    private String getAuthorizationBearerToken(String external_id) throws NoSuchAlgorithmException, JoseException, UnsupportedEncodingException {

        JwtClaims claims = new JwtClaims();

        claims.setIssuedAtToNow();
        claims.setClaim("username","bc_android");
        claims.setClaim("external_reference",external_id);
        claims.setClaim("ip","127.0.0.1");

        String secret_key = "29a9d233e48ffa699c3c1f991943a5518f9123db";
        Key key = new HmacKey(secret_key.getBytes("UTF-8"));

        JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());

        jws.setKey(key);
        jws.setHeader("typ","JWT");
        jws.setHeader("alg","HS256");
        jws.setDoKeyValidation(false);

        jws.setPayload(claims.toJson());
        String result = jws.getCompactSerialization();

        return result;
    }



    public void startServices() {

        Log.i("controller-main", "start services - start");

        startService(WifiStateTask.class);
        //startService(SyncQueuedTaskNewApi.class);

        Log.i("controller-main", "start services - end");
    }


    public void stopServices() {
        Log.i("controller-main", "stop services");
        if (isServiceRunning(WifiStateTask.class, mContext)) {
            WifiStateTask.cancel();
            stopService(WifiStateTask.class);
        }
    }


    private void startService(Class<?> serviceClass){
        if(isServiceNotRunning(serviceClass, mContext))
            mContext.startService(new Intent(mContext, serviceClass));
    }

    private void stopService(Class<?> serviceClass){
        if(isServiceRunning(serviceClass, mContext)){
            mContext.stopService(new Intent(mContext, serviceClass));
        }
    }

    private boolean isServiceRunning(Class<?> serviceClass,Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private boolean isServiceNotRunning(Class<?> serviceClass,Context context) {
        return !isServiceRunning(serviceClass, context);
    }




}
