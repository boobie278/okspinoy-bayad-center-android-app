package ph.npi.netplay.bayadcenter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.mediatek.compatibility.gemini.GeminiSupport;
import com.mediatek.telephony.TelephonyManagerEx;

import java.util.Date;

import ph.npi.netplay.bayadcenter.libs.EpinoyTelephonyInfo;
import ph.npi.netplay.bayadcenter.libs.prettydate.PrettyDate;


/**
 * Created by Rhen Nepacena on 10/29/15.
 */
public class Epinoyload {

    public final static String PREFS_LOCATION = "ph.npi.netplay.bayadcenter";
    public final static String KEY_BILLER_BAL = "billsbalance";
    public final static String KEY_LOAD_BAL   = "balance";

    public final static int DOCU_SETTINGS = 1;
    public final static int DOCU_ADDRETAILER = 2;
    public final static int DOCU_PASACREDITS = 3;
    public final static int DOCU_CELLPHONELOAD = 4;
    public final static int DOCU_BILLSPAY = 5;
    public final static int DOCU_REGISTER = 6;


    public static void showAlert(Context context, String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Set action here for first button
                return;
            }
        });
        alertDialog.show();
    }

    public static void showAlert(Context context, String title, String message, DialogInterface.OnClickListener positiveDialogOnClickListener) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", positiveDialogOnClickListener);
        alertDialog.show();
    }

    public static void showAlert(Context context, String title, String message, DialogInterface.OnClickListener positiveDialogOnClickListener, DialogInterface.OnClickListener negativeDialogOnClickListener) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", positiveDialogOnClickListener);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", negativeDialogOnClickListener);
        alertDialog.show();
    }

    public static void setBalanceLastUpdate(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putLong("balanceLastUpdate", System.currentTimeMillis()).commit();
    }



    public static String getBalanceLastUpdatePretty(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        long lastUpdateTime = prefs.getLong("balanceLastUpdate", 0);
        if (lastUpdateTime == 0) {
            return "";
        }
        PrettyDate prettyDate = new PrettyDate(new Date(lastUpdateTime));
        return "Updated " + prettyDate.toString().toLowerCase().trim();
    }

    public static void setBalance(Context context, float balance, String key) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
//        prefs.edit().putFloat("balance", balance).commit();
        prefs.edit().putFloat(key, balance).commit();
    }



    public static float getBalance(Context context,String key) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
//        return prefs.getFloat("balance", 0);
        return prefs.getFloat(key, 0);
    }

    public static boolean isOffline(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        return prefs.getBoolean("isOffline", false);
    }

    public static void setIsOffline(Context context, boolean isOffline) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putBoolean("isOffline", isOffline).commit();
    }

    public static void setDashboard(Context context, String dashboard){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putString("dsdashboard", dashboard).commit();
    }

    public static String getDashboard(Context context,String key){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        return prefs.getString(key, "");
    }

    public static void setBrandsPointer(Context context,String brands){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putString("dbrand", brands).commit();
    }

    public static String getBrandsPointer(Context context,String key){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        return prefs.getString(key, "");
    }

    public static void setShowcase(Context context,String key ,boolean value){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putBoolean(key, value).commit();
    }

    public static boolean getShowcase(Context context,String key){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        return prefs.getBoolean(key, false);
    }














    ///////////////////////////////////////////////////////

    public static final int SIM01 = 0;
    public static final int SIM02 = 1;
    public static final int SIM_STATE_READY = 5;

    public static boolean isDualSim(Context context){
        boolean checkDualSim = false;



            try{
                TelephonyManagerEx telephonyManagerEx =  new TelephonyManagerEx(context);
                if((telephonyManagerEx.getDeviceId(0).length()> 0) && (telephonyManagerEx.getDeviceId(1).length() > 0)){
                    checkDualSim = true;
                }else{
                    checkDualSim = false;
                }
            }catch (Exception e){
                Log.e("[DualSim-dualsim]",e.toString());
                checkDualSim = false;
            }

        return checkDualSim;
    }

    public static int getSimState(Context context,int simid){
        int simState = 0;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);


        try{
            TelephonyManagerEx telephonyManagerEx =  new TelephonyManagerEx(context);
            simState = telephonyManagerEx.getSimState(simid);

        }catch (Exception e){
            Log.e("[DualSim-state]",e.toString());
            // SIM_STATE_ABSENT
            //simState = 1;
            simState = telephonyManager.getSimState();

        }

        return simState;
    }

    public static String getNetworkOperatorName(Context context,int simid){
        String operatorName = "";
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            try{
                TelephonyManagerEx telephonyManagerEx =  new TelephonyManagerEx(context);
                operatorName = telephonyManagerEx.getNetworkOperatorName(simid);
            }catch(Exception e){
                Log.e("[DualSim-networkop]",e.toString());
                operatorName = telephonyManager.getNetworkOperatorName();
            }

        return operatorName;
    }

    public static String getLineNumber(Context context,int simid){
        String lineNumber = "";
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            try{
                TelephonyManagerEx telephonyManagerEx =  new TelephonyManagerEx(context);
                lineNumber = telephonyManagerEx.getLine1Number(simid);
            }catch (Exception e){
                Log.e("[DualSim-lineNumber]",e.toString());
                lineNumber = telephonyManager.getLine1Number();
            }


        return lineNumber;
    }

    //mobileSimSlut
    public static void setSelectedSimSlot(Context context, int value){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putInt("mobileSimSlut", value).commit();
    }

    public static int getSelectedSimSlot(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        return prefs.getInt("mobileSimSlut", 0);
    }

    //mobileNumberSim01
    //mobileNumberSim02

    public static void setLineNumber1(Context context,String key ,String value){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putString(key, value).commit();
    }

    public static void setLineNumber2(Context context,String key ,String value){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putString(key, value).commit();
    }

    public static String getSimMobileNumber(Context context,String key) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        String number = prefs.getString(key, "");
        return number;
    }


    public static void setSMSProvider(Context context,String value){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putString("dbSMSProvider", value).commit();
    }


    public static String getSMSProvider(Context context,String key){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        return prefs.getString(key, "");
    }


    public static void setMobNumber(Context context ,String value){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putString("mobileNumberFanatics", value).commit();
    }

    public static String getMobNumber(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        return prefs.getString("mobileNumberFanatics","");
    }


    //as of v25
    //mClearAll
    public static void setToClearData(Context context,String key ,String value){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putString(key, value).commit();
    }

    public static String getToClearData(Context context,String key){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        return prefs.getString(key,"");
    }


    /*private static boolean isMediaTek(Context context){
        boolean indicator = false;
        String info = "";

        try{
            TelephonyManagerEx telephonyManagerEx =  new TelephonyManagerEx(context);
            info = telephonyManagerEx.getLine1Number(Epinoyload.SIM01);
            indicator = true;
        }catch (Exception e){
            Log.e("[DualSim-processor]",e.toString());
            indicator = false;
        }

        return indicator;
    }*/

    private static boolean isMediaTek(Context context){
        boolean indicator = false;
        String info = "";

        try{
            TelephonyManagerEx telephonyManagerEx =  new TelephonyManagerEx(context);
            info = telephonyManagerEx.getLine1Number(Epinoyload.SIM01);
            indicator = true;
        }catch (Exception e){
            Log.e("[DualSim-processor]",e.toString());
            indicator = false;
        }

        return indicator;
    }

    //isMediatek
    public static void setIsMediaTek(Context context){
        boolean indicator = isMediaTek(context);
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putBoolean("isMediatekFan", indicator).commit();
    }

    public static boolean getIsMediaTek(Context context,String key){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        return prefs.getBoolean(key,false);
    }


    public static String getMobileNumber(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        String number = prefs.getString("mobileNumberFanatics", "");
        return number;
    }



    public static void setMobileNumber(Context context, String mobileNumber) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putString("mobileNumberFanatics", mobileNumber).commit();
    }


    public static boolean getDataChecker(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        boolean isExisted = prefs.getBoolean("isExistedCool", false);
        return isExisted;
    }

    public static void setDataChecker(Context context, boolean isExisted) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putBoolean("isExistedCool", isExisted).commit();
    }




    ///////////////////////////////////////////////////////////////





    public static String checkProvider(Context context){
        TelephonyManager tMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return  String.valueOf(tMgr.getNetworkOperatorName()).toUpperCase();
    }

    public static boolean checkDualSim(Context context){
        EpinoyTelephonyInfo epinoyTelephonyInfo = EpinoyTelephonyInfo.getInstance(context);


        boolean  isDualSIM = epinoyTelephonyInfo.isDualSIM();

        return isDualSIM;
    }



    public static String switchSim(Context context,String switcher,String provider){
        EpinoyTelephonyInfo epinoyTelephonyInfo = EpinoyTelephonyInfo.getInstance(context);
        TelephonyManager tMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String mLinenumber = String.valueOf(getMobileNumber(context));
        String mLinenumber1 = String.valueOf(getLineNumber1(context,"SIMLine01"));
        String mLinenumber2 = String.valueOf(getLineNumber2(context,"SIMLine02"));
        String result = null;

        boolean isSIM1Ready = epinoyTelephonyInfo.isSIM1Ready();
        boolean isSIM2Ready = epinoyTelephonyInfo.isSIM2Ready();


        if(checkDualSim(context) == true){
            /*String imsiSIM1 = epinoyTelephonyInfo.getImeiSIM1();
            String imsiSIM2 = epinoyTelephonyInfo.getImeiSIM2();*/




            if(switcher.equals("SIM01")) {

                if(isSIM1Ready == true){


                    if((provider).equals("SMART")||
                            (provider).equals("TALK N TEXT")||
                            (provider).equals("SMART PREPAID")){

                        /*if((tMgr.getLine1Number()).equals(mLinenumber) ){
                            result = tMgr.getLine1Number();
                        }else{
                            Toast.makeText(context,"Invalid Mobile Number",Toast.LENGTH_SHORT).show();
                        }*/

                        if((mLinenumber1).equals(mLinenumber) ){
                            result = mLinenumber1;
                        }else{
                            Toast.makeText(context,"Invalid Mobile Number",Toast.LENGTH_SHORT).show();
                        }

                    }else if((provider).equals("GLOBE TELECOM")||(provider).equals("GLOBE")){
                        if((mLinenumber2).equals("") || (mLinenumber2).equals(null) || (mLinenumber2).isEmpty()){
                            result = mLinenumber2;
//                            Toast.makeText(context,String.valueOf("Globe "+result),Toast.LENGTH_SHORT).show();
                        }else{
                            result = mLinenumber2;
//                            Toast.makeText(context,String.valueOf("Globe else "+result),Toast.LENGTH_SHORT).show();
                        }

                    }

                }else{
                    result = null;
                    Toast.makeText(context,"sim01 not available",Toast.LENGTH_SHORT).show();
                }

            }else if(switcher.equals("SIM02")){

                if(isSIM2Ready == true){

                    if((provider).equals("SMART")||
                            (provider).equals("TALK N TEXT")||
                            (provider).equals("SMART PREPAID")){

                        if((tMgr.getLine1Number()).equals(mLinenumber) ){
                            result = tMgr.getLine1Number();
                        }else{
                            Toast.makeText(context,"Invalid Mobile Number",Toast.LENGTH_SHORT).show();
                        }

                    }else if((provider).equals("GLOBE TELECOM")||(provider).equals("GLOBE")){

                        if((mLinenumber2).equals("") || (mLinenumber2).equals(null) || (mLinenumber2).isEmpty()){
                            result = mLinenumber2;
                            EpinoyTelephonyInfo.getInstance(context);
                            Log.d("encryptedsms", String.valueOf(GeminiSupport.isGeminiFeatureEnabled()));
//                            Toast.makeText(context,String.valueOf("Globe "+result),Toast.LENGTH_SHORT).show();
                        }else{
                            result = mLinenumber2;
//                            Toast.makeText(context,String.valueOf("Globe else "+result),Toast.LENGTH_SHORT).show();
                        }

                    }

                }else{
                    result = null;
                    Toast.makeText(context,"sim02 not available",Toast.LENGTH_SHORT).show();
                }
            }
        }else if(checkDualSim(context) == false){
            /*String imsiSIM1 = epinoyTelephonyInfo.getImeiSIM1();
            String imsiSIM2 = epinoyTelephonyInfo.getImeiSIM2();*/

            if(switcher.equals("SIM01")) {

                if(isSIM1Ready == true){


                    if((provider).equals("SMART")||
                            (provider).equals("TALK N TEXT")||
                            (provider).equals("SMART PREPAID")){

                        if((tMgr.getLine1Number()).equals(mLinenumber) ){
                            result = tMgr.getLine1Number();
                        }else{
                            Toast.makeText(context,"Invalid Mobile Number",Toast.LENGTH_SHORT).show();
                        }

                    }else if((provider).equals("GLOBE TELECOM")||(provider).equals("GLOBE")){
                        if((mLinenumber2).equals("") || (mLinenumber2).equals(null) || (mLinenumber2).isEmpty()){
                            result = mLinenumber2;
                            //Toast.makeText(context,String.valueOf("Globe "+result),Toast.LENGTH_SHORT).show();
                        }else{
                            result = mLinenumber2;
                            //Toast.makeText(context,String.valueOf("Globe fuck "+result),Toast.LENGTH_SHORT).show();
                        }

                    }

                }else{
                    result = null;
                    Toast.makeText(context,"sim01 not available",Toast.LENGTH_SHORT).show();
                }

            }
        }




        return result;

    }




    public static String checkLineNumber(Context context,String provider){
        TelephonyManager tMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String mLinenumber = String.valueOf(getMobileNumber(context));
        String mLinenumber1 = String.valueOf(getLineNumber1(context,"SIMLine01"));
        String mLinenumber2 = String.valueOf(getLineNumber2(context,"SIMLine02"));

        String result = null ;



        if((provider).equals("SMART")||
           (provider).equals("TALK N TEXT")||
           (provider).equals("SMART PREPAID")){

            if((tMgr.getLine1Number()).equals(mLinenumber) ){
                result = tMgr.getLine1Number();
            }else{
                Toast.makeText(context,"Invalid Mobile Number",Toast.LENGTH_SHORT).show();
            }
        }else if((checkProvider(context)).equals("GLOBE TELECOM")||(checkProvider(context)).equals("GLOBE")){

            if((mLinenumber2).equals("") || (mLinenumber2).equals(null) || (mLinenumber2).isEmpty()){
                result = mLinenumber2;
                Toast.makeText(context,String.valueOf("Globe "+result),Toast.LENGTH_SHORT).show();
            }else{
                result = mLinenumber2;
                Toast.makeText(context,String.valueOf("Globe else "+result),Toast.LENGTH_SHORT).show();
            }

        }

        return result;
    }



    public static String getLineNumber1(Context context,String key){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION,Context.MODE_PRIVATE);
        return prefs.getString(key,"");
    }

    public static String getLineNumber2(Context context,String key){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION,Context.MODE_PRIVATE);
        return prefs.getString(key,"");
    }


    public static void setProvider(Context context,String key ,String value){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putString(key, value).commit();
    }

    public static String getProvider(Context context,String key){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_LOCATION,Context.MODE_PRIVATE);
        return prefs.getString(key,"");
    }


}
