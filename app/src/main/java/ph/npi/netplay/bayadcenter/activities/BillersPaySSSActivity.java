package ph.npi.netplay.bayadcenter.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.fourmob.datetimepicker.date.DatePickerDialog;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import ph.npi.netplay.bayadcenter.Epinoyload;
import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.models.Billers;
import ph.npi.netplay.bayadcenter.models.BillsPay;
import ph.npi.netplay.bayadcenter.models.Events;
import ph.npi.netplay.bayadcenter.models.UtilMapSerializer;

/**
 * Created by Rhen Nepacena on 11/11/2015.
 */
public class BillersPaySSSActivity extends BillersPayActivity {
    private EditText med_accountnumber;
    private EditText med_firstname;
    private EditText med_lastname;
    private EditText med_middleinitial;
    private EditText med_amount;
    private Button mbtn_submit;

    private Spinner mspnr_SSSSelectForms;

    private Spinner mspnr_SSSPaymentType;
    private Spinner mspnr_SSSLoanType;
    private TextView mtt_SSSPaymentType;
    private TextView mtt_fromDate;
    private TextView mtt_toDate;
    private TextView mtt_labelFrom;
    private TextView mtt_labelTo;
    private TextView mtt_sssLoanType;
    private TextView mtt_SSSPayorType;
    private TextView mtt_sssflexifund;
    private TextView mtt_sssamount;
    private EditText med_sssamount;
    private EditText med_sssflexifund;
    private EditText med_sssloanaccountnumber;
    private TextView mtt_sssloanaccountnumber;

    private Spinner mspnr_SSSPayorType;
    private TextView mtt_ssscompanyname;
    private EditText med_ssscompanyname;
    private EditText med_sssecamount;
    private TextView mtt_sssecamount;

    private Context mContext;

    private String straccountNumber;
    private String stramount;
    private String strFirstName;
    private String strLastName;
    private String strMiddleName;

    private String strServiceCode;

    private String strSSSPaymentType;
    private String strSSSPaymentTypeOutput;
    private String strfromDate;
    private String strtoDate;
    private String strSSSAmount;
    private String strSSSFlexiFund;
    private String strSSSLoanType;
    private String strSSSLoanTypeOutput;

    private String strSSSPayorType;
    private String strSSSPayorTypeOutput;
    private String strSSSLoanAccountNumber;
    private String strCompanyName;
    private String strSSSECAmount;


    private static final String DATE_PICKER_TAG = "datepicker";
    DatePickerDialog datePickerDialogFrom;
    DatePickerDialog datePickerDialogTo;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_bill_sss);
        Events.inputEventLogs("Access SSS Form");

        med_accountnumber   = (EditText) findViewById(R.id.ed_accountnumber);
        med_firstname       = (EditText) findViewById(R.id.ed_firstname);
        med_lastname        = (EditText) findViewById(R.id.ed_lastname);
        med_middleinitial   = (EditText) findViewById(R.id.ed_middleinitial);
        med_amount          = (EditText) findViewById(R.id.ed_amount);
        mbtn_submit         = (Button) findViewById(R.id.btn_submit);
        mspnr_SSSSelectForms= (Spinner) findViewById(R.id.spnr_SSSSelectForms);

        mtt_fromDate        = (TextView) findViewById(R.id.tt_fromDate);
        mtt_toDate          = (TextView) findViewById(R.id.tt_toDate);
        mtt_labelTo          = (TextView) findViewById(R.id.tt_labelFrom);
        mtt_labelFrom        = (TextView) findViewById(R.id.tt_labelTo);
        mtt_SSSPaymentType   = (TextView) findViewById(R.id.tt_SSSPaymentType);
        mspnr_SSSPaymentType = (Spinner) findViewById(R.id.spnr_SSSPaymentType);
        mtt_sssamount        = (TextView) findViewById(R.id.tt_sssamount);
        med_sssamount        = (EditText) findViewById(R.id.ed_sssamount);
        med_sssflexifund     = (EditText) findViewById(R.id.ed_sssflexifund);
        mtt_sssflexifund     = (TextView) findViewById(R.id.tt_sssflexifund);

        mspnr_SSSLoanType        = (Spinner) findViewById(R.id.spnr_SSSLoanType);
        mtt_sssLoanType          = (TextView) findViewById(R.id.tt_sssLoanType);

        mtt_sssloanaccountnumber = (TextView) findViewById(R.id.tt_sssloanaccountnumber);
        med_sssloanaccountnumber = (EditText) findViewById(R.id.ed_sssloanaccountnumber);
        mtt_SSSPayorType         = (TextView) findViewById(R.id.tt_SSSPayorType);
        mspnr_SSSPayorType       = (Spinner) findViewById(R.id.spnr_SSSPayorType);
        mtt_ssscompanyname       = (TextView) findViewById(R.id.tt_ssscompanyname);
        med_ssscompanyname       = (EditText) findViewById(R.id.ed_ssscompanyname);
        mtt_sssecamount          = (TextView) findViewById(R.id.tt_sssecamount);
        med_sssecamount          = (EditText) findViewById(R.id.ed_sssecamount);

        mbtn_submit.setOnClickListener(submitOnClicked);
        mContext = this;

        final Calendar calendar = Calendar.getInstance();
        datePickerDialogFrom = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);
        datePickerDialogTo = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            intentextras = extras.getString("biller_name");
        }

        biller = new Billers();
        biller = biller.getByName(intentextras);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_action_hardware_phone);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(biller.name);


        mtt_fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialogFrom.setYearRange(2014, 2037);
                datePickerDialogFrom.show(getSupportFragmentManager(), DATE_PICKER_TAG);
            }
        });

        mtt_toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialogTo.setYearRange(2014, 2037);
                datePickerDialogTo.show(getSupportFragmentManager(), DATE_PICKER_TAG);


            }
        });

        mspnr_SSSSelectForms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        strServiceCode = "SSS01";
                           mtt_SSSPaymentType.setVisibility(View.VISIBLE);
                           mspnr_SSSPaymentType.setVisibility(View.VISIBLE);
                           mtt_labelFrom.setVisibility(View.VISIBLE);
                           mtt_fromDate.setVisibility(View.VISIBLE);
                           mtt_labelTo.setVisibility(View.VISIBLE);
                           mtt_toDate.setVisibility(View.VISIBLE);
                           mtt_sssamount.setVisibility(View.VISIBLE);
                           med_sssamount.setVisibility(View.VISIBLE);
                           mtt_sssflexifund.setVisibility(View.VISIBLE);
                           med_sssflexifund.setVisibility(View.VISIBLE);
                           mtt_sssLoanType.setVisibility(View.GONE);
                           mspnr_SSSLoanType.setVisibility(View.GONE);
                           mtt_SSSPayorType.setVisibility(View.GONE);
                           mspnr_SSSPayorType.setVisibility(View.GONE);
                           mtt_sssloanaccountnumber.setVisibility(View.GONE);
                           med_sssloanaccountnumber.setVisibility(View.GONE);
                           mtt_ssscompanyname.setVisibility(View.GONE);
                           med_ssscompanyname.setVisibility(View.GONE);
                           mtt_sssecamount.setVisibility(View.GONE);
                           med_sssecamount.setVisibility(View.GONE);
                        break;
                    case 1:
                        strServiceCode = "SSS02";
                        mtt_SSSPaymentType.setVisibility(View.VISIBLE);
                        mspnr_SSSPaymentType.setVisibility(View.VISIBLE);
                        mtt_labelFrom.setVisibility(View.GONE);
                        mtt_fromDate.setVisibility(View.GONE);
                        mtt_labelTo.setVisibility(View.GONE);
                        mtt_toDate.setVisibility(View.GONE);
                        mtt_sssamount.setVisibility(View.GONE);
                        med_sssamount.setVisibility(View.GONE);
                        mtt_sssflexifund.setVisibility(View.GONE);
                        med_sssflexifund.setVisibility(View.GONE);
                        mtt_sssLoanType.setVisibility(View.VISIBLE);
                        mspnr_SSSLoanType.setVisibility(View.VISIBLE);
                        mtt_SSSPayorType.setVisibility(View.GONE);
                        mspnr_SSSPayorType.setVisibility(View.GONE);
                        mtt_sssloanaccountnumber.setVisibility(View.GONE);
                        med_sssloanaccountnumber.setVisibility(View.GONE);
                        mtt_ssscompanyname.setVisibility(View.GONE);
                        med_ssscompanyname.setVisibility(View.GONE);
                        mtt_sssecamount.setVisibility(View.GONE);
                        med_sssecamount.setVisibility(View.GONE);
                        break;
                    case 2:
                        strServiceCode = "SSS03";
                        mtt_SSSPaymentType.setVisibility(View.GONE);
                        mspnr_SSSPaymentType.setVisibility(View.GONE);
                        mtt_labelFrom.setVisibility(View.GONE);
                        mtt_fromDate.setVisibility(View.GONE);
                        mtt_labelTo.setVisibility(View.GONE);
                        mtt_toDate.setVisibility(View.GONE);
                        mtt_sssamount.setVisibility(View.GONE);
                        med_sssamount.setVisibility(View.GONE);
                        mtt_sssflexifund.setVisibility(View.GONE);
                        med_sssflexifund.setVisibility(View.GONE);
                        mtt_sssLoanType.setVisibility(View.GONE);
                        mspnr_SSSLoanType.setVisibility(View.GONE);
                        mtt_SSSPayorType.setVisibility(View.VISIBLE);
                        mspnr_SSSPayorType.setVisibility(View.VISIBLE);
                        mtt_sssloanaccountnumber.setVisibility(View.VISIBLE);
                        med_sssloanaccountnumber.setVisibility(View.VISIBLE);
                        mtt_ssscompanyname.setVisibility(View.VISIBLE);
                        med_ssscompanyname.setVisibility(View.VISIBLE);
                        mtt_sssecamount.setVisibility(View.VISIBLE);
                        med_sssecamount.setVisibility(View.VISIBLE);
                        break;

                    case 3:
                        strServiceCode = "SSS04";
                        mtt_SSSPaymentType.setVisibility(View.VISIBLE);
                        mspnr_SSSPaymentType.setVisibility(View.VISIBLE);
                        mtt_labelFrom.setVisibility(View.VISIBLE);
                        mtt_fromDate.setVisibility(View.VISIBLE);
                        mtt_labelTo.setVisibility(View.VISIBLE);
                        mtt_toDate.setVisibility(View.VISIBLE);
                        mtt_sssamount.setVisibility(View.VISIBLE);
                        med_sssamount.setVisibility(View.VISIBLE);
                        mtt_sssflexifund.setVisibility(View.VISIBLE);
                        med_sssflexifund.setVisibility(View.VISIBLE);
                        mtt_sssLoanType.setVisibility(View.GONE);
                        mspnr_SSSLoanType.setVisibility(View.GONE);
                        mtt_SSSPayorType.setVisibility(View.GONE);
                        mspnr_SSSPayorType.setVisibility(View.GONE);
                        mtt_sssloanaccountnumber.setVisibility(View.GONE);
                        med_sssloanaccountnumber.setVisibility(View.GONE);
                        mtt_ssscompanyname.setVisibility(View.VISIBLE);
                        med_ssscompanyname.setVisibility(View.VISIBLE);
                        mtt_sssecamount.setVisibility(View.GONE);
                        med_sssecamount.setVisibility(View.GONE);
                        break;

                    case 4:
                        strServiceCode = "SSS05";
                        mtt_SSSPaymentType.setVisibility(View.VISIBLE);
                        mspnr_SSSPaymentType.setVisibility(View.VISIBLE);
                        mtt_labelFrom.setVisibility(View.GONE);
                        mtt_fromDate.setVisibility(View.GONE);
                        mtt_labelTo.setVisibility(View.GONE);
                        mtt_toDate.setVisibility(View.GONE);
                        mtt_sssamount.setVisibility(View.GONE);
                        med_sssamount.setVisibility(View.GONE);
                        mtt_sssflexifund.setVisibility(View.GONE);
                        med_sssflexifund.setVisibility(View.GONE);
                        mtt_sssLoanType.setVisibility(View.VISIBLE);
                        mspnr_SSSLoanType.setVisibility(View.VISIBLE);
                        mtt_SSSPayorType.setVisibility(View.GONE);
                        mspnr_SSSPayorType.setVisibility(View.GONE);
                        mtt_sssloanaccountnumber.setVisibility(View.GONE);
                        med_sssloanaccountnumber.setVisibility(View.GONE);
                        mtt_ssscompanyname.setVisibility(View.VISIBLE);
                        med_ssscompanyname.setVisibility(View.VISIBLE);
                        mtt_sssecamount.setVisibility(View.GONE);
                        med_sssecamount.setVisibility(View.GONE);
                        break;

                    case 5:
                        strServiceCode = "SSS06";
                        mtt_SSSPaymentType.setVisibility(View.GONE);
                        mspnr_SSSPaymentType.setVisibility(View.GONE);
                        mtt_labelFrom.setVisibility(View.GONE);
                        mtt_fromDate.setVisibility(View.GONE);
                        mtt_labelTo.setVisibility(View.GONE);
                        mtt_toDate.setVisibility(View.GONE);
                        mtt_sssamount.setVisibility(View.GONE);
                        med_sssamount.setVisibility(View.GONE);
                        mtt_sssflexifund.setVisibility(View.GONE);
                        med_sssflexifund.setVisibility(View.GONE);
                        mtt_sssLoanType.setVisibility(View.GONE);
                        mspnr_SSSLoanType.setVisibility(View.GONE);
                        mtt_SSSPayorType.setVisibility(View.VISIBLE);
                        mspnr_SSSPayorType.setVisibility(View.VISIBLE);
                        mtt_sssloanaccountnumber.setVisibility(View.GONE);
                        med_sssloanaccountnumber.setVisibility(View.GONE);
                        mtt_ssscompanyname.setVisibility(View.VISIBLE);
                        med_ssscompanyname.setVisibility(View.VISIBLE);
                        mtt_sssecamount.setVisibility(View.GONE);
                        med_sssecamount.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        initializeCheckComponents();
        populateSpinner();

    }



    private View.OnClickListener submitOnClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            straccountNumber  = med_accountnumber.getText().toString().trim();
            stramount         = med_amount.getText().toString().trim();
            strFirstName      = med_firstname.getText().toString().trim();
            strLastName       = med_lastname.getText().toString().trim();
            strMiddleName     = med_middleinitial.getText().toString().trim();

            strSSSAmount      = med_sssamount.getText().toString().trim();
            strSSSFlexiFund   = med_sssflexifund.getText().toString().trim();
            strSSSPaymentType = mspnr_SSSPaymentType.getSelectedItem().toString();
            strSSSLoanType    = mspnr_SSSLoanType.getSelectedItem().toString();

            strSSSPayorType   = mspnr_SSSPayorType.getSelectedItem().toString();
            strSSSLoanAccountNumber = med_sssloanaccountnumber.getText().toString().trim();
            strCompanyName    = med_ssscompanyname.getText().toString();
            strSSSECAmount    = med_sssecamount.getText().toString();

            stramount    = stramount.replace(" ", "");
            stramount    = stramount.replace("-","");


            if(strSSSPaymentType.contains("Employer (Disabled)")) {
                strSSSPaymentTypeOutput = "R";
            }else if(strSSSPaymentType.contains("Household (Disabled)")){
                strSSSPaymentTypeOutput = "H";
            }else if(strSSSPaymentType.contains("Self-Employed")){
                strSSSPaymentTypeOutput = "S";
            }else if(strSSSPaymentType.contains("Voluntary Worker")){
                strSSSPaymentTypeOutput = "V";
            }else if(strSSSPaymentType.contains("OFW")){
                strSSSPaymentTypeOutput = "O";
            }else if(strSSSPaymentType.contains("Fisherman")){
                strSSSPaymentTypeOutput = "F";
            }else if(strSSSPaymentType.contains("Non-Working Spouse")){
                strSSSPaymentTypeOutput = "N";
            }


            if(strSSSLoanType.contains("Salary Loan")){
                strSSSLoanTypeOutput = "SL";
            }else if(strSSSLoanType.contains("Calamity Loan")){
                strSSSLoanTypeOutput = "CL";
            }else if(strSSSLoanType.contains("Emergency Loan")){
                strSSSLoanTypeOutput = "EL";
            }else if(strSSSLoanType.contains("Educational Loan")){
                strSSSLoanTypeOutput = "EDL";
            }else if(strSSSLoanType.contains("SLERP")){
                strSSSLoanTypeOutput = "SLE";
            }


           if(strSSSPayorType.contains("Employer")){
                strSSSPaymentTypeOutput = "R";
           }else if(strSSSPayorType.contains("Individual")){
                strSSSPaymentTypeOutput = "I";
           }


            if((straccountNumber.isEmpty()) || (stramount.isEmpty()) || (strFirstName.isEmpty()) || (strLastName.isEmpty())) {
                if (straccountNumber.isEmpty()) {
                    med_accountnumber.setBackgroundResource(R.drawable.edittext_red_rounded);
                }
                if (stramount.isEmpty()) {
                    med_amount.setBackgroundResource(R.drawable.edittext_red_rounded);
                }

                if(strFirstName.isEmpty()){
                    med_firstname.setBackgroundResource(R.drawable.edittext_red_rounded);
                }

                if(strLastName.isEmpty()){
                    med_lastname.setBackgroundResource(R.drawable.edittext_red_rounded);
                }

                return;
            }

            if(straccountNumber.length() < 10){
                Epinoyload.showAlert(mContext, "Account Number ", "Need to check Account Number");
            }else{
                if (Epinoyload.isOffline(mContext) == false) {
                    AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
                    alertDialog.setTitle("Please Confirm");
                    alertDialog.setMessage("You are about to pay " + biller.name + " with " + biller.field + straccountNumber + ". Are you sure you want to continue?");
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            queTransactions();
                            return;
                        }
                    });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            return;
                        }
                    });
                    alertDialog.show();
                } else {
                    // offline
                    Epinoyload.showAlert(mContext, "Please Confirm", "You are about to pay " + biller.name + " with " + straccountNumber + ".\n\nYou are in offline mode ! Transactions will be queued...\n\nAre you sure you want to continue?",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // Postive
                                    queTransactions();
                                }
                            }, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // negative
                                    dialogInterface.dismiss();
                                }
                            }
                    );
                }
            }
        }
    };


    private void queTransactions(){

        HashMap<String,String> api_encode_all = new HashMap<String, String>();
        api_encode_all.put("account_no",straccountNumber);
        api_encode_all.put("amount_due",stramount);

        api_encode_all.put("service_code",strServiceCode);
        api_encode_all.put("mode_of_payment",strPaymentType);

        api_encode_all.put("first_name",strFirstName);
        api_encode_all.put("last_name",strLastName);
        api_encode_all.put("middle_initial",strMiddleName);
        api_encode_all.put("company_name","test account");
        api_encode_all.put("ec_amount",stramount);

        if(strServiceCode.contains("SSS01")){
            api_encode_all.put("payment_type",strSSSPaymentTypeOutput);
            api_encode_all.put("from",strfromDate);
            api_encode_all.put("to",strtoDate);
            api_encode_all.put("sss_amount",strSSSAmount);
            api_encode_all.put("sss_flexifund",strSSSFlexiFund);
        }else if(strServiceCode.contains("SSS02")){
            api_encode_all.put("payment_type",strSSSPaymentTypeOutput);
            api_encode_all.put("loan_type",strSSSLoanTypeOutput);
        }else if(strServiceCode.contains("SSS03")){
            api_encode_all.put("payor_type",strSSSPayorTypeOutput);
            api_encode_all.put("loan_account_no",strSSSLoanAccountNumber);
            api_encode_all.put("rel_type","LP");
            api_encode_all.put("company_name",strCompanyName);
        }else if(strServiceCode.contains("SSS04")){
            api_encode_all.put("payment_type",strSSSPaymentTypeOutput);
            api_encode_all.put("company_name",strCompanyName);
            api_encode_all.put("from",strfromDate);
            api_encode_all.put("to",strtoDate);
            api_encode_all.put("sss_amount",strSSSAmount);
            api_encode_all.put("sss_flexifund",strSSSFlexiFund);
            api_encode_all.put("ec_amount",strSSSECAmount);
        }else if(strServiceCode.contains("SSS05")){
            api_encode_all.put("payment_type",strSSSPaymentTypeOutput);
            api_encode_all.put("company_name",strCompanyName);
        }else if(strServiceCode.contains("SSS06")){
            api_encode_all.put("payor_type",strSSSPayorTypeOutput);
            api_encode_all.put("rel_type","LP");
            api_encode_all.put("company_name",strCompanyName);
        }

        /*if(strPaymentType.contains("Check") || strPaymentType.contains("Combined")){
            api_encode_all.put("check_no", strcheckNumber);
            api_encode_all.put("check_date",strcheckDate);
            api_encode_all.put("bank_name",strbankName);
            api_encode_all.put("branch",strbranch);

            api_encode_all.put("name",strcheckName);
            api_encode_all.put("bank_code",strbankCode);
            api_encode_all.put("check_type",strtypeName);
        }*/

        if(strPaymentType.contains("Cash")){
            api_encode_all.put("amount",stramount);
            api_encode_all.put("amount_paid",stramount);
        }/*else if(strPaymentType.contains("Check")){
            api_encode_all.put("amount",strcheckAmount);
            api_encode_all.put("amount_paid",stramount);
        }else if(strPaymentType.contains("Combined")){
            api_encode_all.put("cash_amount",stramount);
            api_encode_all.put("check_amount",strcheckAmount);
            float cash_amount = Float.parseFloat(stramount);
            float check_amount = Float.parseFloat(strcheckAmount);
            float total = cash_amount + check_amount;
            api_encode_all.put("amount_paid",String.valueOf(total));
        }*/

        UtilMapSerializer utilMapSerializer = new UtilMapSerializer();
        String api_encode = utilMapSerializer.serialize(api_encode_all);

        Calendar calendar = Calendar.getInstance();
       /* String sentDate = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"))
                .format(calendar.getTime());*/
        String sentTime = (new SimpleDateFormat("HH:mm:ss"))
                .format(calendar.getTime());

        BillsPay bps = new BillsPay();
        bps.billspayid = String.valueOf("" + (System.currentTimeMillis() / 1000));
        bps.createdDate = new Date();
        bps.time_sent = sentTime;

        bps.isSuccessful = false;
        bps.biller_code = biller.bilcode;
        bps.billername = biller.name;
        bps.failed_transaction = 0;

        bps.service_code    = strServiceCode;
        bps.account_no      = straccountNumber;
        bps.amount_due      = stramount;
        bps.amount_paid     = stramount;
        bps.mode_of_payment = strPaymentType;
        bps.amount          = stramount;

        bps.first_name      = strFirstName;
        bps.last_name       = strLastName;
        bps.middle_initial  = strMiddleName;

        bps.sss_payment_type= strSSSPaymentTypeOutput;
        bps.from_date       = strfromDate;
        bps.to_date         = strtoDate;
        bps.sss_amount      = strSSSAmount;
        bps.sss_flexifund   = strSSSFlexiFund;
        bps.sss_loan_type   = strSSSLoanType;
        bps.payor_type      = strSSSPayorType;
        bps.loan_account_no = strSSSLoanAccountNumber;
        bps.company_name    = strCompanyName;
        bps.sss_ec_amount   = strSSSECAmount;


        bps.check_no        = strcheckNumber;
        bps.check_date      = strcheckDate;
        bps.bank_name       = strbankName;
        bps.branch          = strbranch;
        bps.check_amount    = strcheckAmount;
        bps.check_name      = strcheckName;
        bps.bank_code       = strbankCode;
        bps.type_name       = strtypeName;

        bps.api_encode  =     api_encode;
        bps.field_status = "ForUploading";
        bps.is_sent = 0;

        bps.save();
//        showDialogAlert("Transactions Saved","Queued");
        processWorkFlow(bps);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.billspay, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        //TODO check day if day < 10 add 0 prefix.

        if(datePickerDialog == datePickerDialogFrom){
            String dayString = day + "";
            String monString = month + "";
            if(day < 10){
                dayString = "0" + day;
            }

            if(month < 9){
                month += 1;
                monString = "0" + month;
            }else{
                monString = String.valueOf(month + 1);
            }

            //mDepositDate = (month + 1) + "-" + dayString + "-" + year;
            strfromDate = (monString) + "/" + dayString + "/" + year;
            mtt_fromDate.setText(strfromDate);

            return;
        }

        if(datePickerDialog == datePickerDialogTo){

            String dayString = day + "";
            String monString = month + "";
            if(day < 10){
                dayString = "0" + day;
            }

            if(month < 9){
                month += 1;
                monString = "0" + month;
            }else{
                monString = String.valueOf(month + 1);
            }

            //mDepositDate = (month + 1) + "-" + dayString + "-" + year;
            strtoDate = (monString) + "/" + dayString + "/" + year;

            mtt_toDate.setText(strtoDate);

            return;
        }


        if(datePickerDialog == datePickerDialogCheck){
            String dayString = day + "";
            String monString = month + "";
            if(day < 10){
                dayString = "0" + day;
            }

            if(month < 9){
                month += 1;
                monString = "0" + month;
            }else{
                monString = String.valueOf(month + 1);
            }


            strcheckDate = (monString) + "-" + dayString + "-" + year;

            mtt_checkDate.setText(strcheckDate);
        }
    }
}
