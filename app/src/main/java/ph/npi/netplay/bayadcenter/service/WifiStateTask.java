package ph.npi.netplay.bayadcenter.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.Map;

/**
 * Created by Rhen Nepacena on 4/11/2015.
 */
public class WifiStateTask extends Service{

    static Thread thread;
    static int tries;
    static WifiManager wifi;

    public static boolean isOnline = false;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("WifiStateTask","WifiStateRunning");
        thread = new Thread() {
            @Override
            public void run() {

                tries = 0;
                wifi = (WifiManager) getApplicationContext().getSystemService(getApplicationContext().WIFI_SERVICE);

                for(;;){

                    try{

                        if(!wifi.isWifiEnabled()) {
//                            wifi.setWifiEnabled(true);
                            sleep(5000);
                        }

                        if(isInterrupted())
                            break;

                        try {
                            processPing();
                        } catch (Exception e){

                            tries++;

                            if(tries >= 10) {
//                                wifi.setWifiEnabled(false);
                                tries = 0;
                            } // end if

                            e.printStackTrace();
                        }

                        sleep(5000); //sleep for 1 minute
                    } catch (InterruptedException e){
                        break;
                    }

                } //end for

            }
        };
        thread.start();


        //return START_STICKY; -not working anymore on kitkat it will only return null
        //http://developer.android.com/reference/android/app/Service.html#START_STICKY
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    static public void cancel() {
        //thread.stop();
        try{
            thread.interrupt();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void processPing(){

        RequestQueue queue = VolleySingleton.getsInstance().getRequestQueue();
        String url = "http://128.199.172.235/andglobe/ping.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try{
                        	
                        	if(response.equals("1")) {
                                //Do nothing
                                tries = 0;
                                isOnline = true;

                                Log.i("wifistate-result","got it");
                                return;
                            }else{
                                Log.i("wifistate-result","dont got it");
                                isOnline = false;

                            }
                        	
                        	tries++;

                            if(tries >= 8) {
//                                wifi.setWifiEnabled(false);
                                tries = 0;
                            } // end if

                        }catch (Exception e){
                            Log.i("wifistate-error",e.toString());

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                isOnline = false;
                tries++;

                if(tries >= 8) {
//                    wifi.setWifiEnabled(false);
                    tries = 0;
                    isOnline = false;
                } // end if

            }
        }){
            @Override
            protected Map<String, String> getParams() {

                /*Map<String, String> params = new HashMap<String, String>();
                params.put("source", getPhoneNumber(getApplicationContext()));*/

                return null;
            }

        };
        queue.add(stringRequest);

    }

    private  String getPhoneNumber(Context context){
        String lineNumber = "";
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try{
            lineNumber = telephonyManager.getLine1Number();
        }catch (Exception e){
            Log.e("[DualSim-lineNumber]",e.toString());
            lineNumber = telephonyManager.getLine1Number();
        }
        return lineNumber;
    }
}
