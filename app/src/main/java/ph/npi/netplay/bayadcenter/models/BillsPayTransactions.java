package ph.npi.netplay.bayadcenter.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by Rhen Nepacena on 11/4/2015.
 */
@Table(name = "billspaytransactions")
public class BillsPayTransactions extends Model {

    @Column(name = "channel")
    public String channel;

    @Column(name = "service_type")
    public String service_type;

    @Column(name = "reference_id")
    public String reference_id;

    @Column(name = "external_reference")
    public String external_reference;

    @Column(name = "status")
    public String status;

    @Column(name = "amount")
    public String amount;

    @Column(name = "wallet_balance")
    public String wallet_balance;

    @Column(name = "timestamp")
    public String timestamp;

    @Column(name = "username")
    public String username;

}
