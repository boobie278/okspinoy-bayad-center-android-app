package ph.npi.netplay.bayadcenter;

import android.app.Application;
import android.content.Context;

import com.activeandroid.ActiveAndroid;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;


/**
 * Created by Rhen Nepacena on 4/3/2015.
 */
public class BayadApplication extends Application {

    private static BayadApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        sInstance = this;
        ActiveAndroid.initialize(this);


    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        ActiveAndroid.dispose();
    }

    public static BayadApplication getsInstance(){
        return sInstance;
    }

    public static Context getAppContext(){
        return sInstance.getApplicationContext();
    }


}
