package ph.npi.netplay.bayadcenter.activities;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.webkit.WebView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

import ph.npi.netplay.bayadcenter.R;


public class ContactUsActivity extends SherlockActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);



        getSupportActionBar().setIcon(R.drawable.ic_action_contact_center);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Contact Us");

        String versionName = "";
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String details = "<html>\n" +
                "<body>\n" +
                "App Version: v" + versionName + "\n" +
                "<br>\n" +
                "<br>\n" +
                "<h3>24/7 Help Desk</h3>\n" +
                "\n" +
                "<p> For concerns, pls call <font color=\"blue\">(02) 234-5678</font>  or send us an email at <font color=\"blue\">support@bayadcenter.com</font>.\n" +
                "<br>\n" +
                "<br>\n" +
                "Be sure to be ready with the following:\n" +
                "<br>\n" +
                "<br>\n" +
                "1. Your Registered Mobile No. / Username <br>\n" +
                "2. Mobile No or Denomination that has a problem. <br>\n" +
                "3. Location of Mobile No that has a problem. (Due to delayed messaging) <br>\n" +
                "4. Complete details of your concern\n" +
                "<br>\n" +
                "<br>\n" +
                "<b>Office Location</b>\n" +
                "<br>\n" +
                "<br>\n" +

                "G/F, Business Solutions Center\n" +
                "Meralco Complex, Ortigas Avenue\n" +
                "0300 Pasig City, Philippines\n" +
                "<br>\n" +
                "<br>\n" +
                "<font color=\"blue\">CIS Bayad Center, Inc.</font> is a duly registered company in the SEC.</p>\n" +
                "\n" +
                "</body>\n" +
                "</html>\n";




        WebView view = (WebView) findViewById(R.id.contact_us_view);
        view.loadData(details, "text/html", "UTF-8");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
