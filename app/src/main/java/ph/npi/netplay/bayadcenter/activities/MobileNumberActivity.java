package ph.npi.netplay.bayadcenter.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.actionbarsherlock.app.SherlockActivity;

import ph.npi.netplay.bayadcenter.Epinoyload;
import ph.npi.netplay.bayadcenter.R;
import ph.npi.netplay.bayadcenter.models.Events;


public class MobileNumberActivity extends SherlockActivity {

    private EditText mMobileNumberEditText;
    private Context mContext;
    private boolean mFromSettings = false;
    private String mMobileNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_number);

        Events.inputEventLogs("Mobile Number Input");

        mContext = this;

        getSupportActionBar().setIcon(R.drawable.ic_action_hardware_phone);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mFromSettings = extras.getBoolean("fromSettings", false);
        }


        mMobileNumberEditText = (EditText) findViewById(R.id.mobileNumberEditText);
        Button saveMobileNumberButton = (Button) findViewById(R.id.saveMobileNumberButton);
        saveMobileNumberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveMobileNumber();
            }
        });

        try{
            String mPhoneNumber = "";
            if (Epinoyload.getMobileNumber(mContext).length() == 0) {
                TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                mPhoneNumber = tMgr.getLine1Number();
            } else {
                mPhoneNumber = Epinoyload.getMobileNumber(mContext);
            }

            if (mPhoneNumber != null && mPhoneNumber.length() > 0) {
                mPhoneNumber = mPhoneNumber.substring((mPhoneNumber.length() - 10), mPhoneNumber.length());
                mMobileNumberEditText.setText(mPhoneNumber);
            }

        }catch (NullPointerException e){

        }

    }

    private void saveMobileNumber() {
        mMobileNumber = mMobileNumberEditText.getText().toString().trim();
        if (mMobileNumber.length() <= 0 || mMobileNumber.length() < 10) {
            Epinoyload.showAlert(mContext, "Error", "Please enter a valid mobile number");
            Events.inputEventLogs("Error Mobile Number Input ");
            return;
        }

        mMobileNumber = "63" + mMobileNumber;



        Epinoyload.showAlert(mContext, "Confirmation", "You have entered the following number:\n\n" + mMobileNumber + "\n\nAre you sure this is the mobile number connected to your phone?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // positive
                Epinoyload.setMobileNumber(mContext, mMobileNumber);
                if (mFromSettings) {
                    finish();
                } else {
                    Intent intent = new Intent(mContext, MainActivity.class);
                    startActivity(intent);
                    Events.inputEventLogs("Mobile Number Input Success");
                    finish();
                }
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        return;
    }

}
